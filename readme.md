# Hello Tecstra
Mike and Dan - these notes were set up a while back when we were using docker.
We're in the process of removing the docker container. 

We'll update this with new info once available :)

## Requirements
- Docker ([To who install it](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04))
- Ubuntu / Mac
## Step by step

### 1. Clone repository
- SSH
```git clone git@bitbucket.org:zookadevelopers/salsas.git```

Checkout to develop branch
```git checkout develop```

### 2. Build image
Run in main directory
```docker build -t salsas ./```

### 3. Run container
`docker run -ti --name salsas -p 80:80 -p 3306:3306 -v $(PWD):/var/www/html -v ~/docker/salsas:/var/lib/mysql salsas`

### 4. Run Services
With the following code line you can run mysql, ngnix services
`/runner/./local.sh`

### 5. Database
Update the ip/dns in the backup file of the database.

`replace "206.189.231.128" "localhost" --  salsas.sql`

`replace "206.189.231.128" "localhost" -- salsas.sql`

Login to mysql with your username and password `mysql -u root -p` and in the mysql console type the following command` create database salsas`, logout to mysql, then import the database `mysql salsas <  [DATABASE-BACKUP-DIRECTORY]`.