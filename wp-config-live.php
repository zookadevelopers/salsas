<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'salsas');

/** MySQL database username */
define('DB_USER', 'admin');

/** MySQL database password */
define('DB_PASSWORD', 'Sigma123s@@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'GPW!h*;1fz%97hjp3s#M$E@K5KY,fKh,r=I``60o%KH)s[Fn2K$F8wW?-#Cm;}n4');
define('SECURE_AUTH_KEY',  'V+7wSf viK<-5F,i96~R)UonLkNg.x}DYgLNAM0C8:m=/iV]{`(+;u7d`9QQgy9A');
define('LOGGED_IN_KEY',    'y`r(-(8qL{ZRcI&5uG@Qp(=/h?Zp%Pc$BOH~)FBhG?C{_pK#v}yE#&t?`1yX&lC9');
define('NONCE_KEY',        'd[yp%lSZpo^!k-^Nis9V_FnE*4[OXDOqe>P?b|rZwn]JT]FLkPWQZ${H-Gr^FlbY');
define('AUTH_SALT',        ',Oi5;- P( k)$Vd+,5n: `uP6tI{V@eLc)1:F|e;XB1Q/p=f&Z~#^MNLYSFus4OS');
define('SECURE_AUTH_SALT', 'hkzvSYxz/g=6{40A|jQ$=_HtTz~mt0e#~D#N2cPnqH|.?+C7%>Zu(7;k*.Hk4An0');
define('LOGGED_IN_SALT',   ' A{PMkSn[@Z7_hEQ.Fb1_kVsyo}vlv!`Hv;QOhz9KL0|&k=$bQAQrxDT7:/7<7s7');
define('NONCE_SALT',       '=<tK[I4F!;D{{u%.Gy-:H0b9.3-NC2$DVv.X-n~j^+.7Dj8@<WbjS*lxi.;(1U;W');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */
define ( 'WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', '206.189.231.128');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
