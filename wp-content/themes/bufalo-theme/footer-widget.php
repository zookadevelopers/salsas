<?php
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?>
<div id="footer-banner" class="footer-banner">
    <div class="container">
        <div class="row">
            <div id="url-site" class="col-12 col-md-4 col-lg-2 url-site">
                <?php if ( get_theme_mod( 'wp_bootstrap_starter_logo_footer' ) ): ?>
                    <a href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        <img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo_footer' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" class="img-fluid" />
                    </a>
                <?php else : ?>
                    <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>" title="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"><img src="<?php echo $upload_theme; ?>logo-bufalo.png" class="img-fluid" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
                <?php endif; ?>
                    
            </div>
            <div id="url-description" class="col-12 col-md-7 col-lg-8 offset-md-1 url-description"><?php dynamic_sidebar('footer-banner-2'); ?>
            </div>
        </div>
    </div>
</div>

<div class="cont-main-footer">
<?php
if ( is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) ) {?>
        <div id="footer-widget" class="footer-widget">
            <div class="container">
                <div class="row">
                    <?php if ( is_active_sidebar( 'footer-1' )) : ?>
                        <div class="col-12 col-md-4 col-lg-3 social-links-section"><?php dynamic_sidebar( 'footer-1' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-2' )) : ?>
                        <div class="col-12 col-md-4 col-lg-4 mt-4 mt-md-0"><?php dynamic_sidebar( 'footer-2' ); ?></div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'footer-3' )) : ?>
                        <div class="col-12 col-md-4 col-lg-5 section-newsletter mt-5 mt-md-0"><?php dynamic_sidebar( 'footer-3' ); ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
<?php }