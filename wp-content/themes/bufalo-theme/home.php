<?php 
// Template Name: Home herdez


$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$site_url = get_site_url();

// Products Query
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
		'orderby'=>'menu_order'
	)
); 
?>

<?php get_header(); ?>
<section class="banner-home container-fluid">
	<div class="row">
		<div class="col-12 p-0 ">
			<div id="carousel-home" class="owl-carousel owl-theme">
				<?php 
					$banner_home = new WP_Query(
						array(
							'post_type'=>'banner_slider', 
							'post_status'=>'publish', 
							'posts_per_page'=> -1,
							'order' => 'ASC',
        					'orderby'=>'menu_order'
						)
					); 
				?>
				<?php while ( $banner_home->have_posts() ) : $banner_home->the_post(); ?>
					<div class="item">
						<img class="img-banner img-fluid w-100" src="<?php the_field('banner-image'); ?>" alt="<?php the_field('banner-title'); ?>" />
					</div>
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>

<section class="text-under-banner">
	<div class="container">
		<div class="col-12 p-0">
			<h2 class="my-3"><?php the_field('under_banner_title', 50); ?></h2>
			<p><?php the_field('under_banner_description', 50); ?></p>
		</div>
	</div>
</section>

<section class="products-home">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>Products</h3>
			</div>
		</div>
		<div class="row items-products">
			<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
				<?php
					$image_id = get_post_thumbnail_id();
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
					$image_title = get_the_title($image_id);
				?>
				<div class="col-6 col-md-4 col-lg-3 text-center item">
					<a class="link-product" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
						<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
						<p class="mt-3"><?php the_title(); ?></p>
					</a>
				</div>
			<?php endwhile; wp_reset_postdata(); ?>
		</div>
	</div>
</section>

<?php 
	$num_post1 = 0;
	$id_page = 1;
	$slug_page = 'bufalo';

	switch_to_blog($id_page); 
	// Get posts for each blog
	$myposts = get_posts( 
	    array( 
	        'category_name'  => $slug_page,
	        'posts_per_page' => -1, 
	        'orderby' => 'modified'
	    )
	);

	// Loop for each blog
	global $post;
	
	if ( !empty($myposts) ):
?>

<section class="featured-recipes-home">
	<div class="container">
		<div class="row">
			<div class="col-12 mb-3">
				<h3>Featured Recipes</h3>
			</div>
		</div>
		<div class="row recipes">
			<div class="col-12 col-lg-8 mb-0">
				<?php 
					// Loop for each blog
					foreach( $myposts as $post ) { 
						if ( get_field('outstanding_recipes') == 'Left position') {
							if ($num_post >= 1) {
	                        	break;
	                        } 
	                        $recipeSlug = $post->post_name;
							$image_id = get_post_thumbnail_id();
							$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
							$image_title = get_the_title($image_id);
	                        ?>
							<a class="d-block h-100" title="<?php the_title(); ?>" href="<?php echo $site_url; ?>/recipe/<?php echo $recipeSlug; ?>">
								<div class="h-100 recipes-item">
									<?php if (get_the_post_thumbnail_url()) { ?>
										<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
									<?php }else{ ?>
										<img src="<?php echo $upload_theme ?>recipe.jpg" alt="<?php the_title(); ?>" />
									<?php }  ?>
									<p class="text"><?php the_title(); ?></p>
								</div>
							</a>
					<?php $num_post++; } }
				?>
			</div>
			<div class="col-12 col-lg-4">
				<?php
				$num_post2 = 0;
				foreach( $myposts as $post ) { 
					if (get_field('outstanding_recipes') == 'Right position') { 
						if ($num_post2 >= 2) {
                        	break;
                        }
                        $recipeSlug = $post->post_name;
                        ?>
						<a class="h-100" title="<?php the_title(); ?>" href="<?php echo $site_url; ?>/recipe/<?php echo $recipeSlug; ?>">
							<div class="item recipes-item">
								<?php if (get_the_post_thumbnail_url()) { ?>
									<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
								<?php }else{ ?>
									<img src="<?php echo $upload_theme ?>recipe.jpg" alt="<?php the_title(); ?>" />
								<?php }  ?>
								<p class="text"><?php the_title(); ?></p>
							</div>
						</a>
				<?php $num_post2++;  } }
				wp_reset_postdata();
				?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<?php 
restore_current_blog();
 get_footer();
