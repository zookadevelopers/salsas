<?php 
function cptui_register_my_cpts() {


	/**
     * Post Type: Categories.
     */

    $labels = array(
        "name" => __( "Categories", "wp-bufalo-theme" ),
        "singular_name" => __( "Category", "wp-bufalo-theme" ),
        "add_new" => __( "Add New Category", "wp-bufalo-theme" ),
    );

    $args = array(
        "label" => __( "Categories", "wp-bufalo-theme" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "products", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "/bufalo/wp-content/uploads/sites/4/2018/11/icon-product.png",
        "supports" => array( "title", "editor", "thumbnail", "custom-fields", "page-attributes" ),
    );

    register_post_type( "products", $args );

	/**
	 * Post Type: Banner Sliders.
	 */

	$labels = array(
		"name" => __( "Banner Sliders", "wp-bufalo-theme" ),
		"singular_name" => __( "Banner Slider", "wp-bufalo-theme" ),
	);

	$args = array(
		"label" => __( "Banner Sliders", "wp-bufalo-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "banner_slider", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://206.189.231.128/lavictoria/wp-content/uploads/sites/6/2018/12/icon-slider.png",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "banner_slider", $args );

	/**
	 * Post Type: Hot Sauces.
	 */

	$labels = array(
		"name" => __( "Hot Sauces", "wp-bufalo-theme" ),
		"singular_name" => __( "Hot Sauce", "wp-bufalo-theme" ),
	);

	$args = array(
		"label" => __( "Hot Sauces", "wp-bufalo-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/hot-sauces", "with_front" => true ),
		"query_var" => "hot-sauces",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "hot_sauces", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
