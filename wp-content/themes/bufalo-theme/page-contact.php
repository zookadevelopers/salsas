<?php 

// Template Name: Contact US

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 <section class="contact-page">
    <div class="container">
    	<div class="row">
    		<div class="col-12">
    			<h1>Contact Búfalo</h1>
    		</div>
    	</div>
        <div class="row">
        	<div class="col-12 col-md-4">
                <div class="text">
                    <?php the_field('contact_details'); ?>
                </div>
            </div>
            <div class="col-12 col-md-8">
                <h3>Submit your question or feedback</h3> 
                <div class="form-contact">
                   <?php the_content() ?>
                </div>
            </div>
        </div>
    </div>
</section> 

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
