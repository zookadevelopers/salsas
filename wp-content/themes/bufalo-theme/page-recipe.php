<?php 
// Template Name: Single Recipe
get_header();

	global $post;
	$upload_dir = wp_upload_dir();
	$upload_theme = get_template_directory_uri().'/inc/assets/img/';
	$site_url = get_site_url();

//	$recipeId = get_query_var('ids');
	$salsas_id_theme = 1;

	switch_to_blog($salsas_id_theme); 

	$salsas_domain = get_site_url();
	$slug = $wp_query->query_vars['id'];

	$args = array(
		'name'        => $slug,
		'post_type'   => 'post',
		'post_status' => 'publish',
		'numberposts' => 1
	);

	$my_posts = get_posts($args);

	$recipeId = $my_posts[0]->ID;

	$recipeInfo = get_post($recipeId);
	$pageId = get_field('page_id', $recipeId);

	$recipeTitle = get_field('sals-recipe-title' , $recipeId);
	$recipeDescription = get_field('sals-recipe-description', $recipeId);
	$recipeVideo = get_field('sals-recipe-video', $recipeId);
	$recipeAuthor = get_field('sals-recipe-author', $recipeId);
	$recipeDate = get_field('sals-recipe-date', $recipeId);
	$recipeBrandLogo = get_field('sals-recipe-brand-logo', $recipeId);

	$recipeServings = get_field('sals-recipe-servings', $recipeId);
	$recipeCaloriesServings = get_field('sals-recipe-calories-servings', $recipeId);
	$recipePrepTime = get_field('sals-recipe-prep-time', $recipeId);
	$recipeCookTime = get_field('sals-recipe-cook-time', $recipeId);
	$recipeTotalTime = get_field('sals-recipe-total-time', $recipeId);

	$recipeImage = get_field('recipe-herdez-image', $recipeId);
	$recipeUrl = get_post_permalink( $recipeId );

	$apiKey = '59fb8bdf-cb43-490b-8b09-975d68551c2a';
	$merchantGroup = '78368';
	$merchantId = '278593';
	$allPostTags = get_the_tags( $recipeId );

	$i=0;
	if ($allPostTags) {
		foreach($allPostTags as $tags) {
			$i++;
			if ($i == 1) {
				$firstTagSlug = $tags->slug;
				$firstTagName = $tags->name;
			}
		}
	}

	if ($recipeImage == '') {
		$recipeImage = get_the_post_thumbnail_url($recipeId);
	}
?>

<?php if( $my_posts && $firstTagSlug == 'bufalo' ): ?>

<div id="page-sub-header" class="sub-header-filter header-single-post" title="<?php echo $recipeTitle; ?>" style="background-image: url('<?php echo $recipeImage; ?>');">

	<a href="<?php echo home_url('/recipes'); ?>" class="back-recipes d-none"><i class="fa fa-angle-left"></i> Back to all Recipes</a>

	<?php if( get_field('recipe_servings_suggestion', $recipeId) ): ?>
		<span class="serving-suggestion">Serving Suggestion</span>
	<?php endif; ?>

	<?php if ($recipeVideo): ?>
		<div class="cont-recipe-video">
			<?php echo $recipeVideo; ?>
			<div class="button-play" id="play-button" data-toggle="modal" data-target="#recipeVideo"></div><!-- /.button-play -->
		</div><!-- /.cont-recipe-video -->
	<?php endif ?>
</div>

<section id="post-<?php echo $recipeId; ?>" <?php post_class(); ?> data-post-id="<?php echo $recipeId; ?>" data-brand-name="<?php echo $recipeAuthor; ?>">

	<div class="container">
		<div class="row">
			<div class="col-12">
				<header class="entry-header">
					<h1 class="recipe-title"><?php echo $recipeTitle; ?></h1>

					<div class="product-reviews">
						<div id="pr-reviewsnippet-top-<?php echo $pageId ?>" class="stars-snippet category-page"></div>
						<a href="/add-review?post_id=<?php echo $recipeId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="btn btn-link btn-write-review-link">Write a Review</a>
					</div>

					<div class="recipe-description">
						<div class="d-none d-md-block">
							<p<?php if (strlen($recipeDescription) < 100) : ?> class="showing"<?php endif; ?>><?php echo $recipeDescription; ?></p>
							<?php if (strlen($recipeDescription) > 100) : ?>
							<div class="cont-show-more">
								<a href="#" title="Read More" class="btn btn-link read-more">Read More</a>
							</div>
							<?php endif; ?>
						</div><!-- /.d-none -->

						<div class="d-block d-md-none">
							<p<?php if (strlen($recipeDescription) < 100) : ?> class="showing"<?php endif; ?>><?php echo $recipeDescription; ?></p>
							<?php if (strlen($recipeDescription) > 100) : ?>
							<div class="cont-show-more">
								<a href="#" title="Read More" class="btn btn-link read-more">Read More</a>
							</div>
							<?php endif; ?>
						</div><!-- /.d-block -->
					</div><!-- /.recipe-description -->

					<div class="recipe-information">
						<?php if( $recipeServings ): ?>
						<div class="recipe-servings information-item">
							<label>Servings</label>
							<span><?php echo $recipeServings ?></span>
						</div><!-- /.recipe-servings -->
						<?php endif; ?>

						<?php if( $recipeCaloriesServings ): ?>
						<div class="recipe-calories information-item">
							<label>Calories</label>
							<span><?php echo $recipeCaloriesServings ?></span>
						</div><!-- /.recipe-calories -->
						<?php endif; ?>

						<?php if( $recipePrepTime ): ?>
						<div class="recipe-prep-time information-item">
							<label>Prep Time</label>
							<span><?php echo $recipePrepTime ?></span>
						</div><!-- /.recipe-prep-time -->
						<?php endif; ?>
						
						<?php if( $recipeCookTime ): ?>
						<div class="recipe-cook-time information-item">
							<label>Cook Time</label>
							<span><?php echo $recipeCookTime ?></span>
						</div><!-- /.recipe-cook-time -->
						<?php endif; ?>

						<?php if( $recipeTotalTime ): ?>
						<div class="recipe-total-time information-item">
							<label>Total Time</label>
							<span><?php echo $recipeTotalTime ?></span>
						</div><!-- /.recipe-total-time -->
						<?php endif; ?>

					</div><!-- /.recipe-information -->

				</header><!-- .entry-header -->
			</div><!-- /.col-12 -->
		</div><!-- /.row -->

		<div class="cont-columns">
			<div class="row">
				<div class="col-lg-4 col-12">
					<div class="column col-left col-ingredients">
						<h4>Ingredients</h4>
						<?php echo get_post_field('post_content', $recipeId); ?>
					</div><!-- /.col-left col-ingredients -->
				</div><!-- /.col-lg-4 col-12 -->
				<div class="col-lg-8 col-12">
					<div class="column col-right col-directions">
						<h4>Directions</h4>
						<?php the_field('sals-recipe-directions', $recipeId); ?>
					</div><!-- /.col-right col-directions -->
				</div><!-- /.col-lg-8 col-12 -->
			</div><!-- /.row -->
		</div><!-- /.cont-columns -->

		<div class="recipe-share">
			<div class="row justify-content-end align-items-center">
				<div class="col-12 col-lg-4">
					<?php echo do_shortcode( '[ssba-buttons]' ); ?>
				</div><!-- /.col-12 -->
				<div class="col-12 col-lg-2 p-0">
					<p class="date">Recipe by Herdez<sup>®</sup><br/><?php echo $recipeDate; ?></p>
				</div><!-- /.col-12 -->
				<div class="col-12 col-lg-3">
					<a href="<?php echo $site_url; ?>/recipes" title="View More Herdez Recipes" class="btn btn-main">View More <br />Herdez Recipes</a>
				</div><!-- /.col-12 -->
				<div class="col-12 col-lg-3">
					<a href="/recipes" title="View Other Mexican Recipes" class="btn btn-main">View Other <br />Mexican Recipes</a>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
		</div><!-- /.recipe-share -->
	</div><!-- /.container -->
		
	<?php if ($recipeVideo): ?>
	<!-- Modal -->
	<div class="modal fade recipe-video" id="recipeVideo" tabindex="-1" role="dialog" aria-labelledby="recipeVideoTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="cont-popup-recipe-video">
						<?php echo $recipeVideo; ?>
					</div><!-- /.cont-recipe-video -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>

</section><!-- #post-## -->


<div class="main-cont-reviews">
	<div class="container">
		<div class="cont-reviews">
			<div class="row">
				<div class="col-12">
					<h2>Reviews</h2>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
			
			<div class="row columns">
				<div class="col-md-5 col-12">
					<div id="pr-reviewsnippet-<?php echo $pageId; ?>" class="stars-snippet"></div>
					<div id="pr-reviewhistogram" class="p-w-r">
						<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
					</div>
					<div class="cont-write-review"></div><!-- /.cont-write-review -->

				</div><!-- /.col-5 -->
				<div class="col-md-7 col-12">
					<div id="pr-reviewdisplay-<?php echo $pageId; ?>" class="cont-review-display"></div>
				</div><!-- /.col-7 -->
			</div><!-- /.row -->

		</div><!-- /.cont-reviews -->
	</div><!-- /.container -->
</div><!-- /.container-fluid -->

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>

<script type="text/javascript" charset="utf-8">
	POWERREVIEWS.display.render(
		[
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php echo $pageId; ?>',
				review_wrapper_url: '/add-review?post_id=<?php echo $recipeId ?>&pr_page_id=<?php echo $pageId; ?>',
				style_sheet: '<?php echo $salsas_domain; ?>/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
				components: {
					CategorySnippet: 'pr-reviewsnippet-top-<?php echo $pageId; ?>',
				}
			},
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php echo $pageId; ?>',
				review_wrapper_url: '/add-review?post_id=<?php echo $recipeId ?>&pr_page_id=<?php echo $pageId; ?>',
				style_sheet: '<?php echo $salsas_domain; ?>/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
				on_render: function(config, data) {
					jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
					jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn btn-write-review');
				},
				components: {
					ReviewSnippet: 'pr-reviewsnippet-<?php echo $pageId; ?>',
					ReviewDisplay: 'pr-reviewdisplay-<?php echo $pageId; ?>'
				}
			}
		]
	);
</script>

<?php else: ?>
	<div class="container">
		<div class="row">
			<div class="col-12 my-5 text-center">
				<h3 class="mb-5">This recipe does not belong to this brand.</h3>
				<a href="/recipes" class="btn btn-main">View Other <br />Mexican Recipes</a>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
<?php endif; ?>
<?php restore_current_blog(); ?>


<?php get_footer(); ?>

