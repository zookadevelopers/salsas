var $ = jQuery;

$(document).ready(function () {
    $('#carousel-home').owlCarousel({
        items:1,
        nav:true,
        loop: true,
        navText : [
        "<img class='img-fluid' width='50%' src='"+wnm_custom.template_url+"/src/img/arrow-white-left.png' />",
         "<img class='img-fluid' width='50%' src='"+wnm_custom.template_url+"/src/img/arrow-white-right.png' />"
        ]
    });

    $('.all-products-carousel').owlCarousel({
        dots: false,
        loop: false,
        navText : [
        "<img src='"+wnm_custom.template_url+"/src/img/arrow-left.png' />",
         "<img src='"+wnm_custom.template_url+"/src/img/arrow-right.png' />"
        ],
        responsive:{
            0:{
                items:1,
                nav:true,
                stagePadding: 100,
                margin: 10,
                center:true,
            },
            600:{
                items:3,
                nav:true,
                margin: 20,
            },
            1000:{
                items:5,
                nav:true,
                margin: 20,
            }
        }
    });

    $('.tnp-email').attr('placeholder', 'Email...');



    // date list review
    setTimeout(function(){ 
        $(".pr-review").each( function() {
            var time = $(this).children(".pr-rd-description").children(".pr-rd-side-content-block").children(".pr-rd-reviewer-details").children(".pr-rd-author-submission-date").children("time");
            $(this).children(".pr-rd-header").children(".pr-rd-star-rating").children("div").children(".pr-snippet-stars").append(time);

            var from = $(this).children(".pr-rd-description").children(".pr-rd-side-content-block").children(".pr-rd-reviewer-details").children(".pr-rd-author-location").children("span");
            $(this).children(".pr-rd-description").children(".pr-rd-side-content-block").children(".pr-rd-reviewer-details").children(".pr-rd-author-nickname").append(from);
        });
    }, 1000);

    // change date btn pagination
    $(document).on("click", ".pr-rd-pagination-btn", function() {
        setTimeout(function(){ 
            $(".pr-review").each( function() {
                var time = $(this).children(".pr-rd-description").children(".pr-rd-side-content-block").children(".pr-rd-reviewer-details").children(".pr-rd-author-submission-date").children("time");
                $(this).children(".pr-rd-header").children(".pr-rd-star-rating").children("div").children(".pr-snippet-stars").append(time);

                var from = $(this).children(".pr-rd-description").children(".pr-rd-side-content-block").children(".pr-rd-reviewer-details").children(".pr-rd-author-location").children("span");
                $(this).children(".pr-rd-description").children(".pr-rd-side-content-block").children(".pr-rd-reviewer-details").children(".pr-rd-author-nickname").append(from);
            });
        }, 1000);
        console.log('change ok');
    });

    $(document).on('click', '.items-all-products a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });

    $('.sizes-list [data-size]').on('click', function(e){
        e.preventDefault();

        var $this = $(this);
        var body = $('body');
        var thisParent = $this.parent();
        var thisSize = $this.data('size');
        var thisPosition = $this.data('position');
        var parentList = $('.sizes-list');
        
        var currentActiveElements = body.find('[data-size='+thisSize+'][data-status="active"]');
        var inactiveElements = body.find('[data-size='+thisSize+'][data-status="inactive"]');
        var activeElements = body.find('[data-status="active"]');

        if (!thisParent.hasClass('active')) {
            parentList.find('li').removeClass('active');
            thisParent.addClass('active');
        }

        if (currentActiveElements.data('status') != 'active') {
            $.each( inactiveElements, function(index){
                $(this).attr('data-status', 'active');
            });

            $.each( activeElements, function(index){
                $(this).attr('data-status', 'inactive');
            });
        }
    });

    //loader
    $(window).load(function() {
        $(".loader").fadeOut("slow");
         var windowWidth = $(window).width();
         
        // append placeholder to search imput in other brands collapse
        $('.top-header input').attr('placeholder', 'Search other Mexican recipes')
        $('.cont-mobile-menu .search-form input').attr('placeholder', 'Search Búfalo ')

        // Mobile Version
        if (windowWidth <= 991) {

            //.dropdown-brands-salsas
            $(".btn-other-brands").click(function(e) {
                e.preventDefault();
                $('.other-brands-collapse').toggleClass('d-block');
            });

            // Mobile menu activate swipe function
            $('.cont-mobile-menu').slideAndSwipe();

            // filters opotions toggle function
            var mainBrands = $('.left-filters .cont-options .sf-field-category > ul > li > label');

            for (var i = 0; i < mainBrands.length; i++) {
                mainBrands[i].addEventListener('click', function(event) {
                    event.preventDefault();
                    this.classList.toggle('active');
                    var content = this.nextElementSibling;
                    if (content.style.display === 'block') {
                        content.style.display = 'none';
                    } else {
                        content.style.display = 'block';
                    }
                });
            }
        }  else {
            //.dropdown-brands-salsas
            $(".dropdown-salsas").hover(
                function() {
                    $('.other-brands-collapse').finish().slideDown('medium');
                },
                function() {
                    $('.other-brands-collapse').finish().slideUp('medium');
                }
            );
        }
    });

    //Back to top button functionallity
    (function($, window, document){

        var backToTopButton = $('.back-top');

        function backToTop(selector, time) {
            $('html, body').animate({
                scrollTop: $(selector).offset().top -30
            }, time);
        }

        $(document).scroll(function(){
            if($(window).scrollTop() + $(window).height() == $(document).height() || $(this).scrollTop() >= 700){
                backToTopButton.fadeIn();
            }else{
                backToTopButton.fadeOut();
            }
        });

        backToTopButton.on('click', function() {
            backToTop('body', 400);
            return false;
        });

    })(jQuery, window, document);

    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
       jQuery('.img-product').each(function(){
            var t = jQuery(this),
                s = 'url(' + t.attr('src') + ')',
                p = t.parent(),
                d = jQuery('<div></div>');

            p.prepend(d);
            d.css({
                'width'                 : t.css('width'),
                'height'                : t.css('height'),
                'margin'                : 'auto',
                'background-size'       : 'contain',
                'background-repeat'     : 'no-repeat',
                'background-position'   : '50% 50%',
                'background-image'      : s
            });
            t.hide();
        }); 
    } 
    $('#write-review').on('click', function() {
       var target = $('#content .cont-write-review a').attr('href');
       window.location.href = document.location.origin+target;
   });  

    /* Read more or less in recipe page */
    var btn_read = $('.recipe-description .read-more');
    btn_read.on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        $('.recipe-description p').toggleClass('showing');
        $this.text($this.text() == 'Read More' ? 'Read Less' : 'Read More');
    });

    if(window.location.href.indexOf("product") > -1) {
        var productsLink = $('.main-header.navbar-light .navbar-nav .nav-item').find('a[href*="products"]');

        productsLink.parent().addClass('active');
    }

    $("#simple-menu").prependTo("#masthead");

    $('#is-product-content').hide();
    $('[class*="acceptance"]').on('click', function(){
        var contactSpecificProduct = $('[type="checkbox"][name="acceptance-840"]').attr('checked') ? true:false;
        
        if (contactSpecificProduct == true) {
            $('#is-product-content').show();
        } else {
            $('#is-product-content').hide();
        }
    });
 });