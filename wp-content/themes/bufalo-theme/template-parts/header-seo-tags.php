<?php if ( is_single() ) : ?>
<meta name="description" content="<?php the_field('product_meta_description'); ?>"/>
<link rel="canonical" href="<?php echo get_permalink(get_the_ID(), false); ?>" />
<meta property="og:description" content="<?php the_field('product_meta_description'); ?>" />
<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="Búfalo" />
<meta property="og:title" content="<?php the_field('product-name'); ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo get_permalink(get_the_ID(), false); ?>" />
<meta property="article:published_time" content="<?php the_time('c'); ?>" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:description" content="<?php the_field('product_meta_description'); ?>" />
<meta name="twitter:image" content="<?php the_post_thumbnail_url(); ?>">
<meta name="twitter:title" content="<?php the_field('product-name'); ?>" />
<?php endif ?>
<?php if (is_page_template('page-recipe.php')): ?>
<?php 
	switch_to_blog(1); 
	$slug = $wp_query->query_vars['id'];

	$args = array(
		'name'        => $slug,
		'post_type'   => 'post',
		'post_status' => 'publish',
		'numberposts' => 1
	);

	$my_posts = get_posts($args);

	$recipeId = $my_posts[0]->ID;
?>
<meta name="robots" content="noindex,nofollow">
	<meta name="description" content="<?php the_field('sals-recipe-description', $recipeId); ?>"/>
	<link rel="canonical" href="<?php the_field('recipe-bufalo-canonical-url', $recipeId); ?>" />
	<meta property="og:description" content="<?php the_field('sals-recipe-description', $recipeId); ?>" />
	<meta property="og:image" content="<?php the_field('recipe-herdez-image', $recipeId); ?>" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:site_name" content="Búfalo" />
	<meta property="og:title" content="<?php the_field('sals-recipe-title', $recipeId); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo get_permalink($recipeId, false); ?>" />
	<meta property="article:published_time" content="<?php the_time('c'); ?>" />
	<meta property="article:section" content="Single Recipe by Búfalo" />
	<meta property="article:tag" content="Búfalo Recipe" />	
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:description" content="<?php the_field('sals-recipe-description', $recipeId); ?>" />
	<meta name="twitter:image" content="<?php the_field('recipe-herdez-image', $recipeId); ?>">
	<meta name="twitter:title" content="<?php the_field('sals-recipe-title', $recipeId); ?>" />
<?php restore_current_blog(); endif; ?>