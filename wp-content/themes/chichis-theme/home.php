<?php 
// Template Name: Home chichis


$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$site_url = get_site_url();

// Products Query
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
		'orderby'=>'menu_order'
	)
); 
?>

<?php get_header(); ?>
<section class="banner-home container-fluid">
	<div class="row">
		<div class="col-12 p-0">
			<div id="carousel-home" class="owl-carousel owl-theme">

				<?php if( have_rows('banner_slide') ): ?>

				<?php while( have_rows('banner_slide') ): the_row(); 

					// vars
					$image = get_sub_field('banner-image');
					$content = get_sub_field('banner-content');
					$buttonLabel = get_sub_field('banner-button');
					$link = get_sub_field('banner-link');
					$buttonLabel2 = get_sub_field('banner-button-label-2');
					$buttonLink2 = get_sub_field('banner-button-link-2');
					$full = get_sub_field('full_banner');

					$alt = $image['alt'];
					$title = $image['title']; 

					?>

					<div class="item row m-0">

						<?php if ($full) { ?>
							
							<div class="col-12 p-0">
								<div class="full-banner">
									<div class="img-recipe w-100" title="<?php echo ($alt) ? $alt : $title ?>" style="background-image: url('<?php echo $image['url']; ?>');">
										<div class="banner-content">
											<div class="row w-100 align-items-end">
												<div class="col-md-9">
													<h2 class="text"><?php echo $content; ?></h2>
												</div><!-- /.col-6 -->
												<?php if ($link || $buttonLink2): ?>
												<div class="col">
													<div class="buttons">
														<?php if ($link): ?>
														<a class="btn btn-chis-white mr-3" title="<?php echo $buttonLabel; ?>" href="<?php echo $link; ?>"><?php echo $buttonLabel; ?></a>	
														<?php endif; ?>
														<?php if ($buttonLink2): ?>
														<a class="btn btn-chis-white" title="<?php echo $buttonLabel2; ?>" href="<?php echo $buttonLink2; ?>"><?php echo $buttonLabel2; ?></a>
														<?php endif; ?>
													</div><!-- /.buttons -->
												</div><!-- /.col-6 -->
												<?php endif; ?>
											</div><!-- /.row -->
										</div><!-- /.banner-content -->
									</div>
								</div><!-- /.full-banner -->
							</div>
							
						<?php } else { ?>
							<div class="col-12 col-md-5 p-0">
								<div class="slide-description">
									<h2 class="text"><?php echo $content; ?></h2>
									
									<?php if ($link || $buttonLink2): ?>
									<div class="buttons">
										<?php if ($link): ?>
										<a class="btn btn-chis-white mar-aut" title="<?php echo $buttonLabel; ?>" href="<?php echo $link; ?>"><?php echo $buttonLabel; ?></a>
										<?php endif; ?>
										<?php if ($buttonLink2): ?>
										<a class="btn btn-chis-white mar-aut" title="<?php echo $buttonLabel2; ?>" href="<?php echo $buttonLink2; ?>"><?php echo $buttonLabel2; ?></a>
										<?php endif; ?>
									</div><!-- /.buttons -->
									<?php endif; ?>

								</div><!-- /.slide-description -->
							</div><!-- /.col-12 col-sm-6 -->
							<div class="col-12 col-md-7 p-0">
								<div class="img-recipe banner-top" title="<?php echo ($alt) ? $alt : $title ?>" style="background-image: url('<?php echo $image['url']; ?>');"></div>
							</div><!-- /.col-12 col-sm-6 -->
						<?php } ?>
					</div>

				<?php endwhile; ?>

				<?php endif; ?>

			</div>
		</div>
	</div>
</section>

<section class="title">
	<h1 class="text-uppercase">Bringing people together for fun and flavorful food.</h1>
</section><!-- /.title -->

<section class="products-home">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<h3 class="text-uppercase">All Products</h3>
			</div>
		</div>
		<div class="row items-products align-items-end">
			<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
				<?php
					$image_id = get_post_thumbnail_id();
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
					$image_title = get_the_title($image_id);
				?>
				<div class="col-6 col-md text-center item">
					<a class="link-product" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
						</figure>
						<p class="text-uppercase"><?php the_title(); ?></p>
					</a>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="title">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h3 class="text-uppercase">Featured Recipes</h3>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container-fluid p-0">
			<?php 
				$id_page = 1;
				$num_post = 0;
				$slug_page = 'chi-chis';

				switch_to_blog($id_page); 
				// Get posts for each blog
				$myposts = get_posts( 
					array( 
						'category_name'  => $slug_page,
						'posts_per_page' => -1, 
						'order' => 'ASC'
					)
				);
				// Loop for each blog
				global $post;
				$num_post = 1;
				
				foreach( $myposts as $post ) { 
					if ($num_post > 3) {
						break;
					} ?>

					<?php
						//$pageId = get_the_ID();
						$recipeId = get_the_ID();
						$pageId = get_field('page_id', $recipeId);
						$name = get_the_title();
						$imgUrl = get_the_post_thumbnail_url();
						$currentUrl = get_permalink();
						$allposttags = get_the_tags();
						$currentUrl = home_url( $wp->request );
						$recipeSlug = $post->post_name;

						$objPowerReviews[] = [
							'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
							'locale' => 'en_US',
							'merchant_group_id' => '78368',
							'merchant_id' => '278593',
							'page_id' => strval($pageId),
							'style_sheet' => '/wp-content/themes/chichis-theme/inc/assets/css/custom/reviews.css',
							'components' => [
								'CategorySnippet' => 'pr-reviewsnippet-feature-'.$pageId
							]
						];
						$image_id = get_post_thumbnail_id();
						$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
						$image_title = get_the_title($image_id);
					?>

					<div class="row recipes m-0 <?php if ($num_post == 2) { echo 'flex-row-reverse';} ?>">
						<div class="col-12 col-md-6 p-0">
							<div class="recipes-item">
								<div class="info">
									<p class="text"><?php the_field('sals-recipe-title'); ?></p>
									<div id="pr-reviewsnippet-feature-<?php echo $pageId; ?>" class="stars-snippet"></div>
									<a class="btn btn-chis-white" title="View Recipe" href="<?php echo $site_url; ?>/recipe/<?php echo $recipeSlug; ?>">View Recipe</a>
								</div><!-- /.text -->
							</div><!-- /.cont-recipe -->
						</div>
						<div class="col-12 col-md-6 p-0">
							<figure>
								<?php if (get_the_post_thumbnail_url()) { ?>
									<img class="img-fluid" width="auto" height="auto" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
								<?php }else{ ?>
									<img class="img-fluid" width="auto" height="auto" src="<?php echo $upload_theme ?>recipe.jpg" alt="<?php the_title(); ?>" />
								<?php }  ?>
							</figure>
						</div><!-- /.col-12 col-md-6 -->
					</div>
				<?php 
				$num_post++;  }
				restore_current_blog();
			?>
		</div>
	</div>
	<div class="show-more">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h3><a href="<?php echo get_site_url(); ?>/recipes" title="See More Recipes">See More Recipes</a></h3>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.show-more -->
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>
<?php 
 get_footer();
