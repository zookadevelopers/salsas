var $ = jQuery;

$(document).ready(function () {
	function removeVid() {
		$('.vid-box').remove()
		$('body').removeClass('vid-open')
		
	}

	function fullPlay(vidUrl) {
		removeVid()
		if(vidUrl.indexOf('youtube') > 0) {
			var vidCode = '<div class="vid-box"><div class="buttons"><a href="#" id="close">&times;</a></div><div class="vid-container"><div class="vid-area">' + 
			'<iframe src="' + vidUrl + '?autoplay=1&showinfo=0&modestbranding=1&rel=0&showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>' +
			'</div></div></div>'
			
			$('body').append(vidCode)
		}
		else {
			var vidCode = '<div class="vid-box"><div class="buttons"><a href="#" id="close">&times;</a></div><div class="vid-container"><div class="vid-area">' + 
			'<img alt="so good" class="recipe-img" src="' + vidUrl + '" />' +
			'</div></div></div>'
			
			$('body').append(vidCode)
		}
		$('body').addClass('vid-open')
	}

	$('.vid-link').click(function(evt) {
		evt.preventDefault()
		var url = $(this).attr('data-url')
		fullPlay(url)
		
		var $recipe = $(this).find('.recipe')
		if($recipe.length > 0) {
			$('.vid-container').append($recipe.html())
		}
	})

	$('body').on('click', '#close, .vid-box', function(evt) {
		evt.preventDefault()
		removeVid()
	})

	$('body').on('keydown', function(evt) {
		if(evt.keyCode == 27) {
			removeVid()
		}
	})
});
