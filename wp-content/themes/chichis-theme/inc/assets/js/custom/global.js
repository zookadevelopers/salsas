var $ = jQuery;

function transforImagesIE() {
	/* FIX IMAGES IN IE */
	if (document.documentMode || /Edge/.test(navigator.userAgent)) {
		$('html').addClass('ie-browser');

		jQuery('.banner-home .img-fluid').each(function(){
			var t = jQuery(this),
				s = 'url(' + t.attr('src') + ')',
				p = t.parent(),
				d = jQuery('<div class="banner-img-custom img-ie"></div>');

			p.prepend(d);
			d.css({
				'width'                 : '100%',
				'height'                : '530',
				'margin'                : 'auto',
				'background-size'       : 'cover',
				'background-repeat'     : 'no-repeat',
				'background-position'   : '50% 50%',
				'background-image'      : s
			});
			t.hide();
		});

		jQuery('.featured-recipes-home .img-fluid').each(function(){
			var t = jQuery(this),
				s = 'url(' + t.attr('src') + ')',
				p = t.parent(),
				d = jQuery('<div class="img-ie"></div>');

			p.prepend(d);
			d.css({
				'width'                 : '100%',
				'height'                : t.css('height'),
				'margin'                : 'auto',
				'background-size'       : 'cover',
				'background-repeat'     : 'no-repeat',
				'background-position'   : '50% 50%',
				'background-image'      : s
			});
			t.hide();
		}); 

		

		jQuery('.all-products-carousel .item .link-product figure .img-product').each(function(){
			var t = jQuery(this),
				s = 'url(' + t.attr('src') + ')',
				p = t.parent(),
				d = jQuery('<div class="img-ie"></div>');

			p.prepend(d);
			d.css({
				'width'                 : '100%',
				'height'                : '220',
				'margin'                : 'auto',
				'background-size'       : 'contain',
				'background-repeat'     : 'no-repeat',
				'background-position'   : 'center',
				'background-image'      : s
			});
			t.hide();
		}); 
	}
}

$(document).ready(function () {
	transforImagesIE();

	$(window).on('resize', function() {
		var win = $(this); //this = window
		if (win.width() < 1200) {
			$('.img-ie').remove();
			transforImagesIE();
		}
	});

	$('#carousel-home').owlCarousel({
		items: 1,
		nav: true,
		loop: true,
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		transitionStyle: "fade"
	});

	$('.all-products-carousel').owlCarousel({
		nav: false,
		dots: false,
		loop: false,
		margin: 20,
		responsive:{
			0:{
				items: 1
			},
			600:{
				items: 3
			},
			1000:{
				items: 3
			}
		}
	});

	$('.tnp-email').attr('placeholder', 'Email...');

	$(document).on('click', '.items-all-products a[href^="#"]', function (event) {
		event.preventDefault();

		$('html, body').animate({
			scrollTop: $($.attr(this, 'href')).offset().top
		}, 500);
	});

	$('.sizes-list [data-size]').on('click', function(e){
		e.preventDefault();

		var $this = $(this);
		var body = $('body');
		var thisParent = $this.parent();
		var thisSize = $this.data('size');
		var thisPosition = $this.data('position');
		var parentList = $('.sizes-list');
		
		var currentActiveElements = body.find('[data-size='+thisSize+'][data-status="active"]');
		var inactiveElements = body.find('[data-size='+thisSize+'][data-status="inactive"]');
		var activeElements = body.find('[data-status="active"]');

		if (!thisParent.hasClass('active')) {
			parentList.find('li').removeClass('active');
			thisParent.addClass('active');
		}

		if (currentActiveElements.data('status') != 'active') {
			$.each( inactiveElements, function(index){
				$(this).attr('data-status', 'active');
			});

			$.each( activeElements, function(index){
				$(this).attr('data-status', 'inactive');
			});
		}
	});

	$(window).load(function() {
	    $(".loader").fadeOut("slow");
	    var windowWidth = $(window).width();

	    // append placeholder to search imput in other brands collapse
	    $('.top-header input').attr('placeholder', 'Search other Mexican recipes');

	    // Mobile Version
	    if (windowWidth <= 991) {

	    	//.dropdown-brands-salsas
            $(".btn-other-brands").click(function(e) {
                e.preventDefault();
                $('.other-brands-collapse').toggleClass('d-block');
            });

	        // Mobile menu activate swipe function
	        $('.cont-mobile-menu').slideAndSwipe();

	        // filters options toggle function
	        var mainBrands = $('.left-filters .cont-options .sf-field-category > ul > li > label');

	        for (var i = 0; i < mainBrands.length; i++) {
	            mainBrands[i].addEventListener('click', function(event) {
	                event.preventDefault();
	                this.classList.toggle('active');
	                var content = this.nextElementSibling;
	                if (content.style.display === 'block') {
	                    content.style.display = 'none';
	                } else {
	                    content.style.display = 'block';
	                }
	            });
	        }

	        // Reeplace text for products link in mobile menu
	        $('.cont-mobile-menu .navbar-nav > .nav-item .dropdown-menu li:first-child > a').text('All Products');
	    } else {
            //.dropdown-brands-salsas
            $(".dropdown-salsas").hover(
                function() {
                    $('.other-brands-collapse').finish().slideDown('medium');
                },
                function() {
                    $('.other-brands-collapse').finish().slideUp('medium');
                }
            );
        }
	});

	// custom dropdown for desktop main menu
	$(function($) {
		var headerDropdownLink = $('header .navbar-nav > .nav-item.dropdown');
		var headerDropdown = headerDropdownLink.find('.dropdown-menu .dropdown-item:first-child');
		headerDropdownLink.hover(function() {
			$(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
		}, function() {
			$(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
		});

		headerDropdownLink.find('> a').click(function(){
			location.href = headerDropdown.attr('href');
		});
	});

	//Back to top button functionallity
	(function($, window, document){

		var backToTopButton = $('.back-top');

		function backToTop(selector, time) {
			$('html, body').animate({
				scrollTop: $(selector).offset().top -30
			}, time);
		}

		$(document).scroll(function(){
			if($(window).scrollTop() + $(window).height() == $(document).height() || $(this).scrollTop() >= 700){
				backToTopButton.fadeIn();
			}else{
				backToTopButton.fadeOut();
			}
		});

		backToTopButton.on('click', function() {
			backToTop('body', 400);
			return false;
		});

	})(jQuery, window, document);

	$('.button-play').on('click', function(e) {
		$('.recipe-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
	});

	$('.recipe-video [data-dismiss="modal"]').on('click', function(e) {
		$('.recipe-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
	});

	/* R E V I E W S */
	$('#write-review').on('click', function() {
	   var target = $('#content .cont-write-review a').attr('href');
	   window.location.href = document.location.origin+target;
	});

	/* V A  L I D A T E   S I Z E   P R O D U C T */
	var validateSize = $('ul.sizes-list li a[data-size="16"]');
	if (validateSize.length) {
		$('ul.sizes-list li').removeClass('active');
		validateSize.click();
	}

	/* Read more or less in recipe page */
	var btn_read = $('.recipe-description .read-more');
	btn_read.on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		$('.recipe-description p').toggleClass('showing');
		$this.text($this.text() == 'Read More' ? 'Read Less' : 'Read More');
	});
	
});
