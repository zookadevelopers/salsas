<?php 
function cptui_register_my_cpts() {

 	/**
     * Post Type: Categories.
     */

    $labels = array(
        "name" => __( "Categories", "wp-chichis-theme" ),
        "singular_name" => __( "Category", "wp-chichis-theme" ),
        "add_new" => __( "Add New Category", "wp-chichis-theme" ),
    );

    $args = array(
        "label" => __( "Categories", "wp-chichis-theme" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "products", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "/herdez/wp-content/uploads/sites/4/2018/11/icon-product.png",
        "supports" => array( "title", "editor", "thumbnail", "custom-fields", "page-attributes" ),
    );

    register_post_type( "products", $args );

	/**
	 * Post Type: Salsas & Dips.
	 */

	$labels = array(
		"name" => __( "Salsas & Dips", "wp-chichis-theme" ),
		"singular_name" => __( "Salsas & Dips", "wp-chichis-theme" ),
	);

	$args = array(
		"label" => __( "Salsas & Dips", "wp-chichis-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/salsas-dips", "with_front" => true ),
		"query_var" => "salsas-dips",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "salsas_dips", $args );

	/**
	 * Post Type: Chiles & Mixes.
	 */

	$labels = array(
		"name" => __( "Chiles & Mixes", "wp-chichis-theme" ),
		"singular_name" => __( "Chiles & Mixes", "wp-chichis-theme" ),
	);

	$args = array(
		"label" => __( "Chiles & Mixes", "wp-chichis-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/chiles-mixes", "with_front" => true ),
		"query_var" => "chiles_mixes",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "chiles_mixes", $args );

	/**
	 * Post Type: Tortillas & Chips.
	 */

	$labels = array(
		"name" => __( "Tortillas & Chips", "wp-chichis-theme" ),
		"singular_name" => __( "Tortilla & Chips", "wp-chichis-theme" ),
	);

	$args = array(
		"label" => __( "Tortillas & Chips", "wp-chichis-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/tortillas-chips", "with_front" => true ),
		"query_var" => "tortillas-chips",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "tortillas_chips", $args );

	/**
	 * Post Type: Tortillas.
	 */

	$labels = array(
		"name" => __( "Tortillas", "wp-chichis-theme" ),
		"singular_name" => __( "Tortilla", "wp-chichis-theme" ),
	);

	$args = array(
		"label" => __( "Tortillas", "wp-chichis-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/tortillas", "with_front" => true ),
		"query_var" => "tortillas",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "tortillas", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
