<?php 
// Template Name: Page About Us
get_header();
?>
<?php
	global $bannerUrl;

	$banner_value = get_field('banner_image');

	if( $banner_value ) {
		$bannerUrl = $banner_value;
	} else {
		$bannerUrl = get_template_directory_uri().'/inc/assets/img/about-us/chi-chis-banner-image-qbout-us-desktop.png';
	}

	$twoFullColumns = have_rows('two_columns_section');
?>
<div class="about-us-page">
	<section class="banner position-relative" title="About Us Page" style="background-image: url('<?php echo $bannerUrl; ?>')"></section><!-- /.banner -->

	<section class="content">
		<div class="container-fluid">
			<div class="row justify-content-center bg-texture">
				<div class="col-md-8">
					<h1 class="text-uppercase"><?php the_field('main_title'); ?></h1>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->

			<?php if( $twoFullColumns ): ?>
			<?php while( $twoFullColumns ): the_row(); ?>
				<?php
				global $columnImageUrl;

				$columnImage = get_sub_field('image');

				if( $columnImage ) {
					$columnImageUrl = $columnImage;
				} else {
					$columnImageUrl = get_template_directory_uri().'/inc/assets/img/about-us/chichis-aboutus-image.jpg';
				}
				?>

			<div class="row mb-4 flex-column-reverse flex-md-row">
				<div class="col-lg-6 px-0">
					<div class="proud-home">
						<h2><?php echo get_sub_field('text'); ?></h2>
					</div><!-- /.proud-home -->
				</div><!-- /.col-lg-6 -->
				<div class="col-lg-6 px-0">
					<img src="<?php echo $columnImageUrl; ?>" alt="About Us Page" class="img-fluid proud-img" widht="auto" height="auto" />
				</div><!-- /.col-lg-6 -->
			</div><!-- /.row -->
			<?php break; ?>
			<?php endwhile; ?>
			<?php endif; ?>

			<?php if( have_rows('two_columns') ): ?>
				<?php global $columnsImageUrl; ?>
				<?php $count = 0; while( have_rows('two_columns') ): the_row(); $count++; ?>

					<?php
					$text = get_sub_field('text');
					$image = get_sub_field('image');

					if( $image ) {
						$columnsImageUrl = $image;
					} else {
						$columnsImageUrl = get_template_directory_uri().'/inc/assets/img/about-us/chichis-aboutus-image-seafoodenchilada.jpg';
					}
					?>

					<div class="row justify-content-center">
						<div class="col-lg-9">
							<div class="row justify-content-center align-items-center<?php if ($count % 2==0): ?> flex-lg-row-reverse<?php endif ?>">
								<div class="col-lg-6 mt-5">
									<img src="<?php echo $columnsImageUrl; ?>" alt="About Us Page" class="img-fluid<?php if ($count % 2==0): ?> rigth-img<?php else: ?> left-img<?php endif; ?>" widht="auto" height="auto" />
								</div><!-- /.col-lg-6 -->
								<div class="col-lg-6 mt-5">
									<h3 class="<?php if ($count % 2==0): ?>mr-md-5 pr-md-4<?php else: ?>ml-md-5 pl-md-4<?php endif; ?>"><?php echo $text; ?></h3>
								</div><!-- /.col-lg-6 -->
							</div><!-- /.row -->
						</div><!-- /.col-lg-9 -->
					</div><!-- /.row -->

				<?php endwhile; ?>
			<?php endif; ?>
			
		</div><!-- /.container-fluid -->
	</section><!-- /.content -->
</div><!-- /.about-us-page -->

<?php 
 get_footer();