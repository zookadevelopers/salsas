<?php 

// Template Name: Fiesta Style
get_header();

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>



<section class="fiesta">
	<div class="underlay">
		<img src="<?php echo $upload_theme; ?>fiesta-style/dancing-people.gif" width="auto" height="auto" alt="Fiesta Dancing People" />
	</div>	

	<?php 
		$img_top = get_field('top_image');
		$alt = $img_top['alt'];
		$title = $img_top['title']; 
	?>
	<img class="main" src="<?php echo $img_top['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />

	<?php 
		$img_logo = get_field('top_logo');
		$alt = $img_logo['alt'];
		$title = $img_logo['title']; 
	?>
	<div class="fiesta-logo">
		<img src="<?php echo $img_logo['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />
	</div>
</section>

<section class="musica-for-your-mouth">
	<img class="top" alt="Fiesta Style" src="<?php echo $upload_theme; ?>fiesta-style/blue-rip-top.png" width="auto" height="auto" />
	<div class="content">
		<?php 
			$des_top_img = get_field('description_top_img');
			$alt = $des_top_img['alt'];
			$title = $des_top_img['title']; 
		?>
		<img class="d-none d-md-block big" src="<?php echo $des_top_img['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />

		<?php 
			$des_top_img_mob = get_field('description_top_img_mobile');
			$alt = $des_top_img_mob['alt'];
			$title = $des_top_img_mob['title']; 
		?>
		<img class="d-block d-md-none  small" src="<?php echo $des_top_img_mob['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />

		<?php the_field('description_text'); ?>

		<h4 class="par-custom"><?php the_field('description_text_bottom'); ?></h4>
		
	</div>
	<img class="bottom" alt="Fiesta Style" src="<?php echo $upload_theme; ?>fiesta-style/blue-bottom-rip.png" width="auto" height="auto" />
</section>

<section class="three-salsas">
	<div class="container-fluid">
		<div class="three-info">
			<div class="row">
				<div class="col-md-4">
					<?php if( have_rows('salsas_group1') ): ?>
						<?php while( have_rows('salsas_group1') ): the_row(); ?>
							
							<?php 
								$group_img = get_sub_field('image');
								$alt = $group_img['alt'];
								$title = $group_img['title']; 
								
								$group_imgTitle = get_sub_field('image_title');
								$altTitle = $group_imgTitle['alt'];
								$titleTitle = $group_imgTitle['title']; 
							?>
							<img class="img-fluid" src="<?php echo $group_img['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />

							<h4 class="tomatillo"><img class="img-fluid"  src="<?php echo $group_imgTitle['url']; ?>" width="auto" height="auto" alt="<?php echo ($altTitle) ? $alt : $titleTitle ?>" /></h4>

							<p><?php the_sub_field('description'); ?></p>

							<div class="lear-more">
								<a class="btn-chichis" title="Learn More" href="<?php the_sub_field('url_page')['url']; ?>" >Learn More</a>
							</div><!-- /.lear-more -->

						<?php endwhile; ?>
					<?php endif; ?>
				</div><!-- /.col-md-4 -->

				<div class="col-md-4">
					<?php if( have_rows('salsas_group2') ): ?>
						<?php while( have_rows('salsas_group2') ): the_row(); ?>
							
							<?php 
								$group_img = get_sub_field('image');
								$alt = $group_img['alt'];
								$title = $group_img['title']; 
								
								$group_imgTitle = get_sub_field('image_title');
								$altTitle = $group_imgTitle['alt'];
								$titleTitle = $group_imgTitle['title']; 
							?>
							<img class="img-fluid" src="<?php echo $group_img['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />
							
							<h4 class="roasted"><img class="img-fluid"  src="<?php echo $group_imgTitle['url']; ?>" width="auto" height="auto" alt="<?php echo ($altTitle) ? $alt : $titleTitle ?>" /></h4>

							<p><?php the_sub_field('description'); ?></p>

							<div class="lear-more">
								<a class="btn-chichis" title="Learn More" href="<?php the_sub_field('url_page')['url']; ?>">Learn More</a>
							</div><!-- /.lear-more -->
							
						<?php endwhile; ?>
					<?php endif; ?>
				</div><!-- /.col-md-4 -->

				<div class="col-md-4">
					<?php if( have_rows('salsas_group3') ): ?>
						<?php while( have_rows('salsas_group3') ): the_row(); ?>
							
							<?php 
								$group_img = get_sub_field('image');
								$alt = $group_img['alt'];
								$title = $group_img['title']; 

								$group_imgTitle = get_sub_field('image_title');
								$altTitle = $group_imgTitle['alt'];
								$titleTitle = $group_imgTitle['title']; 
							?>
							<img class="img-fluid" src="<?php echo $group_img['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />
							
							<h4 class="smoky"><img class="img-fluid"  src="<?php echo $group_imgTitle['url']; ?>" width="auto" height="auto" alt="<?php echo ($altTitle) ? $alt : $titleTitle ?>" /></h4>

							<p><?php the_sub_field('description'); ?></p>

							<div class="lear-more">
								<a class="btn-chichis" title="Learn More" href="<?php the_sub_field('url_page')['url']; ?>">Learn More</a>
							</div><!-- /.lear-more -->
							
						<?php endwhile; ?>
					<?php endif; ?>
				</div><!-- /.col-md-4 -->
			</div><!-- /.row -->
		</div><!-- /.three-info -->
	</div><!-- /.container-fluid -->
</section>


<section class="dancing-ingredients">
	<img class="top" src="<?php echo $upload_theme; ?>fiesta-style/dancing-ingredients-top.png">
	<?php 
		$banner_img = get_field('salsas_banner');
		$alt = $banner_img['alt'];
		$title = $banner_img['title']; 
	?>
	<img class="big" src="<?php echo $banner_img['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />

	<?php 
		$banner_img_mob = get_field('salsas_banner_mobile');
		$alt = $banner_img_mob['alt'];
		$title = $banner_img_mob['title']; 
	?>
	<img class="small" src="<?php echo $banner_img_mob['url']; ?>" width="auto" height="auto" alt="<?php echo ($alt) ? $alt : $title ?>" />
</section>

<section class="fiesta-vids">
	<img class="full-width" alt="Fiesta Style" src="<?php echo $upload_theme; ?>fiesta-style/orange-back-top.png">
	<div class="main">
		<div class="container">
			<div class="video-info">
				<div class="row">
					<?php if( have_rows('video_group') ):
						$count = 1;
					    while ( have_rows('video_group') ) : the_row(); ?>
					        <div class="vid-link <?php if ($count == 1): ?>full<?php endif ?>" style="background-image: url('<?php the_sub_field('image_video'); ?>')" data-url="<?php the_sub_field('url_video'); ?>">
								<span class="mask"></span>
								<span class="msg"><?php the_sub_field('text_video'); ?></span>
							</div>
					    <?php $count++; endwhile;
					endif; ?>
				</div>
			</div><!-- /.video-info -->
		</div>
	</div>
</section>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();