<?php 
// Template Name: Page Foldables 2
get_header();

?>
<header>
	<img class="overlay img-fluid d-md-block d-none" alt="Chi-Chi's Foldables" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/header.png" />
	<img class="overlay img-fluid d-block d-md-none" alt="Chi-Chi's Foldables" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/landing page-foldable-header-mobile.png" />
	
	<?php if( have_rows('foldable_banner') ): ?>
		<?php while( have_rows('foldable_banner') ): the_row(); ?>
			<img class="main img-fluid" alt="<?php echo get_sub_field('title') ?>" src="<?php echo get_sub_field('image_banner')['url'] ?>" />
		<?php endwhile; ?>
	<?php endif; ?>

	<div class="foldable-logo">
		<?php if( have_rows('foldable_logo') ): ?>
			<?php while( have_rows('foldable_logo') ): the_row(); ?>
				<img alt="<?php echo get_sub_field('title') ?>" class="img-fluid" src="<?php echo get_sub_field('image_logo')['url'] ?>" />
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
</header>

<section class="how-to-fold">
	<img class="top img-fluid sep" alt="Chi-Chi's Foldables" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/blue-rip-top.png" />
	<img class="bottom img-fluid" alt="Chi-Chi's Foldables" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/blue-rip-main.jpg" />
	<div class="wrap-wrapper" >
		<div class="ltext" >
			<?php if( have_rows('text_info_top_left') ): ?>
				<?php while( have_rows('text_info_top_left') ): the_row(); ?>
					<img class="not-your-average img-fluid" alt="<?php echo get_sub_field('title') ?>" src="<?php echo get_sub_field('image_left')['url'] ?>" />
				<?php endwhile; ?>
			<?php endif; ?>

			<?php if( have_rows('text_info_bottom') ): ?>
				<?php while( have_rows('text_info_bottom') ): the_row(); ?>
					<img class="get-the-bread img-fluid" alt="<?php echo get_sub_field('title') ?>" src="<?php echo get_sub_field('image_bottom')['url'] ?>" />
				<?php endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="wrap" >
			<?php if( have_rows('image_animation') ): ?>
				<?php while( have_rows('image_animation') ): the_row(); ?>
					<img src="<?php echo get_sub_field('animation')['url'] ?>" alt="<?php echo get_sub_field('title') ?>" class="img-fluid" />
				<?php endwhile; ?>
			<?php endif; ?>
		</div>

		<div class="rtext" >
			<?php if( have_rows('text_info_right') ): ?>
				<?php while( have_rows('text_info_right') ): the_row(); ?>
					<img class="with-a-twist img-fluid" alt="<?php echo get_sub_field('title') ?>" src="<?php echo get_sub_field('image_right')['url'] ?>" />
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="folded-fried-vid">
	<img class="background img-fluid" alt="it's a party" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/party-background.jpg" />
	<div class="folded-holder vid-link" data-url="https://www.youtube.com/embed/IDKOXK5pQq4">
		<img class="folded img-fluid" alt="Chi-Chi's Foldables"  src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/folded-vid-poster.png"/>
	</div>
	<div class="play-button vid-link" data-url="https://www.youtube.com/embed/IDKOXK5pQq4">
		<img class="play img-fluid" alt="play" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/play-blue.png"/>
	</div>
</section>

<section class="agua">
	<img class="full-width img-fluid" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/blue-bottom-rip.jpg" alt="Chi-Chi's Foldables" />
	<div class="content">
		<?php echo get_field('description'); ?>
	</div>
</section>

<section class="steps">
	<img class="top-logo" alt="zero trans fats" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/zero-trans-fats.png" />
	<img class="show-steps" alt="the steps" src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/the-steps.png" />
</section>

<section class="recipe-vids">
	<div class="easy-recipes">
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/inc/assets/img/foldable/easy-recipes.png" alt="easy-recipes" width="auto" height="auto" class="img-fluid" />
		</figure>
	</div><!-- /.easy-recipes -->
	<div class="container-fluid">
		<div class="row">
			<?php if( have_rows('content_video') ):
				$count = 1;
			    while ( have_rows('content_video') ) : the_row(); ?>
			    	<?php switch ($count) {
			    		case 7:
			    		case 9: ?>
							<div class="col-md-6 col-lg-4 p-0">
								<div class="img-cell">
									<img src="<?php echo get_sub_field('image_video')['url']; ?>" alt="<?php the_sub_field('title'); ?>" class="img-fluid" />
								</div>
							</div><!-- /.col-md-6 col-lg-4 -->
			    			<?php break; ?>
			    		
			    		<?php default: ?>
					    	<div class="col-md-6 col-lg-4 p-0">
						        <div class="vid-link <?php if ($count == 1): ?>full<?php endif ?>" style="background-image: url('<?php echo get_sub_field('image_video')['url']; ?>')" data-url="<?php the_sub_field('video_url'); ?>">
									<span class="mask"></span>
									<span class="msg"><?php the_sub_field('description'); ?></span>

									<div class="recipe">
										<div class="recipe-content">
											<h1><?php the_sub_field('title'); ?></h1>
											<div class="recipe-steps">
												<div class="table container">
													<div class="row">
														<div class="col-lg-6">
															<?php the_sub_field('ingredients'); ?>
														</div><!-- /.col-lg-6 -->
														<div class="col-lg-6">
															<?php the_sub_field('directions'); ?>
														</div><!-- /.col-lg-6 -->
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div><!-- /.col-md-6 col-lg-4 -->
			    			<?php break; ?>
			    	<?php } ?>
			    <?php $count++; endwhile;
			endif; ?>
		</div><!-- /.row -->
	</div><!-- /.container-fluid -->
</section>

<?php 
 get_footer();
