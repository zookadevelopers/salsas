<?php 

// PostType Page Template: Single page product

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$url_site = get_site_url();
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="faq-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 p-0">
                <h1>Frequently Asked Questions</h1>
            </div>
        </div>
    </div>
    <div class="announcer-bar">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact-faq">
                        <p>Other questions, comments or feedback? We can help you. </p>
                        <div class="cont-cta">
                            <a href="<?php echo $url_site; ?>/contact-us" title="Contact Us" class="btn btn-chichis">Contact us</a>
                        </div><!-- /.cont-cta -->
                    </div>
                </div>
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.announcer-bar -->
    <div class="cont-tabs">
        <div class="container">
            <div class="row">
                <?php the_content(); ?>
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div><!-- /.cont-tabs -->
</section> 

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
