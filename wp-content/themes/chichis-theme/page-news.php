<?php 

// Template Name: Newsletter

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="newsletter">
    <div class="container">
        <div class="row info-product">
            <div class="col-12">
                <i class="far fa-check-circle"></i>
                <h2><?php the_title(); ?></h2>
                <div><?php the_content(); ?></div>
                <div class="btn-chichis m-auto mt-4">
                    <a href="<?php echo site_url(); ?>" id="write-review" title="Back Home" class="btn-chichis">back home</a>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php 
 get_footer();
