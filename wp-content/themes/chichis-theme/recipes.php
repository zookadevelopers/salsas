<?php 
// Template Name: Recipes
get_header(); ?>

<?php 
	$upload_dir = wp_upload_dir();
	$upload_theme = get_template_directory_uri().'/inc/assets/img/';
	
	switch_to_blog(1);
	$salsas_domain = get_site_url();
	restore_current_blog();

	$site_url = get_site_url();
	$id_page = 1;
	$num_post = 0;
	$slug_page = 'chi-chis';

	switch_to_blog($id_page); 

?>

<div class="container-fluid normal">
	<div class="row">
		<div class="col-12 col-lg-3 col-md-3 left-filters d-none d-md-block">
			<?php get_template_part( 'template-parts/category-filters' ); ?>
		</div><!-- /.col-12 col-lg-3 -->
		<div class="col-12 col-md-9 col-lg-9 col-xl-8 right-recipes" id="main-recipes">
			<div class="container">
				<div class="d-block d-md-none">
					<div class="row">
						<div class="col-12">
							<a href="javascript:;" class="btn-filter-recipes title-filters ssm-toggle-nav" title="Filter All Recipes">
								<svg xmlns="http://www.w3.org/2000/svg" width="30" height="31" viewBox="0 0 30 31">
									<g fill="none" fill-rule="evenodd">
										<rect width="30" height="3" y="4" fill="#4b764a" rx="1"/>
										<rect width="30" height="3" y="14" fill="#4b764a" rx="1"/>
										<rect width="30" height="3" y="24" fill="#4b764a" rx="1"/>
										<circle cx="7.5" cy="15.5" r="2.5" fill="#FFF"/>
										<circle cx="17.5" cy="5.5" r="2.5" fill="#FFF"/>
										<circle cx="23.5" cy="25.5" r="2.5" fill="#FFF"/>
										<path fill="#4b764a" fill-rule="nonzero" d="M17.5 10.25a4.75 4.75 0 1 1 0-9.5 4.75 4.75 0 0 1 0 9.5zm0-2.5a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5zM7.5 20.25a4.75 4.75 0 1 1 0-9.5 4.75 4.75 0 0 1 0 9.5zm0-2.5a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5zM23.5 30.25a4.75 4.75 0 1 1 0-9.5 4.75 4.75 0 0 1 0 9.5zm0-2.5a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5z"/>
									</g>
								</svg> Filter All Recipes</a>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
				</div><!-- /.d-block d-md-none -->
				<div class="row justify-content-sm-start justify-content-lg-start">
					<script type="text/javascript">
						var data = [];
					</script>

					<?php
						if (get_field('chichis_filter_id', 'option')): 
							$chichis_filter_id = get_field( 'chichis_filter_id', 'option' ); 
						endif;

						$args = array( 
							'post_type' => 'post', 
							'search_filter_id' => $chichis_filter_id,
							'category_name'  => $slug_page,
						);

						$loop = new WP_Query( $args );
						global $imgUrl;
						while ( $loop->have_posts() ) : $loop->the_post();
							global $post;

							$imgUrl = get_field('recipe-chichis-image');

							if ($imgUrl == '') {
								$imgUrl = get_the_post_thumbnail_url();
							}

							$pageId = get_field('page_id');
							$postId = get_the_ID();
							$name = get_the_title();
							$currentUrl = get_permalink();
							$recipeSlug = $post->post_name;
							
							$apiKey = '59fb8bdf-cb43-490b-8b09-975d68551c2a';
							$merchantGroup = '78368';
							$merchantId = '278593';
					?>
					<script type="text/javascript">
						data.push({
							locale: 'en_US',
							api_key: '<?php echo $apiKey; ?>',
							merchant_group_id: '<?php echo $merchantGroup; ?>',
							merchant_id: '<?php echo $merchantId; ?>',
							page_id: '<?php echo $pageId ?>',
							style_sheet: '<?php echo $salsas_domain; ?>/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
							review_wrapper_url: '/chi-chis/add-review?post_id=<?php echo $postId ?>&pr_page_id=<?php echo $pageId ?>',
							components:{
								CategorySnippet: 'pr-reviewsnippet-<?php echo $pageId ?>'
							}
						});
					</script>
					
					<div class="col-xl-4 col-lg-6 col-md-6 col-sm-5 col-6">
						<div id="post-<?php the_ID(); ?>" class="item type-post hentry" data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

							<a href="<?php echo $site_url; ?>/recipe/<?php echo $recipeSlug; ?>" title="<?php the_field('sals-recipe-title'); ?>" class="d-block">
								<div class="post-thumbnail category-post-image" style="background-image: url(<?php echo $imgUrl; ?>)">
									<?php the_post_thumbnail(); ?>
								</div>
								<div class="entry-header">
									<p class="entry-title"><?php the_field('sals-recipe-title'); ?></p>
									<div class="brand">With <?php the_field('sals-recipe-author'); ?></div>
								</div><!-- .entry-header -->

								<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
							</a>
							<?php if ($pageId != '') : ?>
							<a href="/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" class="btn btn-link btn-write-review-link">Write a Review</a>
							<?php endif; ?>
						</div><!-- #post-## -->
					</div>
					<?php 
						$count++; 
						endwhile;
						restore_current_blog();
					?>
					<script type="text/javascript" charset="utf-8">
						var currentUrl = window.location.href;
						jQuery(document).ready(function(){
							POWERREVIEWS.display.render(data);
						});
					</script>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.col-12 col-lg-9 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<?php get_footer(); ?>