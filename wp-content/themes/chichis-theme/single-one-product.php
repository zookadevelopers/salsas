<?php 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>

<section class="single-product">
	<div class="container">
		<div class="row info-product">
			<div class="col-12 col-md-6 col-lg-6">
				<div class="product-big-image">

					<?php if( have_rows('product_group') ): ?>
						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_image = get_sub_field('product_group_image');
							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);
							$product_nutritional = get_sub_field('product_group_nutritional_image');
							?>
							
							<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
								<img src="<?php echo $product_image; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
							</figure>

						<?php
							$count++;
							endwhile;
						?>
					<?php else: ?>
						<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('product-name'); ?>" />
					<?php endif; ?>

				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-6">
				<h1><?php the_field('product-name'); ?><small><?php the_field('product-heat-indicator'); ?><?php the_field('product-style'); ?></small></h1>
				
				<p class="description"><?php the_field('product-description'); ?></p>

				<div class="sizes">
					<span>Available sizes</span>

					<?php if( have_rows('product_group') ): ?>

						<ul class="sizes-list">

						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);

						?>

						<li class="<?php if ($count == 1): ?>active<?php endif; ?>"><a href="#" data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>"><?php echo $product_size['size_qty'].' '.$product_size['size_type']; ?></a></li>

						<?php
							$count++;
							endwhile;
						?>

						</ul>

					<?php endif; ?>
				</div><!-- /.sizes -->

				<div class="cont-cta">
					<?php if (get_field('product_buy_online')) : ?>
						<a href="<?php the_field('product_buy_online_url') ?>" title="Buy Online" class="btn btn-chichis m-0" target="_blank">Buy Online</a>
					<?php endif; ?>
					<a href="#" title="Write a Review" class="btn btn-chichis review m-0">Write a Review</a>
				</div><!-- /.cont-cta -->

				<?php echo do_shortcode('[ssba-buttons]'); ?>
			</div>
		</div>

		<div class="row cards-product">
			<div class="col-12 col-md-6">
				<div class="card">
					<div class="card-header">
						<p>Ingredients</p>
					</div>
					<div class="card-body">
						<p class="card-text"><?php the_field('product-ingredients'); ?></p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="card">
					<div class="card-header">
						<p>Nutrition Facts</p>
					</div>
					<div class="card-body">
						<?php if( have_rows('product_group') ): ?>

							<?php
								$count = 1;
								while( have_rows('product_group') ): the_row(); 

								$product_size = get_sub_field('product_group_size');
								$product_size_qty = str_replace('.', '', $product_size['size_qty']);
								$product_nutritional = get_sub_field('product_group_nutritional_image');

							?>

							<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
								<img src="<?php echo $product_nutritional; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
							</figure>

							<?php
								$count++;
								endwhile;
							?>

						<?php endif; ?>

					</div>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<section class="featured-recipes-home">
	<div class="title">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h3>Reviews</h3>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
		</div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<?php 
 get_footer();

