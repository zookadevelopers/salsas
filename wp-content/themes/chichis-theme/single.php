<?php 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>

<section class="single-product">
	<div class="container">
		<div class="row info-product">
			<div class="col-12 col-md-6 col-lg-6">
				<div class="product-big-image">

					<?php if( have_rows('product_group') ): ?>
						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_image = get_sub_field('product_group_image');
							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);
							$product_nutritional = get_sub_field('product_group_nutritional_image');
							?>
							
							<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
								<img src="<?php echo $product_image; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
							</figure>

						<?php
							$count++;
							endwhile;
						?>
					<?php else: ?>
						<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('product-name'); ?>" />
					<?php endif; ?>

				</div>
			</div>
			<div class="col-12 col-md-6 col-lg-6">
				<h1><?php the_field('product-name'); ?><small><?php the_field('product-heat-indicator'); ?><?php the_field('product-style'); ?></small></h1>

				<div class="product-reviews">
					<div id="pr-reviewsnippet-top-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
				</div>

				<p class="description"><?php the_field('product-description'); ?></p>

				<div class="sizes">
					<span>Available sizes</span>

					<?php if( have_rows('product_group') ): ?>

						<ul class="sizes-list">

						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);

						?>

						<li class="<?php if ($count == 1): ?>active<?php endif; ?>"><a href="#" data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>"><?php echo $product_size['size_qty'].' '.$product_size['size_type']; ?></a></li>

						<?php
							$count++;
							endwhile;
						?>

						</ul>

					<?php endif; ?>
				</div><!-- /.sizes -->

				<div class="cont-cta">
					<?php if (get_field('product_buy_online')) : ?>
						<a href="<?php the_field('product_buy_online_url') ?>" title="Buy Online" class="btn-chichis m-0" target="_blank">Buy Online</a>
					<?php endif; ?>

					<?php if (get_field('product_find_more')) : ?>
						<a href="<?php the_field('product_find_more_url') ?>" title="<?php the_field('product_find_more_text') ?>" class="btn-chichis m-0" target="_blank"><?php the_field('product_find_more_text') ?></a>
					<?php endif; ?>

					<a href="javascript:;" id="write-review" title="Write a Review" class="btn-chichis review m-0">Write a Review</a>
				</div><!-- /.cont-cta -->

				<?php echo do_shortcode('[ssba-buttons]'); ?>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<section class="ingredients-product">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-6 p-0">
				<div class="ingredients text-center">
					<h2 class="title-ingredients mb-4">Ingredients</h2>
					<div class="text-ingredients mb-5"><?php the_field('product-ingredients'); ?></div>
				</div>
				<div class="nutrition d-flex justify-content-center">
					<div class="btn-victoria">
						<a href="#" class="btn-chichis px-5" title="View Nutritional Information" data-toggle="modal" data-target="#nutritionalModal">View Nutritional Information</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="title">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h3>Reviews</h3>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container">
			<div class="row columns">
				<div class="col-md-5 col-12">
					<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet"></div>
					<div id="pr-reviewhistogram" class="p-w-r">
						<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
					</div>
					<div class="cont-write-review"></div><!-- /.cont-write-review -->

				</div><!-- /.col-5 -->
				<div class="col-md-7 col-12">
					<div id="pr-reviewdisplay-<?php the_field('page_id'); ?>" class="cont-review-display"></div>
				</div><!-- /.col-7 -->
			</div><!-- /.row -->
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade nutritional-pop-up" id="nutritionalModal" tabindex="-1" role="dialog" aria-labelledby="nutritionalModalLabel" aria-hidden="true">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    <figure><img src="<?php echo $upload_theme ?>close-white.png" class="img-fluid" width="auto" height="auto" /></figure>
	</button>
	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
		<?php if( have_rows('product_group') ): ?>

			<?php
				$count = 1;
				while( have_rows('product_group') ): the_row(); 

				$product_size = get_sub_field('product_group_size');
				$product_size_qty = str_replace('.', '', $product_size['size_qty']);
				$product_nutritional = get_sub_field('product_group_nutritional_image');

			?>

			<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
				<img src="<?php echo $product_nutritional; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
			</figure>

			<?php
				$count++;
				endwhile; wp_reset_postdata();
			?>

		<?php endif; ?>
	</div>
</div>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>

	<script type="text/javascript" charset="utf-8">
		jQuery(document).ready(function(){

			POWERREVIEWS.display.render(
				[
					{
						api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
						locale: 'en_US',
						merchant_group_id: '78368',
						merchant_id: '278593',
						page_id: '<?php the_field('page_id'); ?>',
						review_wrapper_url: '/chi-chis/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
						style_sheet: '/wp-content/themes/chichis-theme/inc/assets/css/custom/reviews.css',
						components: {
							CategorySnippet: 'pr-reviewsnippet-top-<?php the_field('page_id'); ?>',
						}
					},
					{
						api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
						locale: 'en_US',
						merchant_group_id: '78368',
						merchant_id: '278593',
						page_id: '<?php the_field('page_id'); ?>',
						review_wrapper_url: '/chi-chis/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
						style_sheet: '/wp-content/themes/chichis-theme/inc/assets/css/custom/reviews.css',
						on_render: function(config, data) {
							jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
							jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn-chichis');
						},
						components: {
							ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
							ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>'
						}
					}
				]
			);
		});
	</script>

<?php get_footer(); ?>

