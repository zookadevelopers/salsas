<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
?>
<div class="col-12 item-search">
	<?php if (get_the_post_thumbnail_url()): ?>
		<img class="d-none img-product" src="<?php the_post_thumbnail_url(); ?>" alt="" />
	<?php endif ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="entry-header">
			<?php the_title( sprintf( '<p class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></p>' ); ?>
		</div><!-- .entry-header -->

		<div class="entry-summary">
			<?php if (get_the_excerpt() == false): ?>
				<p><?php the_field('product-description'); ?></p>			
			<?php endif ?>
				<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->

	</article><!-- #post-## -->
	<div class="cont-button">
		<a href="<?php echo get_permalink( $post->ID ); ?>" class="btn btn-chichis">View</a>
	</div><!-- /.cont-button -->
	
</div>
	