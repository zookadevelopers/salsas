<div class="d-block d-md-none">
	<?php 
		
		$upload_dir = wp_upload_dir();
		$upload_theme = get_template_directory_uri().'/inc/assets/img/';
		global $brandLogo;

		switch_to_blog(1);
		$salsas_domain = get_site_url();

		$brandLogoCms = get_field('chichis_logo', 'option');

		if ($brandLogoCms) {
			$brandLogo = $brandLogoCms;
		} else {
			$brandLogo = $upload_theme.'logo-mobile.png';
		}

		restore_current_blog();
	?>

	<div class="ssm-overlay ssm-toggle-nav"></div>
	<div class="cont-mobile-menu left-filters">
		<a href="javascript:;" class="btn-close-menu-mobile ssm-toggle-nav"><figure><img src="<?php echo $upload_theme ?>close.png" class="img-fluid" width="auto" height="auto" /></figure></a>
		<section class="mobile-logo">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
							<a class="d-block" href="<?php echo esc_url( home_url( '/' )); ?>">
								<figure>
									<img class="img-fluid" src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" />
								</figure>
							</a>
						<?php else : ?>
							<a class="d-block" href="<?php echo esc_url( home_url( '/' )); ?>">
								<figure>
									<img class="img-fluid" src="<?php echo $brandLogo; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" />
								</figure>
							</a>
						<?php endif; ?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.mobile-logo -->
		<section class="menu-scroll">
			<div class="container">
				<div class="visible-menu text-center">
					<div class="row">
						<div class="col-12">
							<?php
								wp_nav_menu(array(
									'theme_location'	=> 'primary',
									'container'			=> 'div',
									'container_id'		=> 'main-nav',
									'container_class'	=> 'justify-content-center header-nav',
									'menu_id'			=> false,
									'menu_class'		=> 'navbar-nav',
									'depth'				=> 3,
									'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
									'walker'			=> new wp_bootstrap_navwalker()
								));
							?>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
					<div class="row">
						<div class="col-12">
							<?php get_search_form(); ?>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
				</div><!-- /.visible-menu -->
				<div class="visible-filters">
					<div class="row">
						<div class="col-12">
							<?php get_template_part( 'template-parts/category-filters' );?>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
				</div><!-- /.visible-filters -->
			</div><!-- /.container -->
		</section><!-- /.menu-scroll -->
	</div><!-- /.cont-mobile-menu -->
</div><!-- /.d-block d-sm-none -->
