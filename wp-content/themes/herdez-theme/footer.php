<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
				</div><!-- /.col-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
	<?php if ( is_home() || is_page_template('single-all-products.php')): ?>
	
	<div class="container">
		<div class="row text-center">
			<div class="col-12 py-4">
				<hr>
				<h2 class="mar-top-30 px-5">Compliment your recipes with</h2>

				<?php if( have_rows('brands_group', 'option') ): ?>

					<div class="row justify-content-center align-items-center my-5">

					<?php while( have_rows('brands_group', 'option') ): the_row(); 

						$brand_name = get_sub_field('brand_name');
						$brand_image = get_sub_field('brand_image');
						$brand_link = get_sub_field('brand_link');

					?>
						
						<div class="col-6 col-md-3">
							<a class="d-block" href="<?php echo $brand_link; ?>" title="<?php echo $brand_name; ?>" target="_blank">
								<img src="<?php echo $brand_image; ?>" class="img-fluid img-brand" alt="<?php echo $brand_name; ?>" />
							</a>
						</div>

					<?php endwhile; ?>

					</div>

				<?php endif; ?>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
	<?php endif ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php get_template_part( 'footer-widget' ); ?>

		<div class="container pt-3 pb-3">
            <div class="site-info d-block d-md-flex align-items-center">
            	<?php dynamic_sidebar( 'footer-copyright' ); ?>
                <span class="sep d-none d-md-block">&nbsp;</span> <p class="mt-3 mt-md-0">Copyright <?php echo date('Y'); ?> &copy;&nbsp; <?php echo '<a class="d-block d-md-inline" href="'. site_url() .'" target="_blank">salsas.com/herdez</a>'; ?></p>
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div>
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
</body>
</html>