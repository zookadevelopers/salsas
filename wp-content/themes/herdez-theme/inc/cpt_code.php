<?php
function cptui_register_my_cpts() {

	/**
     * Post Type: Categories.
     */

    $labels = array(
        "name" => __( "Categories", "wp-herdez-theme" ),
        "singular_name" => __( "Category", "wp-herdez-theme" ),
        "add_new" => __( "Add New Category", "wp-herdez-theme" ),
    );

    $args = array(
        "label" => __( "Categories", "wp-herdez-theme" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "products", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "/herdez/wp-content/uploads/sites/4/2018/11/icon-product.png",
        "supports" => array( "title", "editor", "thumbnail", "custom-fields", "page-attributes" ),
    );

    register_post_type( "products", $args );

	/**
	 * Post Type: Taqueria Street Sauces.
	 */

	$labels = array(
		"name" => __( "Taqueria Street Sauces", "wp-herdez-theme" ),
		"singular_name" => __( "Taqueria Street Sauces", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Taqueria Street Sauces", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/taqueria-street-sauces", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "taqueria_street", $args );

	/**
	 * Post Type: Salsas.
	 */

	$labels = array(
		"name" => __( "Salsas", "wp-herdez-theme" ),
		"singular_name" => __( "Salsa", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Salsas", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/salsas", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "salsas", $args );

	/**
	 * Post Type: Guacamole.
	 */

	$labels = array(
		"name" => __( "Guacamole", "wp-herdez-theme" ),
		"singular_name" => __( "Guacamole", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Guacamole", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/guacamole", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "guacamole", $args );

	/**
	 * Post Type: Cooking Sauces.
	 */

	$labels = array(
		"name" => __( "Cooking Sauces", "wp-herdez-theme" ),
		"singular_name" => __( "Cooking Sauces", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Cooking Sauces", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/cooking-sauces", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "cooking_sauces", $args );

	/**
	 * Post Type: Tortillas.
	 */

	$labels = array(
		"name" => __( "Tortillas", "wp-herdez-theme" ),
		"singular_name" => __( "Tortilla", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Tortillas", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/tortillas", "with_front" => true ),
		"query_var" => "tortillas",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "tortillas", $args );

	/**
	 * Post Type: Dips.
	 */

	$labels = array(
		"name" => __( "Dips", "wp-herdez-theme" ),
		"singular_name" => __( "Dips", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Dips", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/dips", "with_front" => true ),
		"query_var" => "dips",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "dips", $args );

	/**
	 * Post Type: Chiles & Peppers.
	 */

	$labels = array(
		"name" => __( "Chiles & Peppers", "wp-herdez-theme" ),
		"singular_name" => __( "Chiles & Peppers", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Chiles & Peppers", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/chiles-peppers", "with_front" => true ),
		"query_var" => "chiles-peppers",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "chiles_peppers", $args );

	/**
	 * Post Type: Banner Sliders.
	 */

	$labels = array(
		"name" => __( "Banner Sliders", "wp-herdez-theme" ),
		"singular_name" => __( "Banner Slider", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Banner Sliders", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "banner_slider", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://206.189.231.128/lavictoria/wp-content/uploads/sites/6/2018/12/icon-slider.png",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "banner_slider", $args );

	/**
	 * Post Type: Refrigerated Salsas.
	 */

	$labels = array(
		"name" => __( "Refrigerated Salsas", "wp-herdez-theme" ),
		"singular_name" => __( "Refrigerated salsas", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Refrigerated Salsas", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/refrigerated-salsas", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "refrigerated_salsas", $args );

	/**
	 * Post Type: Tortillas Chips.
	 */

	$labels = array(
		"name" => __( "Tortillas Chips", "wp-herdez-theme" ),
		"singular_name" => __( "Tortilla Chip", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Tortillas Chips", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/tortilla-chips", "with_front" => true ),
		"query_var" => "tortilla_chips",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "tortilla_chips", $args );

	/**
	 * Post Type: Peppers.
	 */

	$labels = array(
		"name" => __( "Peppers", "wp-herdez-theme" ),
		"singular_name" => __( "Pepper", "wp-herdez-theme" ),
	);

	$args = array(
		"label" => __( "Peppers", "wp-herdez-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/peppers", "with_front" => true ),
		"query_var" => "peppers",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "peppers", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
