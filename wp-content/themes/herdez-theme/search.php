<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section>
		<div class="container search-page-title">
			<div class="row">
				<div class="col-12">
					<?php 
					global $wp_query;
					echo '<p>'.$wp_query->found_posts.' Results for '.'"'.get_search_query().'"'.'</p>';
					?>
				</div>
			</div>
		</div>
	</section>


	<section id="primary" class="container search-result">
		<main id="main" class="site-main" role="main">

		<?php
		if ( have_posts() ) : ?>

			<div class="row">
				<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content-search', get_post_format() );

					endwhile;

					the_posts_pagination( array(
					    'mid_size' => 3,
					    'prev_text' => __( '<', 'textdomain' ),
					    'next_text' => __( '>', 'textdomain' ),
					) );
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
			
			</div><!-- /.row -->
		</main><!-- #main -->
	</section><!-- #primary -->


<?php
get_footer();
