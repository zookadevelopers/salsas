<?php 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<section class="single-product">
	<div class="container">
		<div class="row info-product">
			<div class="col-12">
				<h2><?php the_title(); ?></h2>
				<div><?php the_content(); ?></div>
				<img class="my-5 img-product img-fluid w-100" title="<?php the_title(); ?>" src="<?php the_post_thumbnail_url(); ?>"/>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php 
 get_footer();

