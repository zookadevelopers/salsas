<?php 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>

<section class="single-product">
	<div class="container">
		<div class="row info-product">
			<div class="col-12 col-md-6 col-lg-4">

				<?php if( have_rows('product_group') ): ?>
					<?php
						$count = 1;
						while( have_rows('product_group') ): the_row(); 

						$product_image = get_sub_field('product_group_image');
						$product_size = get_sub_field('product_group_size');
						$product_size_qty = str_replace('.', '', $product_size['size_qty']);
						$product_nutritional = get_sub_field('product_group_nutritional_image');
					?>
					
					<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
						<img src="<?php echo $product_image; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
					</figure>

					<?php
						$count++;
						endwhile;
					?>
				<?php else: ?>
					<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('product-name'); ?>" />
				<?php endif; ?>
				<!-- <p class="code text-center">UPC: <?php the_field('product-upc'); ?></p> -->


			</div>
			<div class="col-12 col-md-6 col-lg-8">
				<h1><?php the_field('product-name'); ?></h1>

				<div class="product-reviews">
					<div id="pr-reviewsnippet-top-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
				</div>
				
				<p class="tags-product">HERDEZ® ripens, dries, smokes and cans red jalapeño chiles to preserve their bold flavor.</p>
				<p class="description"><?php the_field('product-description'); ?></p>

				<div class="sizes flex-wrap">
					<span>Available sizes</span>

					<?php if( have_rows('product_group') ): ?>

						<ul class="sizes-list">

						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);

						?>

						<li class="<?php if ($count == 1): ?>active<?php endif; ?>"><a href="#" data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>"><?php echo $product_size['size_qty'].' '.$product_size['size_type']; ?></a></li>

						<?php
							$count++;
							endwhile;
						?>

						</ul>

					<?php endif; ?>
				</div><!-- /.sizes -->

				<div class="cont-cta">
					<?php if (get_field('product_buy_online')) : ?>
						<a href="<?php the_field('product_buy_online_url') ?>" title="Buy Online" class="btn btn-main">Buy Online</a>
					<?php endif; ?>

					<?php if (get_field('product_find_more')) : ?>
						<a href="<?php the_field('product_find_more_url') ?>" title="<?php the_field('product_find_more_text') ?>" class="btn btn-main"><?php the_field('product_find_more_text') ?></a>
					<?php endif; ?> 

					<a href="javascript:;" id="write-review" title="Write a Review" class="btn btn-main review">Write a Review</a>
				</div><!-- /.cont-cta -->
				
				<div class="recipe-share">
					<div class="row">
						<div class="col-12">
							<?php echo do_shortcode('[ssba-buttons]'); ?>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
				</div><!-- /.recipe-share -->
			</div>
		</div>
	</div>
</section>

<section class="ingredients-product mb-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-6 p-0">
				<div class="ingredients text-center">
					<p class="title-ingredients">Ingredients</p>
					<div class="text-ingredients mb-5 px-4"><?php the_field('product-ingredients'); ?></div>
				</div>
				<div class="nutrition d-flex justify-content-center">
					<!-- Button trigger modal -->
					<a class="btn btn-main" href="#" data-toggle="modal" title="View Nutritional Information" data-target="#nutritionalModal">View Nutritional Information</a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="container">
		<div class="row mb-4">
			<div class="col-12">
				<h3>Reviews</h3>
			</div>
		</div>
		<div class="row columns reviews">
			<div class="col-12 col-md-6 col-lg-4 p-0">
				<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet"></div>
				<div id="pr-reviewhistogram" class="p-w-r">
					<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
				</div>
				<div class="cont-write-review"></div><!-- /.cont-write-review -->

			</div><!-- /.col-5 -->
			<div class="col-12 col-md-6 col-lg-8">
				<div id="pr-reviewdisplay-<?php the_field('page_id'); ?>" class="cont-review-display"></div>
			</div><!-- /.col-7 -->
		</div><!-- /.row -->
	</div>
</section>

<!-- Modal -->
<div class="modal fade nutritional-pop-up" id="nutritionalModal" tabindex="-1" role="dialog" aria-labelledby="nutritionalModalLabel" aria-hidden="true">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	</button>
	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
		<?php if( have_rows('product_group') ): ?>

			<?php
				$count = 1;
				while( have_rows('product_group') ): the_row(); 

				$product_size = get_sub_field('product_group_size');
				$product_size_qty = str_replace('.', '', $product_size['size_qty']);
				$product_nutritional = get_sub_field('product_group_nutritional_image');

			?>

			<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
				<img src="<?php echo $product_nutritional; ?>" class="" alt="<?php the_field('product-name'); ?>"/>
			</figure>

			<?php
				$count++;
				endwhile;
			?>

		<?php endif; ?>
	</div>
</div>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>

<script type="text/javascript" charset="utf-8">
	POWERREVIEWS.display.render(
		[
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: '/herdez/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: '/wp-content/themes/herdez-theme/inc/assets/css/custom/reviews.css',
				components: {
					CategorySnippet: 'pr-reviewsnippet-top-<?php the_field('page_id'); ?>',
				}
			},
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: '/herdez/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: '/wp-content/themes/herdez-theme/inc/assets/css/custom/reviews.css',
				on_render: function(config, data) {
					jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
					jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn btn-main');
				},
				components: {
					ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
					ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>'
				}
			}
		]
	);
</script>

<?php get_footer(); ?>

