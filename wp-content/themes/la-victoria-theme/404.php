<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-12 col-sm-12 col-lg-12 page-404-main-section">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found py-5">
				<header class="page-header text-center py-5">
					<h1 class="page-title text-uppercase"><?php esc_html_e( 'Oops!', 'wp-bootstrap-starter' ); ?> </h1>
					<small>Error Code: 404</small>
				</header><!-- .page-header -->

				<div class="page-content">
					<p class="text-center"><?php esc_html_e( 'We can’t seem to find the page you’re looking for. Here are some helpful links instead.', 'wp-bootstrap-starter' ); ?>
					</p>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
