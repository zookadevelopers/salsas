<?php 
// Template Name: Home herdez

global $wp;
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$site_url = get_site_url();

// Products Query
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
		'orderby'=>'menu_order'
	)
); 
?>

<?php get_header(); ?>
<section class="banner-home container-fluid">
	<div class="row">
		<div class="col-12 p-0">
			<div id="carousel-home" class="owl-carousel owl-theme">
				<?php 
					$banner_home = new WP_Query(
						array(
							'post_type'=>'banner_slider', 
							'post_status'=>'publish', 
							'posts_per_page'=> -1,
							'order' => 'ASC',
        					'orderby'=>'menu_order'
						)
					); 
				?>
				<?php while ( $banner_home->have_posts() ) : $banner_home->the_post(); ?>
					<?php
						$img_icon = get_field('banner-image');
						$image = $img_icon['url'];
						$alt = $img_icon['alt'];
						$title = $img_icon['title']; 
					?>
					<div class="item">
						<img class="img-banner img-fluid w-100" src="<?php echo $image; ?>" alt="<?php echo ($alt) ? $alt : $title ?>" />
						<div class="slide-description">
							<h4><?php the_field('banner-title'); ?></h4>
							<p><?php the_field('banner-description'); ?></p>
						</div><!-- /.slide-description -->
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<section class="title">
	<h1>traditional salsa for the modern world</h1>
</section><!-- /.title -->

<section class="products-home">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<h2>Explore our products</h2>
			</div>
		</div>
		<div class="row items-products align-items-end">
			<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
				<?php
					$image_id = get_post_thumbnail_id();
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
					$image_title = get_the_title($image_id);
				?>
				<div class="col-6 col-md-4 text-center item">
					<a class="link-product d-block" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
						</figure>
						<p class=""><?php the_title(); ?></p>
					</a>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="title">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h2 class="gray">Featured Recipes</h2>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container">
			
			<div class="row recipes">
				<?php 
					$id_page = 1;
					$num_post = 0;
					$slug_page = 'victoria';

					switch_to_blog($id_page); 
					// Get posts for each blog
					$myposts = get_posts( 
						array( 
							'category_name'  => $slug_page,
							'posts_per_page' => -1, 
							'orderby' => 'modified'
						)
					);
					// Loop for each blog
					global $post;
					
					foreach( $myposts as $post ) { 
						if (get_field('outstanding_recipes') == 'Left position') {
							$pageId = get_field('page_id');
							$postId = $post->ID;
							$name = $post->post_title;
							$imgUrl = get_the_post_thumbnail_url();
							$currentUrl = home_url( $wp->request );
							$recipeSlug = $post->post_name;

							if ($num_post >= 3) {
								break;
							} ?>
							<?php
								$objPowerReviews[] = [
									'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
									'locale' => 'en_US',
									'merchant_group_id' => '78368',
									'merchant_id' => '278593',
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/la-victoria-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/la-victoria/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
									]
								];

		                        $image_id = get_post_thumbnail_id();
								$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
								$image_title = get_the_title($image_id); 
							?>
							<div class="col-12 col-md-4">
								<div class="recipes-item">
									<figure>
										<?php if (get_the_post_thumbnail_url()) { ?>
											<img class="img-fluid" width="auto" height="auto" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
										<?php }else{ ?>
											<img class="img-fluid" width="auto" height="auto" src="<?php echo $upload_theme ?>recipe.jpg" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
										<?php }  ?>
									</figure>
									<div class="info">
										<p class="text"><?php the_field('sals-recipe-title'); ?></p>
										<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
										<a class="d-block" title="View Recipe" href="<?php echo $site_url; ?>/recipe/<?php echo $recipeSlug; ?>">View Recipe</a>
									</div><!-- /.text -->
								</div><!-- /.cont-recipe -->
							</div>
					<?php $num_post++;  } }
					restore_current_blog();
				?>
			</div>
			<script type="text/javascript" charset="utf-8">
				var currentUrl = window.location.href;
				jQuery(document).ready(function(){
					POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
				});
			</script>
		</div>
	</div>
</section>

<section class="news">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<h2>100 Years of Traditions</h2>
			</div>
		</div>
		<div class="row post-news">
			<div class="col-12 col-md-8 mb-4 mb-md-0 d-none d-md-block">
				<?php 
					$count = 0;
					$myposts = get_posts( 
					    array( 
					        'posts_per_page' => -1, 
					        'orderby' => 'modified'
					    )
					);
					
					global $post;
					foreach( $myposts as $post ) { 
						if ( get_field('outstanding_news') == 'Left position'): 
							
							if ($count >= 1) {
								break;
							}

							if (get_the_post_thumbnail_url()) { 
								$img_post_outstand = get_the_post_thumbnail_url(); 
							} else { 
								$img_post_outstand = $upload_theme.'recipe.jpg'; 
							}  
							$image_id = get_post_thumbnail_id();
							$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
							$image_title = get_the_title($image_id);
							?>

							<div class="news-item left" title="<?php the_title(); ?>" style="background-image: url('<?php echo $img_post_outstand ?>');">
								<div class="info red">
									<h4><?php the_title(); ?></h4>
									<div class="description"><p><?php the_excerpt(); ?></p></div>
									<a class="link" title="Read Full Story" href="<?php the_permalink(); ?>">read full story</a>
								</div><!-- /.info -->
							</div><!-- /.news-item left -->
					
					<?php $count++; ?>
					<?php endif; ?>
					<?php  } ?>
			</div>

			<div class="col-12 col-md-4 pl-md-0 d-none d-md-block">
				<?php
				$count = 0;
				$myposts = get_posts( 
				    array( 
				        'posts_per_page' => -1, 
				        'orderby' => 'modified'
				    )
				);

				global $post;
				foreach( $myposts as $post ) { 
					if ( get_field('outstanding_news') == 'Right position'):
 
						
						if ($count >= 2) {
							break;
						}

						if (get_the_post_thumbnail_url()) { 
							$img_post_outstand = get_the_post_thumbnail_url(); 
						} else { 
							$img_post_outstand = $upload_theme.'recipe.jpg'; 
						} ?>

						<div class="news-item right" style="background-image: url('<?php echo $img_post_outstand ?>');">
							<div class="info yellow">
								<h4><?php the_title(); ?></h4>
								<a class="link" title="read full story" href="<?php the_permalink(); ?>">read full story</a>
							</div><!-- /.info -->
						</div><!-- /.news-item right -->

				<?php $count++; ?>
				<?php endif; ?>
				<?php  } ?>
			</div>

			<div class="col-12 d-block d-md-none">
				<div id="carousel-mobile-home-news" class="owl-carousel owl-theme">
					<?php 
						$myposts = get_posts( 
					    array( 
					        'posts_per_page' => 5, 
					        'orderby' => 'modified'
					    )
					);
					
					global $post;
					foreach( $myposts as $post ) { 
					
						if (get_the_post_thumbnail_url()) { 
							$img_post_outstand = get_the_post_thumbnail_url();
						} else { 
							$img_post_outstand = $upload_theme.'recipe.jpg'; 
						}  ?>
						<div class="news-item left" style="background-image: url('<?php echo $img_post_outstand ?>');">
							<div class="info red">
								<h4><?php the_title(); ?></h4>
								<div class="description"><?php the_excerpt(); ?></div>
								<a class="link" title="read full story" href="<?php the_permalink(); ?>">read full story</a>
							</div><!-- /.info -->
						</div><!-- /.news-item left -->
					<?php }?>
				</div>
			</div><!-- /.col-12 d-block d-md-none -->
		</div>
	</div>
</section><!-- /.news -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<?php 
 get_footer();
