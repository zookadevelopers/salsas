var $ = jQuery;

$(document).ready(function () {
    $('#carousel-home, #carousel-mobile-home-news').owlCarousel({
        items:1,
        nav:true,
        loop: true,
        navText : [
        "<img class='img-fluid' width='75%' src='"+wnm_custom.template_url+"/src/img/arrow-white-left.png' />",
         "<img class='img-fluid' width='75%' src='"+wnm_custom.template_url+"/src/img/arrow-white-right.png' />"
        ]
    });

    $('#carousel-mobile-about').owlCarousel({
        items: 1,
        nav: true,
        loop: true,
        navText : [
        "<img class='img-fluid' width='75%' src='"+wnm_custom.template_url+"/src/img/arrow-red-left.png' />",
         "<img class='img-fluid' width='75%' src='"+wnm_custom.template_url+"/src/img/arrow-red-right.png' />"
        ]
    });

    $('.all-products-carousel').owlCarousel({
        nav:true,
        dots: false,
        loop: false,
        margin: 20,
        navText : [
        "<img src='"+wnm_custom.template_url+"/src/img/arrow-red-left.png' class='img-fluid' />",
         "<img src='"+wnm_custom.template_url+"/src/img/arrow-red-right.png' class='img-fluid' />"
        ],
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:3,
                nav:true
            }
        }
    });

    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
       jQuery('.featured-recipes-home .recipes-item .img-fluid').each(function(){
            var t = jQuery(this),
                s = 'url(' + t.attr('src') + ')',
                p = t.parent(),
                d = jQuery('<div></div>');

            p.prepend(d);
            d.css({
                'width'                 : t.css('width'),
                'height'                : t.css('height'),
                'margin'                : 'auto',
                'background-size'       : 'cover',
                'background-repeat'     : 'no-repeat',
                'background-position'   : '50% 50%',
                'background-image'      : s
            });
            t.hide();
        }); 
    }

    if (document.documentMode || /Edge/.test(navigator.userAgent)) {
       $('.all-products-carousel .img-product').each(function(){
            var t = $(this),
                s = 'url(' + t.attr('src') + ')',
                p = t.parent(),
                d = $('<div></div>');

            p.prepend(d);
            d.css({
                'width'                 : t.css('width'),
                'height'                : t.css('height'),
                'margin'                : 'auto',
                'background-size'       : 'contain',
                'background-repeat'     : 'no-repeat',
                'background-position'   : '50% 50%',
                'background-image'      : s
            });
            t.hide();
        }); 
    }

    $('#write-review').on('click', function() {
        var target = $('#content div.cont-write-review.btn-write-review > a').attr('href');
        window.location.href = document.location.origin+target;
    });

    $('.tnp-email').attr('placeholder', 'Email...');

    $(document).on('click', '.items-all-products a[href^="#"]', function (event) {
        event.preventDefault();

        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top
        }, 500);
    });

    $('.sizes-list [data-size]').on('click', function(e){
        e.preventDefault();

        var $this = $(this);
        var body = $('body');
        var thisParent = $this.parent();
        var thisSize = $this.data('size');
        var thisPosition = $this.data('position');
        var parentList = $('.sizes-list');
        
        var currentActiveElements = body.find('[data-size='+thisSize+'][data-status="active"]');
        var inactiveElements = body.find('[data-size='+thisSize+'][data-status="inactive"]');
        var activeElements = body.find('[data-status="active"]');

        if (!thisParent.hasClass('active')) {
            parentList.find('li').removeClass('active');
            thisParent.addClass('active');
        }

        if (currentActiveElements.data('status') != 'active') {
            $.each( inactiveElements, function(index){
                $(this).attr('data-status', 'active');
            });

            $.each( activeElements, function(index){
                $(this).attr('data-status', 'inactive');
            });
        }
    });

    $('#is-product-content').hide();
    $('[class*="acceptance"]').on('click', function(){
        var contactSpecificProduct = $('[type="checkbox"][name="acceptance-840"]').attr('checked') ? true:false;
        
        if (contactSpecificProduct == true) {
            $('#is-product-content').show();
        } else {
            $('#is-product-content').hide();
        }
    });

    //Back to top button functionallity
    (function($, window, document){

        var backToTopButton = $('.back-top');

        function backToTop(selector, time) {
            $('html, body').animate({
                scrollTop: $(selector).offset().top -30
            }, time);
        }

        $(document).scroll(function(){
            if($(window).scrollTop() + $(window).height() == $(document).height() || $(this).scrollTop() >= 700){
                backToTopButton.fadeIn();
            }else{
                backToTopButton.fadeOut();
            }
        });

        backToTopButton.on('click', function() {
            backToTop('body', 400);
            return false;
        });

    })(jQuery, window, document);

    /* Read more or less in recipe page */
    var btn_read = $('.recipe-description .read-more');
    btn_read.on('click', function(e){
        e.preventDefault();
        var $this = $(this);
        $('.recipe-description p').toggleClass('showing');
        $this.text($this.text() == 'Read More' ? 'Read Less' : 'Read More');
    });

    if(window.location.href.indexOf("product") > -1) {
        var productsLink = $('.main-header.navbar-light .navbar-nav .nav-item').find('a[href*="products"]');

        productsLink.parent().addClass('active');
    }

    if ($('body').hasClass('page-template-page-contact')) {
        $('.contact-page input[type="submit"]').addClass('btn-sub-victoria');
    }
});


$(window).load(function() {
    $(".loader").fadeOut("slow");
    var windowWidth = $(window).width();

    // append placeholder to search input in other brands collapse
    $('.top-header input').attr('placeholder', 'Search other Mexican recipes');

    // Mobile Version
    if (windowWidth <= 991) {
        
        //.dropdown-brands-salsas
        $(".btn-other-brands").click(function(e) {
            e.preventDefault();
            $('.other-brands-collapse').toggleClass('d-block');
        });

        // Mobile menu activate swipe function
        $('.cont-mobile-menu').slideAndSwipe();

        // append placeholder to search input in mobile menu
        $('.cont-mobile-menu .search-form input').attr('placeholder', 'Search La Victoria');

        // filters opotions toggle function
        var mainBrands = $('.left-filters .cont-options .sf-field-category > ul > li > label');

        for (var i = 0; i < mainBrands.length; i++) {
            mainBrands[i].addEventListener('click', function(event) {
                event.preventDefault();
                this.classList.toggle('active');
                var content = this.nextElementSibling;
                if (content.style.display === 'block') {
                    content.style.display = 'none';
                } else {
                    content.style.display = 'block';
                }
            });
        }
    } else {
        //.dropdown-brands-salsas
        $(".dropdown-salsas").hover(
            function() {
                $('.other-brands-collapse').finish().slideDown('medium');
            },
            function() {
                $('.other-brands-collapse').finish().slideUp('medium');
            }
        );
    }
});