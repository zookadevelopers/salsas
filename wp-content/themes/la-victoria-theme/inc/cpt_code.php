<?php 
function cptui_register_my_cpts() {

	/**
     * Post Type: Categories.
     */

    $labels = array(
        "name" => __( "Categories", "wp-la-victoria-theme" ),
        "singular_name" => __( "Category", "wp-la-victoria-theme" ),
        "add_new" => __( "Add New Category", "wp-la-victoria-theme" ),
    );

    $args = array(
        "label" => __( "Categories", "wp-la-victoria-theme" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "products", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "/herdez/wp-content/uploads/sites/4/2018/11/icon-product.png",
        "supports" => array( "title", "editor", "thumbnail", "custom-fields", "page-attributes" ),
    );

    register_post_type( "products", $args );

	/**
	 * Post Type: Salsas.
	 */

	$labels = array(
		"name" => __( "Salsas", "wp-la-victoria-theme" ),
		"singular_name" => __( "Salsa", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Salsas", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/salsas", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "salsas", $args );

	/**
	 * Post Type: Chiles & Peppers.
	 */

	$labels = array(
		"name" => __( "Chiles & Peppers", "wp-la-victoria-theme" ),
		"singular_name" => __( "Chiles & Peppers", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Chiles & Peppers", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/chiles-peppers", "with_front" => true ),
		"query_var" => "chiles-peppers",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "chiles_peppers", $args );

	/**
	 * Post Type: Hot Sauces.
	 */

	$labels = array(
		"name" => __( "Hot Sauces", "wp-la-victoria-theme" ),
		"singular_name" => __( "Hot Sauce", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Hot Sauces", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/hot-sauces", "with_front" => true ),
		"query_var" => "hot-sauces",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "hot_sauce", $args );

	/**
	 * Post Type: Taco Sauces.
	 */

	$labels = array(
		"name" => __( "Taco Sauces", "wp-la-victoria-theme" ),
		"singular_name" => __( "Taco Sauces", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Taco Sauces", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/taco-sauces", "with_front" => true ),
		"query_var" => "taco-sauces",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "taco_sauce", $args );

	/**
	 * Post Type: Enchilada Sauces.
	 */

	$labels = array(
		"name" => __( "Enchilada Sauces", "wp-la-victoria-theme" ),
		"singular_name" => __( "Enchilada Sauces", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Enchilada Sauces", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/enchilada-sauces", "with_front" => true ),
		"query_var" => "enchilada-sauces",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "enchilada_sauce", $args );

	/**
	 * Post Type: Banner Sliders.
	 */

	$labels = array(
		"name" => __( "Banner Sliders", "wp-la-victoria-theme" ),
		"singular_name" => __( "Banner Slider", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Banner Sliders", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "banner_slider", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://206.189.231.128/lavictoria/wp-content/uploads/sites/6/2018/12/icon-slider.png",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "banner_slider", $args );

	/**
	 * Post Type: Organic salsas.
	 */

	$labels = array(
		"name" => __( "Organic salsas", "wp-la-victoria-theme" ),
		"singular_name" => __( "Organic salsas", "wp-la-victoria-theme" ),
	);

	$args = array(
		"label" => __( "Organic salsas", "wp-la-victoria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/organic-salsas", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "organic_salsas", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
