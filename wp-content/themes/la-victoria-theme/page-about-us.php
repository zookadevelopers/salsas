<?php
/**
 * Template Name: About us
 */

get_header();

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$site_url = get_site_url();
// Products Query
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
		'orderby'=>'menu_order'
	)
);
?>

<?php
	global $bannerImage;

	$banner_value = get_field('banner_image');

	if( $banner_value ) {
		$bannerImage = $banner_value;
	} else {
		$bannerImage = $upload_theme.'banner-products.jpg';
	}
?>
	
<section class="banner-product container-fluid" style="background-image: url('<?php echo $bannerImage; ?>');"></section>

<section class="text-under-banner text-product no-bg">
	<div class="container">
		<div class="col-12">
			<h1>About Us</h1>
		</div>
	</div>
</section>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<?php the_content(); ?>
		</div><!-- /.col-12 -->
	</div><!-- /.row -->
</div><!-- /.container -->
<?php endwhile; else : ?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<p><?php esc_html_e( 'This information is not found.' ); ?></p>
		</div><!-- /.col-12 -->
	</div><!-- /.row -->
</div><!-- /.container -->
<?php endif; ?>

<section class="products-home">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<h2>Explore our products</h2>
			</div>
		</div>

		<div class="col-12 d-block d-md-none">
			<div id="carousel-mobile-about" class="owl-carousel owl-theme carousel-mobile-about">
				<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
				<div class="col-12 col-md-4 text-center item">
					<a class="link-product d-block" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
						</figure>
						<p class=""><?php the_title(); ?></p>
					</a>
				</div>
			<?php endwhile; ?>
			</div>
		</div><!-- /.col-12 d-block d-md-none -->

		<div class="d-none d-md-block">
			<div class="row items-products align-items-end">
				<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
					<div class="col-12 col-md-4 text-center item">
						<a class="link-product d-block" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
							<figure>
								<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
							</figure>
							<p class=""><?php the_title(); ?></p>
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</div><!-- /.d-none d-md-block -->
	</div>
</section>

<section class="featured-recipes-home">
	<div class="title">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h2>Featured Recipes</h2>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container">
			
			<div class="row recipes">
				<?php 
					$id_page = 1;
					$num_post = 0;
					$slug_page = 'victoria';

					switch_to_blog($id_page); 
					// Get posts for each blog
					$myposts = get_posts( 
						array( 
							'category_name'  => $slug_page,
							'posts_per_page' => -1, 
							'orderby' => 'modified'
						)
					);
					// Loop for each blog
					global $post;
					
					foreach( $myposts as $post ) { 
						if (get_field('outstanding_recipes') == 'Left position') {
							$pageId = get_field('page_id');
							$postId = $post->ID;
							$name = $post->post_title;
							$imgUrl = get_the_post_thumbnail_url();
							$currentUrl = home_url( $wp->request );
							$recipeSlug = $post->post_name;

							if ($num_post >= 3) {
								break;
							} ?>
							<?php
								$objPowerReviews[] = [
									'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
									'locale' => 'en_US',
									'merchant_group_id' => '78368',
									'merchant_id' => '278593',
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/la-victoria-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/la-victoria/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
									]
								];
							?>
							<div class="col-12 col-md-4">
								<div class="recipes-item">
									<figure>
										<?php if (get_the_post_thumbnail_url()) { ?>
											<img class="img-fluid" width="auto" height="auto" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('sals-recipe-title'); ?>" />
										<?php }else{ ?>
											<img class="img-fluid" width="auto" height="auto" src="<?php echo $upload_theme ?>recipe.jpg" alt="<?php the_field('sals-recipe-title'); ?>" />
										<?php }  ?>
									</figure>
									<div class="info">
										<p class="text"><?php the_field('sals-recipe-title'); ?></p>
										<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
										<a class="d-block" title="View Recipe" href="<?php echo $site_url; ?>/recipe/<?php echo $recipeSlug; ?>">View Recipe</a>
									</div><!-- /.text -->
								</div><!-- /.cont-recipe -->
							</div>
					<?php $num_post++;  } }
					restore_current_blog();
				?>
			</div>
			<script type="text/javascript" charset="utf-8">
				var currentUrl = window.location.href;
				jQuery(document).ready(function(){
					POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
				});
			</script>
		</div>
	</div>
</section>

<?php get_footer(); ?>
