<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/

$search_arg = $wp_query->query_vars['s'];  
$salsas_id_theme = 1;
$slug_page = 'victoria';

// Recipes Query
	switch_to_blog($salsas_id_theme);
	$salsas_domain = get_site_url();
	$args = array(
		's' => $search_arg ,
		'post_type' 		=> 'post', 
		'category_name'  	=> $slug_page,
		'posts_per_page' 	=> -1
	);
	$recipes_query 		= new WP_Query( $args );
	restore_current_blog();

// Recipes Adding the is_recipe property to distinguish them from the current site's post
	foreach ($recipes_query->posts as $key => $recipe) {	
		$recipe->is_recipe = true;
	}

// Search Query in current site
	$new_search_args  	= array(
		's' => $search_arg,
		'posts_per_page' => -1
	);

	$new_search_query 	= new WP_Query( $new_search_args );

// Query that will be rendered
	$all_posts = array_merge( $new_search_query->posts, $recipes_query->posts );

// Pagination Variables
	$current_page = isset( $_GET['page'] ) ? (int) $_GET['page'] : 1;

	$row_items			= 0;

	$items_per_page		= 6;
	$total_items 		= count( $all_posts );
	$pages				= ceil( $total_items / $items_per_page );

	$min_items			= ( ( $current_page * $items_per_page ) - $items_per_page ) + 1;

	$max_items 			= ( $min_items + $items_per_page ) - 1;



get_header(); ?>

	<section>
		<div class="container search-page-title">
			<div class="row">
				<div class="col-12">
					<?php 
					echo '<h2>'.count($all_posts).' Results for '.'"'.get_search_query().'"'.'</h2>';
					?>
				</div>
			</div>
		</div>
	</section>

	<section id="primary" class="container search-result">
		<main id="main" class="site-main" role="main">

		<?php
		if ( isset($all_posts) ) : ?>

			<div class="row">
				<?php
					
					/**
					 * Results are shown inside an array that has, 
					 * posts of the current site and recipes that come from the 
					 * principial site (salsas)
					 */

					/* Start the Loop */
					foreach ($all_posts as $key => $searched_post) {

						$row_items++;
						
						if($row_items < $min_items) { continue; }
						if($row_items > $max_items) { break; }

						include 'template-parts/content-search.php';
					
					}


					$args = array(
						'format' => '?page=%#%',
						'current' 	=> $current_page,
						'total' 	=> $pages,
                        'prev_text'	=> __('<i class="fa fa-chevron-left" aria-hidden="true"></i>'),
                        'next_text' => __('<i class="fa fa-chevron-right" aria-hidden="true"></i>'),

                    );
                    ?>
					<div class="col-12 text-center navigation"> <?php echo paginate_links($args); ?> </div>
					<?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
			
			</div><!-- /.row -->
		</main><!-- #main -->
	</section><!-- #primary -->


<?php
get_footer();
