<?php 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

$other_products = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> 4,
		'order' => 'DESC'
	)
);
?> 

<?php get_header(); ?>

<?php
	global $indicator_image;

	$indicator_value = get_field('product-heat-indicator');

	switch ($indicator_value) {
		case 'extra hot':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-extra-hot.png';
			break;
		case 'hot':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-hot.png';
			break;
		case 'medium':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-medium.png';
			break;
		case 'mild':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-mild.png';
			break;
	}

?>

<section class="single-product">
	<div class="container">
		<div class="row info-product">
			<div class="col-12 col-md-6 col-lg-6">
				<h1><?php the_field('product-name'); ?></h1>
				
				<p class="description"><?php the_field('product-description'); ?></p>

				<div class="sizes">
					<span>Available sizes</span>

					<?php if( have_rows('product_group') ): ?>

						<ul class="sizes-list">

						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);

						?>

						<li class="<?php if ($count == 1): ?>active<?php endif; ?>"><a href="#" data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>"><?php echo $product_size['size_qty'].' '.$product_size['size_type']; ?></a></li>

						<?php
							$count++;
							endwhile;
						?>

						</ul>

					<?php endif; ?>
				</div><!-- /.sizes -->

				<div class="cont-cta d-flex">
					<?php if (get_field('product_buy_online')) : ?>
					<div class="btn-victoria mr-3">
						<a href="<?php the_field('product_buy_online_url') ?>" title="Buy Online" class="btn-sub-victoria">buy online</a>
					</div>
					<?php endif; ?>
					<div class="btn-victoria">
						<a href="#" title="Write a Review" class="btn-sub-victoria">write a review</a>
					</div>
				</div><!-- /.cont-cta -->

				<?php echo do_shortcode('[ssba-buttons]'); ?>
			</div>
			<div class="col-12 col-md-6 col-lg-6">
				<div class="product-big-image">
					<?php if( have_rows('product_group') ): ?>
						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_image = get_sub_field('product_group_image');
							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);
							$product_nutritional = get_sub_field('product_group_nutritional_image');
						?>
						
						<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
							<img src="<?php echo $product_image; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
						</figure>

						<?php
							$count++;
							endwhile;
						?>
					<?php else: ?>
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('product-name'); ?>" />
						</figure>
					<?php endif; ?>
					<img class="img-indicator img-fluid" src="<?php echo $indicator_image; ?>" alt="Heal Indicator <?php the_field('product-heat-indicator'); ?>" />
				</div>
			</div>
		</div>
		<div class="row cards-product">
			<div class="col-12 col-md-6">
				<div class="card">
					<div class="card-header">
						<p>Ingredients</p>
					</div>
					<div class="card-body">
						<p class="card-text"><?php the_field('product-ingredients'); ?></p>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="card">
					<div class="card-header">
						<p>Nutrition Facts</p>
					</div>
					<div class="card-body">
						<?php if( have_rows('product_group') ): ?>

							<?php
								$count = 1;
								while( have_rows('product_group') ): the_row(); 

								$product_size = get_sub_field('product_group_size');
								$product_size_qty = str_replace('.', '', $product_size['size_qty']);
								$product_nutritional = get_sub_field('product_group_nutritional_image');

							?>

							<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
								<img src="<?php echo $product_nutritional; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
							</figure>

							<?php
								$count++;
								endwhile;
							?>

						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="title">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h3>Reviews</h3>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-12">
					
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
		</div>
</section>

<section class="other-products">
	<div class="container">
		<div class="row">
			<?php while ( $other_products->have_posts() ) : $other_products->the_post(); ?>
				<div class="col-12 col-lg-3">
					<div class="text-center item">
						<a class="link-product" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
							<figure>
								<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
							</figure>
							<p class=""><?php the_title(); ?></p>
						</a>
					</div>	
				</div><!-- /.col-12 col-lg-3 -->
			<?php endwhile; ?>
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.other-products -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<?php 
 get_footer();

