<?php 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

$other_products = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> 4,
		'order' => 'DESC'
	)
);
?> 

<?php get_header(); ?>

<?php
	global $indicator_image;
	global $organic_image;

	$indicator_value = get_field('product-heat-indicator');
	$organic_image = $upload_theme."product-icons-usda-non-gmo.png";

	switch ($indicator_value) {
		case 'extra hot':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-extra-hot.png';
			break;
		case 'hot':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-hot.png';
			break;
		case 'medium':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-medium.png';
			break;
		case 'mild':
			$indicator_image = $upload_theme.'la-victoria-heat-indicator-mild.png';
			break;
	}

	$post_type = get_post_type();
	$obj = get_post_type_object( $post_type );
	$custom_post_title = $obj->labels->singular_name;

?>

<section class="single-product">
	<div class="container">
		<div class="row info-product">
			<div class="col-12 col-sm-12 col-md-12 col-lg-5">
				<h1><?php the_field('product-name'); ?><small><?php the_field('product-heat-indicator'); ?></small></h1>

				<div class="product-reviews">
					<div id="pr-reviewsnippet-top-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
				</div>
				
				<p class="description"><?php the_field('product-description'); ?></p>

				<div class="sizes">
					<span>Available sizes</span>

					<?php if( have_rows('product_group') ): ?>

						<ul class="sizes-list">

						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);

						?>

						<li class="<?php if ($count == 1): ?>active<?php endif; ?>"><a href="#" data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>"><?php echo $product_size['size_qty'].' '.$product_size['size_type']; ?></a></li>

						<?php
							$count++;
							endwhile; wp_reset_postdata();
						?>

						</ul>

					<?php endif; ?>
				</div><!-- /.sizes -->

				<div class="cont-cta d-flex">
					<?php if (get_field('product_buy_online')) : ?>
					<div class="btn-victoria mr-3">
						<a href="<?php the_field('product_buy_online_url') ?>" title="Buy Online" class="btn-sub-victoria">buy online</a>
					</div>
					<?php endif; ?>

					<?php if (get_field('product_find_more')) : ?>
						<div class="btn-victoria mr-3">
							<a href="<?php the_field('product_find_more_url') ?>" title="<?php the_field('product_find_more_text') ?>" class="btn-sub-victoria"><?php the_field('product_find_more_text') ?></a>
						</div>
					<?php endif; ?>

					<?php if(get_field('page_id')): ?>
						<div class="btn-victoria">
							<a href="javascript:;" id="write-review" title="Write a Review" class="btn-sub-victoria">write a review</a>
						</div>
					<?php endif; ?>
				</div><!-- /.cont-cta -->

				<?php echo do_shortcode('[ssba-buttons]'); ?>
			</div>
			<div class="col-12 col-sm-12 col-md-11 col-lg-7">
				<div class="product-big-image">
					<?php if( have_rows('product_group') ): ?>
						<?php
							$count = 1;
							while( have_rows('product_group') ): the_row(); 

							$product_image = get_sub_field('product_group_image');
							$product_size = get_sub_field('product_group_size');
							$product_size_qty = str_replace('.', '', $product_size['size_qty']);
							$product_nutritional = get_sub_field('product_group_nutritional_image');
						?>
						
						<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
							<img src="<?php echo $product_image; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
						</figure>

						<?php
							$count++;
							endwhile; wp_reset_postdata();
						?>
					<?php else: ?>
						<figure>
							<img class="img-product img-fluid" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('product-name'); ?>" />
						</figure>
					<?php endif; ?>
					<img class="img-indicator img-fluid" src="<?php echo $indicator_image; ?>" alt="Heal Indicator <?php the_field('product-heat-indicator'); ?>" />
					<?php if ($custom_post_title == "Organic salsas"): ?>
						<img class="img-organic img-fluid" src="<?php echo $organic_image; ?>" alt="Organic Product" />
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div><!-- /.container -->
</section>

<section class="ingredients-product mb-5">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-md-6 p-0">
				<div class="ingredients text-center">
					<h2 class="title-ingredients mb-4">Ingredients</h2>
					<div class="text-ingredients mb-5"><?php the_field('product-ingredients'); ?></div>
				</div>
				<div class="nutrition d-flex justify-content-center">
					<div class="btn-victoria">
						<a href="#" class="btn-sub-victoria" data-toggle="modal" data-target="#nutritionalModal">View Nutritional Information</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="featured-recipes-home cont-reviews">
	<div class="title mb-4">
		<div class="container">
			<div class="row">
				<div class="col-12 d-flex justify-content-center">
					<h2>Reviews</h2>
				</div>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.title -->
	<div class="content">
		<div class="container">
			<div class="row columns">
				<div class="col-md-5 col-12">
					<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet"></div>
					<div id="pr-reviewhistogram" class="p-w-r">
						<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
					</div>
					<div class="cont-write-review"></div><!-- /.cont-write-review -->

				</div><!-- /.col-5 -->
				<div class="col-md-7 col-12">
					<div id="pr-reviewdisplay-<?php the_field('page_id'); ?>" class="cont-review-display"></div>
				</div><!-- /.col-7 -->
			</div><!-- /.row -->
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="container">
		<div class="row">
			<div class="col-12 d-flex justify-content-center">
				<h2>Explore our other products</h2>
			</div>
		</div>
		<div class="row items-products">
			<div class="all-products-carousel owl-carousel owl-theme">
				<?php while ( $other_products->have_posts() ) : $other_products->the_post(); ?>
					<div class="text-center item">
						<a class="link-product" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
							<figure>
								<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title(); ?>" />
							</figure>
							<p class=""><?php the_title(); ?></p>
						</a>
					</div>	
				<?php endwhile; wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>

<!-- Modal -->
<div class="modal fade nutritional-pop-up" id="nutritionalModal" tabindex="-1" role="dialog" aria-labelledby="nutritionalModalLabel" aria-hidden="true">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	    <figure><img src="<?php echo $upload_theme ?>close-white.png" class="img-fluid" width="auto" height="auto" /></figure>
	</button>
	<div class="modal-dialog d-flex justify-content-center align-items-center" role="document">
		<?php if( have_rows('product_group') ): ?>

			<?php
				$count = 1;
				while( have_rows('product_group') ): the_row(); 

				$product_size = get_sub_field('product_group_size');
				$product_size_qty = str_replace('.', '', $product_size['size_qty']);
				$product_nutritional = get_sub_field('product_group_nutritional_image');

			?>

			<figure data-position="<?php echo $count; ?>" data-size="<?php echo $product_size_qty; ?>" data-status="<?php if ($count == 1): ?>active<?php else: ?>inactive<?php endif; ?>">
				<img src="<?php echo $product_nutritional; ?>" class="img-product img-fluid" alt="<?php the_field('product-name'); ?>"/>
			</figure>

			<?php
				$count++;
				endwhile; wp_reset_postdata();
			?>

		<?php endif; ?>
	</div>
</div>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>

<script type="text/javascript" charset="utf-8">
	POWERREVIEWS.display.render(
		[
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: '/la-victoria/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: '/wp-content/themes/la-victoria-theme/inc/assets/css/custom/reviews.css',
				components: {
					CategorySnippet: 'pr-reviewsnippet-top-<?php the_field('page_id'); ?>',
				}
			},
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: '/la-victoria/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: '/wp-content/themes/la-victoria-theme/inc/assets/css/custom/reviews.css',
				on_render: function(config, data) {
					jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
					jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn-sub-victoria');
					jQuery('.cont-write-review').addClass('btn-write-review');
				},
				components: {
					ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
					ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>'
				}
			}
		]
	);
</script>

<?php get_footer(); ?>

