<?php 
	switch_to_blog(1);
	$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?>
<div class="top-header">
	<div class="top-header-collapse">
		<div class="row align-items-center m-0">
			<div class="dropdown-salsas">
				<div class="salsa-logo">
					<a href="/" title="Salsas Site">
						<figure>
							<img class="img-fluid" src="<?php echo $upload_theme ?>logo-icon.png" alt="Other favorite BRANDS" width="auto" height="auto" />
						</figure>
					</a>
					<a class="btn-other-brands" href="#" role="button" title="Other favorite brands"><p>Other favorite brands</p></a>
				</div><!-- /.salsa-logo -->
				<div class="collapse other-brands-collapse" id="otherBrandsCollapse">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<?php
									wp_nav_menu(array(
										'theme_location'    => 'other-brands',
										'container'       => 'div',
										'container_id'    => 'main-nav',
										'container_class' => 'other-brands-nav',
										'menu_id'         => false,
										'depth'           => 3,
										'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
										'walker'          => new wp_bootstrap_navwalker()
									));
								?>
							</div><!-- /.col-12 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</div>
			</div><!-- /.col-6 -->
		</div><!-- /.row -->
	</div>
</div><!-- /.top-header -->
<?php restore_current_blog(); ?>