<?php
	$id_page = 1;
	switch_to_blog($id_page); 
?>

<div class="nav-fixed">
	<h5>Filter All Recipes</h5>
	<div class="cont-options">
		<?php if (get_field('la_victoria_filter_id', 'option')): ?>
			<?php $victoria_filter_id = get_field( 'la_victoria_filter_id', 'option' ); ?>
			<?php echo do_shortcode( '[searchandfilter id="'.$victoria_filter_id.'"]' ); ?>
		<?php else: ?>
			<p>The filter is disabled.</p>
		<?php endif; ?>
	</div><!-- /.cont-options -->
</div><!-- /.nav-fixed -->

<?php restore_current_blog(); ?>