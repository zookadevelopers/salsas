<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */


$id = $searched_post->ID;
$is_recipe = $searched_post->is_recipe;
$searched_post_slug 	= $searched_post->post_name;
$searched_post_title 	= $searched_post->post_title;
$site_url 			= get_site_url();
?>


<div class="col-12 item-search">
	<article id="post-<?php echo $id; ?>"  >
		<div class="entry-header">
			<?php 
				if ($is_recipe): 
					$recipe_url = $site_url.'/recipe/'.$searched_post_slug;
					echo sprintf( '<p class="entry-title"><a href="%s" rel="bookmark">%s', $recipe_url , $searched_post_title ); echo '</a></p>';
				else:
					$permalink = get_post_permalink($id);
					echo sprintf( '<p class="entry-title"><a href="%s" rel="bookmark">%s', esc_url( get_permalink() ), $searched_post_title ); echo '</a></p>';
				endif 
			?>
		</div><!-- .entry-header -->

		<div class="entry-summary">
			<?php if (get_the_excerpt($id) == false): ?>
				<p><?php the_field('product-description'); ?></p>			
			<?php endif ?>
				<?php the_excerpt($id); ?>
		</div><!-- .entry-summary -->

	</article><!-- #post-## -->
	
</div>
	