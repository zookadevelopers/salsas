<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
global $brandLogo;

switch_to_blog(1);
$salsas_domain = get_site_url();

$brandLogoCms = get_field('maria_logo', 'option');

if ($brandLogoCms) {
	$brandLogo = $brandLogoCms;
} else {
	$brandLogo = $upload_theme.'logo-dona-maria.png';
}

restore_current_blog();

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php get_template_part( 'template-parts/header-seo-tags' ); ?>
<?php wp_head(); ?>
<?php if (is_page_template('page-recipe.php')): ?>
	<link rel="stylesheet" id="salsas-recipes-css" href="<?php echo $salsas_domain; ?>/wp-content/themes/salsas-theme/inc/assets/css/custom/pages/recipes.css" type="text/css" media="all" />
<?php endif ?>
</head>
<div class="loader d-none"><img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="" /></div>
<body <?php body_class(); ?>>
	<?php get_template_part( 'template-parts/mobile-menu' );?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
	<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
		<?php get_template_part( 'template-parts/announcer-bar' ); ?>
		<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?> main-header" role="banner">
			<div class="container">
				<nav class="navbar navbar-expand-md row">
					<div class="col d-block d-md-none p-0 text-center">
						<button class="ssm-toggle-nav hamburger-menu"><i class="fas fa-bars"></i></button>
					</div><!-- /.col-12 col-2 -->
					<div class="col-8 col-md-3 col-lg-2">
						<div class="navbar-brand">
							<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
								<a href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" class="img-fluid">
								</a>
							<?php else : ?>
								<a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?php echo $brandLogo; ?>" class="img-fluid" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
							<?php endif; ?>

						</div>
					</div><!-- /.col-12 col-lg-4 -->
					<div class="col d-block d-md-none p-0 text-center">
                        <button class="ssm-toggle-nav search-button hamburger-menu"><i class="fas fa-search"></i></button>
                    </div><!-- /.col-12 col-2 -->
					<div class="col-12 col-md-9 col-lg-10 d-md-flex align-items-center">
						<?php
							wp_nav_menu(array(
								'theme_location'    => 'primary',
								'container'       => 'div',
								'container_id'    => 'main-nav',
								'container_class' => 'collapse navbar-collapse justify-content-center header-nav',
								'menu_id'         => false,
								'menu_class'      => 'navbar-nav',
								'depth'           => 3,
								'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
								'walker'          => new wp_bootstrap_navwalker()
							));
						?>
						<?php get_search_form(); ?>
					</div><!-- /.col-12 col-lg-8 -->
				</nav>
			</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content pt-0">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 p-0">
			
	<?php endif; ?>