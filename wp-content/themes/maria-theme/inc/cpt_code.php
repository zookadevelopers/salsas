<?php
function cptui_register_my_cpts() {

	/**
     * Post Type: Categories.
     */

    $labels = array(
        "name" => __( "Categories", "wp-maria-theme" ),
        "singular_name" => __( "Category", "wp-maria-theme" ),
        "add_new" => __( "Add New Category", "wp-maria-theme" ),
    );

    $args = array(
        "label" => __( "Categories", "wp-maria-theme" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "products", "with_front" => true ),
        "query_var" => true,
        "menu_icon" => "/maria/wp-content/uploads/sites/4/2018/11/icon-product.png",
        "supports" => array( "title", "editor", "thumbnail", "custom-fields", "page-attributes" ),
    );

    register_post_type( "products", $args );

	/**
	 * Post Type: Banner Sliders.
	 */

	$labels = array(
		"name" => __( "Banner Sliders", "wp-maria-theme" ),
		"singular_name" => __( "Banner Slider", "wp-maria-theme" ),
	);

	$args = array(
		"label" => __( "Banner Sliders", "wp-maria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "banner_slider", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "http://206.189.231.128/lavictoria/wp-content/uploads/sites/6/2018/12/icon-slider.png",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "banner_slider", $args );

	/**
	 * Post Type: Mole.
	 */

	$labels = array(
		"name" => __( "Mole", "wp-maria-theme" ),
		"singular_name" => __( "Mole", "wp-maria-theme" ),
	);

	$args = array(
		"label" => __( "Mole", "wp-maria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/mole", "with_front" => true ),
		"query_var" => "mole",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "mole", $args );

	/**
	 * Post Type: Ready To Serve Mole.
	 */

	$labels = array(
		"name" => __( "Ready To Serve Mole", "wp-maria-theme" ),
		"singular_name" => __( "Ready To Serve Mole", "wp-maria-theme" ),
	);

	$args = array(
		"label" => __( "Ready To Serve Mole", "wp-maria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/ready-to-serve-mole", "with_front" => true ),
		"query_var" => "serve_mole",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "ready_to_serve_mole", $args );

	/**
	 * Post Type: Nopalitos.
	 */

	$labels = array(
		"name" => __( "Nopalitos", "wp-maria-theme" ),
		"singular_name" => __( "Nopalitos", "wp-maria-theme" ),
	);

	$args = array(
		"label" => __( "Nopalitos", "wp-maria-theme" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => "edit.php?post_type=products",
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "products/nopalitos", "with_front" => true ),
		"query_var" => "nopalitos",
		"supports" => array( "title", "editor", "thumbnail", "page-attributes" ),
	);

	register_post_type( "nopalitos", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );
