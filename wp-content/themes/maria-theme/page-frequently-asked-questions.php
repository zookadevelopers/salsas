<?php 

// PostType Page Template: Single page product

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 <section class="contact-page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Frequently Asked Questions</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 tabs-faq">
                <ul class="nav nav-pills d-flex justify-content-between row" id="pills-tab" role="tablist">
                    <li class="nav-item col-12 col-md-4">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Recipes</a>
                    </li>
                    <li class="nav-item col-12 col-md-4">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Products</a>
                    </li>
                    <li class="nav-item col-12 col-md-4">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Ingredients</a>
                    </li>
                </ul>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <?php the_field('content_recipes'); ?>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <?php the_field('content_products'); ?>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <?php the_field('content_ingredients'); ?>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="contact-faq">
                    <p>Other questions, comments or feedback? We can help you. </p>
                    <a class="btn btn-primary" href="contact">Contact us</a>
                </div>
            </div>
        </div>
    </div>
</section> 

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
