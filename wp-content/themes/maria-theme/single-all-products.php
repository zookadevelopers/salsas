<?php 
// Template Name: All Products


// all Products Query
$all_products = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
		'orderby'=>'menu_order'
	)
); 

// Products Query
$products_order = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
        'order' => 'ASC',
        'orderby'=>'menu_order'
	)
); 
$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>	
	<section class="banner-all-products text-product">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
			<div class="row items-all-products">
				<?php 
				$show = get_field('hide_in_banner');
				 ?>
				<?php while ( $products_order->have_posts() ) : $products_order->the_post(); 
					if ( !in_array( "yes", get_field( 'hide_in_banner' ) ) ) {
						$image_id = get_post_thumbnail_id();
						$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
						$image_title = get_the_title($image_id);
				?>
					<div class="col-6 col-md-4 col-lg-3 text-center item">
						<a class="link-product" title="<?php the_title(); ?>" href="<?php the_permalink(); ?>">
							<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
							<p class="mt-2"><?php the_title(); ?></p>
						</a>
					</div>
				<?php }  endwhile; wp_reset_postdata();?>
			</div>
		</div>
	</section>

	<section class="products-home product-category all-product">
		<div class="container">
			<?php while ( $all_products->have_posts() ) : $all_products->the_post(); 
			$posSlug = get_field('post_type_slug');
			$posSlug = str_replace(" ","_",$posSlug);

			// one Product Query
			$one_product = new WP_Query(
				array(
					'post_type'=>$posSlug, 
					'post_status'=>'publish', 
					'posts_per_page'=> -1,
					'order' => 'ASC',
					'orderby'=>'menu_order'
				)
			);
			?>
				<div id="<?php echo $post->post_name; ?>" class="row name-product">
					<div class="col-12">
						<a href="<?php the_permalink(); ?>" title="<?php the_field('title_category'); ?>"><h3><?php the_field('title_category'); ?></h3></a>
					</div>
					<div class="col-12 description-product mt-3">
						<?php the_content() ?>
					</div>
				</div>
				<div class="row items-products list-product">
					<?php while ( $one_product->have_posts() ) : $one_product->the_post(); ?>
						<?php
							$pageId = get_field('page_id');
							$postId = get_the_ID();
							$name = get_the_title();
							$imgUrl = get_the_post_thumbnail_url();
							$currentUrl = home_url( $wp->request );

							$apiKey = '59fb8bdf-cb43-490b-8b09-975d68551c2a';
							$merchantGroup = '78368';
							$merchantId = '278593';

							if ($pageId != '') {
								$objPowerReviews[] = [
									'api_key' => $apiKey,
									'locale' => 'en_US',
									'merchant_group_id' => $merchantGroup,
									'merchant_id' => $merchantId,
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/maria-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
									]
								];
							}

							$image_id = get_post_thumbnail_id();
							$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
							$image_title = get_the_title($image_id);
						?>
						<div class="col-6 col-md-4 col-lg-3 item">
							<a class="link-product" title="<?php the_field('product-name'); ?>" href="<?php the_permalink(); ?>">
								<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
								<p class=""><?php the_field('product-name'); ?></p>
							</a>
							<div class="all-product-reviews">
								<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
							</div>
							<?php if ($pageId != '') { ?>
								<a href="/dona-maria/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="review-link">Write a Review</a>
							<?php } ?>

						</div>
					<?php endwhile; wp_reset_postdata();?>
				</div>
			<?php endwhile; wp_reset_postdata(); ?> 
		</div>
		<a class="back-top" title="Back to Top">
			<figure>
				<img class="img-fluid" src="<?php echo $upload_theme; ?>back-to-top.png" width="auto" height="auto" alt="Back to Top" />
			</figure>
		</a>
	</section>




<script type="text/javascript" charset="utf-8">
	var currentUrl = window.location.href;
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
