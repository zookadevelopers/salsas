<?php 
// Template Name: Products

// Products Home
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC'
	)
); 

$posInfo = get_post();
// var_dump($posInfo);
$posName = get_field('post_type_slug');
$posName = str_replace(" ","_",$posName);

// Products Query
$products = new WP_Query(
	array(
		'post_type'=>$posName, 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
        'orderby'=>'menu_order'
	)
);

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
$categoryOtherProducts = get_field('other_products_category');
?> 

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php $postId =  get_the_ID(); ?>
	<?php
		global $category_image;

		$banner_value = get_field('category_banner_image');

		if( $banner_value ) {
			$category_image = $banner_value;
		} else {
			$category_image = $upload_theme.'banner-products.jpg';
		}
	?>
<section class="banner-product container-fluid position-relative" title="<?php the_field('title_category'); ?>" style="background-image: url('<?php echo $category_image; ?>');">
</section>

<section class="text-under-banner text-product py-4">
	<div class="container">
		<div class="col-12 p-0 pb-4 pb-md-1">
			<h1><?php the_field('title_category'); ?></h1>
			<?php the_content() ?>
		</div>
	</div>
</section>

<section class="products-home product-category">
	<div class="container">
		<div class="row items-products">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				<?php
					$pageId = get_field('page_id');
					$postId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$apiKey = '59fb8bdf-cb43-490b-8b09-975d68551c2a';
					$merchantGroup = '78368';
					$merchantId = '278593';

					if ($pageId != '') {
						$objPowerReviews[] = [
							'api_key' => $apiKey,
							'locale' => 'en_US',
							'merchant_group_id' => $merchantGroup,
							'merchant_id' => $merchantId,
							'page_id' => strval($pageId),
							'style_sheet' => '/wp-content/themes/maria-theme/inc/assets/css/custom/reviews.css',
							'review_wrapper_url' => '/dona-maria/add-review?pr_page_id='.$pageId,
							'components' => [
								'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
							]
						];
					}
					$image_id = get_post_thumbnail_id();
					$image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', TRUE);
					$image_title = get_the_title($image_id);
				?>
				<div class="col-6 col-md-4 col-lg-3 item">
					<a class="link-product" title="<?php the_field('product-name'); ?>" href="<?php the_permalink(); ?>">
						<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php echo ($image_alt) ? $image_alt : $image_title ?>" />
						<p class=""><?php the_field('product-name'); ?></p>
					</a>
					<div class="all-product-reviews">
						<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
					</div>
					<?php if ($pageId != '') { ?>
						<a href="/dona-maria/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="review-link">Write a Review</a>
					<?php } ?>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>

<?php if( $categoryOtherProducts ): ?>
<section class="featured-recipes-home p-0">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="text-center text-md-left title-carousel">Explore our other products</h3>
			</div>
		</div>
		<div class="row items-products">
			<div class="all-products-carousel owl-carousel owl-theme">
				<?php foreach( $categoryOtherProducts as $post): ?>
				<?php setup_postdata($post); ?>
				<div class="text-center item">
					<a class="link-product" title="<?php the_field('title_category'); ?>" href="<?php the_permalink(); ?>">
						<figure>
							<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('title_category'); ?>" />
						</figure>
						<p><?php the_field('title_category'); ?></p>
					</a>
				</div>
				<?php endforeach; ?>
				<?php wp_reset_postdata(); ?>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<script type="text/javascript" charset="utf-8">
	var currentUrl = window.location.href;
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>
<?php 
 get_footer();
