<?php
	$id_page = 1;
	switch_to_blog($id_page); 
?>

<div class="nav-fixed">
	<div class="center d-flex align-items-center">
		<svg xmlns="http://www.w3.org/2000/svg" width="19" height="20" viewBox="0 0 30 31" class="d-md-none mr-3">
			<g fill="none" fill-rule="evenodd">
				<rect width="30" height="3" y="4" fill="#000" rx="1"/>
				<rect width="30" height="3" y="14" fill="#000" rx="1"/>
				<rect width="30" height="3" y="24" fill="#000" rx="1"/>
				<circle cx="7.5" cy="15.5" r="2.5" fill="#FFF"/>
				<circle cx="17.5" cy="5.5" r="2.5" fill="#FFF"/>
				<circle cx="23.5" cy="25.5" r="2.5" fill="#FFF"/>
				<path fill="#000" fill-rule="nonzero" d="M17.5 10.25a4.75 4.75 0 1 1 0-9.5 4.75 4.75 0 0 1 0 9.5zm0-2.5a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5zM7.5 20.25a4.75 4.75 0 1 1 0-9.5 4.75 4.75 0 0 1 0 9.5zm0-2.5a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5zM23.5 30.25a4.75 4.75 0 1 1 0-9.5 4.75 4.75 0 0 1 0 9.5zm0-2.5a2.25 2.25 0 1 0 0-4.5 2.25 2.25 0 0 0 0 4.5z"/>
			</g>
		</svg><h5 class="m-0">Filter All Recipes</h5>
	</div><!-- /.title -->
	<div class="cont-options">
		<?php if (get_field('maria_filter_id', 'option')): ?>
			<?php $maria_filter_id = get_field( 'maria_filter_id', 'option' ); ?>
			<?php echo do_shortcode( '[searchandfilter id="'.$maria_filter_id.'"]' ); ?>
		<?php else: ?>
			<p>The filter is disabled.</p>
		<?php endif; ?>
	</div><!-- /.cont-options -->
</div><!-- /.nav-fixed -->

<?php restore_current_blog(); ?>