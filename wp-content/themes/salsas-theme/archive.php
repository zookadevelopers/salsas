<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); 
$upload_theme = get_template_directory_uri().'/inc/assets/img/'; 

if ( have_posts() ) {
	$column_results = 'col-12 col-md-8 col-lg-9 col-xl-8';
} else {
	$column_results = 'col-12 col-md-12 col-lg-12 col-xl-12';
} ?>

<style>	
	.sub-header-filter.overlay::before {
		background-color: rgba(0, 0, 0, .<?php the_field('category_page_opacity', 'option') ?>) !important;	
	}
</style>


<section id="primary" class="content-area col-12 col-sm-12 col-lg-12 p-0">
	<div class="container-fluid normal">
		<div class="row">
			<?php if ( have_posts() ) : ?>
				<div class="col-12 col-lg-3 col-md-4 left-filters d-none d-md-block">
					<?php get_template_part( 'template-parts/category-filters' ); ?>
				</div><!-- /.col-12 col-lg-3 -->
			<?php endif; ?>

			<div class="<?php echo $column_results; ?> right-recipes">
				<main id="main" class="site-main" role="main">
					<div class="container">
						<?php if ( have_posts() ): ?>
						<div class="d-block d-md-none">
							<div class="row">
								<div class="col-12">
									<a href="javascript:;" class="btn-filter-recipes title-filters ssm-toggle-nav" title="Filter All Recipes"><img src="<?php echo $upload_theme; ?>icons/icon-filter.svg" class="img-fluid" /> Filter All Recipes</a>
								</div><!-- /.col-12 -->
							</div><!-- /.row -->
						</div><!-- /.d-block d-md-none -->

						<div class="row justify-content-sm-center justify-content-lg-start">
							<?php while ( have_posts() ) : the_post();

							if ( 'post' === get_post_type() ):

								$pageId = get_field('page_id');
								$postId = get_the_ID();
								$name = get_the_title();
								$imgUrl = get_the_post_thumbnail_url();
								$currentUrl = get_permalink();
								$allposttags = get_the_tags();
								$i=0;
								if ($allposttags) {
									foreach($allposttags as $tags) {
										$i++;
										if (1 == $i) {
											$firsttag = $tags->name;
										}
									}
								}

								$apiKey = '59fb8bdf-cb43-490b-8b09-975d68551c2a';
								$merchantGroup = '78368';
								$merchantId = '278593';

								$objPowerReviews[] = [
									'api_key' => $apiKey,
									'locale' => 'en_US',
									'merchant_group_id' => $merchantGroup,
									'merchant_id' => $merchantId,
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-cat-'.$pageId
									]
								];

								$imgUrl = get_field('recipe-salsas-image');

								if ($imgUrl == '') {
									$imgUrl = get_the_post_thumbnail_url();
								}
							?>
							<div class="col-xl-4 col-lg-6 col-md-6 col-sm-5 col-6 px-md-3 px-2">
								<div id="post-<?php the_ID(); ?>" class="item type-post hentry" data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

									<a href="<?php echo esc_url( get_permalink()); ?>" title="<?php the_field('sals-recipe-title'); ?>" class="d-block">
										<div class="post-thumbnail category-post-image" style="background-image: url(<?php echo $imgUrl; ?>)">
											<?php the_post_thumbnail(); ?>
										</div>
										<div class="entry-header">
											<p class="entry-title"><?php the_field('sals-recipe-title'); ?></p>
											<div class="brand">With <?php the_field('sals-recipe-author'); ?></div>
										</div><!-- .entry-header -->
										<div id="pr-reviewsnippet-cat-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
									</a>
									<?php if ($pageId != '') : ?>
									<a href="/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="btn btn-link btn-write-review-link">Write a Review</a>
									<?php endif; ?>
								</div><!-- #post-## -->
							</div>

							<?php endif; ?>

							<?php if ( is_page_template('search.php')): ?>

							<?php
								$pageId = get_field('page_id');
								$postId = get_the_ID();
								$name = get_the_title();
								$imgUrl = get_the_post_thumbnail_url();
								$currentUrl = get_permalink();
								$allposttags = get_the_tags();
								$i=0;
								if ($allposttags) {
									foreach($allposttags as $tags) {
										$i++;
										if (1 == $i) {
											$firsttag = $tags->name;
										}
									}
								}

								$objPowerReviews[] = [
									'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
									'locale' => 'en_US',
									'merchant_group_id' => '78368',
									'merchant_id' => '278593',
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-search-'.$pageId
									]
								];
							?>

							<div class="col-xl-4 col-lg-6 col-md-6 col-sm-5 col-6">
								<div id="post-<?php the_ID(); ?>" class="item type-post hentry" data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

									<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
										<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
											<?php the_post_thumbnail(); ?>
										</div>
										<div class="entry-header">
											<p class="entry-title">
												<a href="<?php echo esc_url( get_permalink()); ?>" rel="bookmark"><?php the_field('sals-recipe-title'); ?></a>
											</p>
											<div class="brand">With <?php the_field('sals-recipe-author'); ?></div>
										</div><!-- .entry-header -->

										<div id="pr-reviewsnippet-search-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
									</a>
								</div><!-- #post-## -->
							</div>
							<?php endif; ?>

						<?php endwhile;

						else :
							?>
							<div class="col-12 my-5 text-center">
								<h3 class="mb-5">The selected filters do not have related recipes, we invite you to select other options.</h3>
							</div><!-- /.col-12 -->
							<?php

						endif; ?>
					</div><!-- /.row -->
				</div>
			</main><!-- #main -->
		</div>
	</div>
</div>

</section><!-- #primary -->
<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>
<?php get_footer(); ?>
