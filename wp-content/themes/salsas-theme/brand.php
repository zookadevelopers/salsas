<?php 
// Template Name: Brands
get_header(); ?>

<section class="cont-all-brands">
	<div class="container">
		<div class="row justify-content-center text-center">
			<div class="col-12 col-lg-9">
				<h1><?php the_field('brands_title'); ?></h1>
				<p><?php the_field('brands_description'); ?></p>
			</div>
		</div>
		<?php if( have_rows('brands_group') ): ?>

		<div class="row justify-content-center align-items-center">

			<?php while( have_rows('brands_group') ): the_row(); 

				$brand_name = get_sub_field('brand_name');
				$brand_image = get_sub_field('brand_image');
				$brand_link = get_sub_field('brand_link');

			?>
				
			<div class="col-12 col-md-4 item">
				<a class="d-block" href="<?php echo $brand_link; ?>" target="_blank">
					<img src="<?php echo $brand_image; ?>" class="img-fluid" alt="<?php echo $brand_name; ?>" />
				</a>
			</div>

			<?php endwhile; ?>

		</div>

		<?php endif; ?>
	</div><!-- /.container -->
</section><!-- /.cont-brands -->

<?php get_template_part( 'template-parts/popular-recipes-groups'); ?>

<?php get_footer(); ?>
