<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
				</div><!-- /.col-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<?php get_template_part( 'footer-widget' ); ?>

		<div class="container pt-3 pb-3">
            <div class="site-info d-flex align-items-center">
            	<?php dynamic_sidebar( 'footer-copyright' ); ?>
                <span class="sep text-white"> | </span> Copyright <?php echo date('Y'); ?> &copy;&nbsp; <?php echo '<a href="'. site_url() .'" target="_blank">salsas.com</a>'; ?>
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div>
</div><!-- #page -->

<?php wp_footer(); ?>
<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
</body>
</html>