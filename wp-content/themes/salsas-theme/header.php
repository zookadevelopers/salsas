<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if (is_single()) : ?>
		
	<?php 
		$post = get_post( $post_id );
		$slug = $post->post_name;
		$allposttags = get_the_tags();
        $categories = get_the_category();
        $authorName = get_the_author_meta( 'display_name', $post->post_author);
        $directions = get_field('sals-recipe-directions');

        $imgUrl = get_field('recipe-salsas-image');

        if ($imgUrl == '') {
            $imgUrl = get_the_post_thumbnail_url();
        }

        $prepTime = strtolower(str_replace('', ',', get_field('sals-recipe-prep-time')));
        preg_match_all('!\d+!', $prepTime, $matchesPrepTime);

        if (isset($matchesPrepTime[0][1])) {
            $prepTime = 'PT'.$matchesPrepTime[0][0].'H'.$matchesPrepTime[0][1].'M';
        } else {
            $time = (strpos($prepTime, 'Hours')) ? 'H' : 'M';
            $prepTime = 'PT'.$matchesPrepTime[0][0].$time;
        }

        $cookTime = strtolower(str_replace('', ',', get_field('sals-recipe-cook-time')['sals-cook-time-label']));
        preg_match_all('!\d+!', $cookTime, $matchesCookTime);

        if (isset($matchesCookTime[0][0])) {
            $cookTime = 'PT'.get_field('sals-recipe-cook-time')['sals-cook-time-time'].'H'.$matchesCookTime[0][0].'M';
        } else {
            $time = (strpos($cookTime, 'hours')) ? 'H' : 'M';
            $cookTime = 'PT'.get_field('sals-recipe-cook-time')['sals-cook-time-time'].$time;
        }

        $totalTime = strtolower(str_replace('', ',', get_field('sals-recipe-total-time')));
        preg_match_all('!\d+!', $totalTime, $matchesTotalTime);

        if (isset($matchesTotalTime[0][1])) {
            $totalTime = 'PT'.$matchesTotalTime[0][0].'H'.$matchesTotalTime[0][1].'M';
        } else {
            $time = (strpos($totalTime, 'hours')) ? 'H' : 'M';
            $totalTime = 'PT'.$matchesTotalTime[0][0].$time;
        }

        $videoUrl = get_field("sals-recipe-video");

        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $videoUrl, $match);

        if(!empty($match[0])) {
            $videoUrl = $match[0][0];
        }

		$i=0;
		if ($allposttags) {
			foreach($allposttags as $tags) {
				$i++;
				if (1 == $i) {
					$firsttag = $tags->name;
				}
			}
		}
	?>
	<meta name="description" content="<?php the_field('sals-recipe-description'); ?>"/>
	<link rel="canonical" href="<?php the_field('recipe-salsas-canonical-url'); ?>" />
	<meta property="og:description" content="<?php the_field('sals-recipe-description'); ?>" />
	<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:site_name" content="Salsas" />
	<meta property="og:title" content="<?php the_field('sals-recipe-title'); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo get_permalink(get_the_ID(), false); ?>" />
	<meta property="article:published_time" content="<?php the_time('c'); ?>" />
	<meta property="article:section" content="<?php echo $firsttag; ?>" />
	<meta property="article:tag" content="<?php echo $firsttag; ?>" />	
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:description" content="<?php the_field('sals-recipe-description'); ?>" />
	<meta name="twitter:image" content="<?php the_post_thumbnail_url(); ?>">
	<meta name="twitter:title" content="<?php the_field('sals-recipe-title'); ?>" />

    <!-- Object to Google Assistant -->
    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Recipe",
            "name": "<?php the_field('sals-recipe-title'); ?>",
            "image": [
                "<?php echo $imgUrl; ?>"
            ],
            "author": {
                "@type": "Person",
                "name": "<?php echo $authorName; ?>"
            },
            "datePublished": "<?php echo $post->post_modified; ?>",
            "description": "<?php the_field('sals-recipe-description'); ?>",
            "prepTime": "<?php echo $prepTime; ?>",
            "cookTime": "<?php echo $cookTime; ?>",
            "totalTime": "<?php echo $totalTime; ?>",
            "keywords": "<?php the_field('sals-recipe-title'); ?>",
            "recipeYield": "<?php the_field('sals-recipe-servings'); ?>",
            "recipeCategory": "<?php echo esc_html( $categories[0]->name ); ?>",
            "recipeCuisine": "American",
            "nutrition": {
                "@type": "NutritionInformation",
                "calories": "270 calories"
            },
            "recipeIngredient": [
                "<?php echo strip_tags($post->post_content); ?>"
            ],
            "recipeInstructions": [
                {
                    "@type": "HowToStep",
                    "text": "<?php echo strip_tags($directions); ?>"
                }
            ],
            "review": {
                "@type": "Review",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "4",
                    "bestRating": "5"
                },
                "author": {
                    "@type": "Person",
                    "name": "Julia Benson"
                },
                "datePublished": "2018-05-01",
                "reviewBody": "This cake is delicious!",
                "publisher": "The cake makery"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "ratingCount": "18"
            },
            "video": {
                "name": "<?php the_field('sals-recipe-title'); ?>",
                "description": "<?php the_field('sals-recipe-description'); ?>",
                "thumbnailUrl": [
                    "<?php echo $imgUrl; ?>"
                ],
                "contentUrl": "<?php echo $videoUrl; ?>",
                "embedUrl": "<?php echo $videoUrl; ?>",
                "uploadDate": "<?php echo $post->post_modified; ?>"
            }
        }
    </script>

	<?php endif;?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<?php get_template_part( 'template-parts/mobile-menu' );?>
	<div class="loader d-none"><img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="" /></div>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
	<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
		<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?> main-header" role="banner">
			<div class="container-fluid">
				<nav class="navbar navbar-expand-md row">
					<div class="col d-block d-md-none p-0 text-center">
						<button class="ssm-toggle-nav hamburger-menu" aria-label="mobile navigation" role="presentation"><i class="fas fa-bars"></i></button>
					</div><!-- /.col-12 col-2 -->
					<div class="col-8 col-md-4 col-lg-3 p-0">
						<div class="navbar-brand w-100">
							<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
								<a href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" class="img-fluid logo" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
									<img src="<?php echo $upload_theme; ?>logo-icon.png" class="img-fluid logo-icon" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
								</a>
							<?php else : ?>
								<a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>">
                                    <img src="<?php echo $upload_theme; ?>logo-salsas.png" class="img-fluid logo" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                    <img src="<?php echo $upload_theme; ?>logo-icon.png" class="img-fluid logo-icon" width="auto" height="auto" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                </a>
							<?php endif; ?>
						</div>
					</div><!-- /.col-12 col-lg-4 -->
                    <div class="col d-block d-md-none p-0 text-center">
                        <button class="ssm-toggle-nav search-button hamburger-menu" aria-label="mobile navigation" role="presentation"><i class="fas fa-search"></i></button>
                    </div><!-- /.col-12 col-2 -->
					<div class="col-12 col-md-9 d-flex align-items-center">
						<?php
							if (is_front_page()) { 
								$notHome = ' d-flex justify-content-end';
							}

							wp_nav_menu(array(
								'theme_location'    => 'primary',
								'container'       => 'div',
								'container_id'    => 'main-nav',
								'container_class' => 'collapse navbar-collapse justify-content-end header-nav',
								'menu_id'         => false,
								'menu_class'      => 'navbar-nav'.$notHome,
								'depth'           => 3,
								'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
								'walker'          => new wp_bootstrap_navwalker()
							));
						?>
						<div class="d-none d-md-block">
                            <?php get_search_form(); ?>
						</div><!-- /.d-none d-md-block -->
					</div><!-- /.col-12 col-lg-8 -->
				</nav>
			</div>
		</header><!-- #masthead -->

	<div id="content" class="site-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 p-0">
			
	<?php endif; ?>