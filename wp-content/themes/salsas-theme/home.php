<?php 
// Template Name: Home
get_header(); ?>

<div id="page-sub-header" class="sub-header-filter header-home" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12 text-center">
				<h2 class="sup-title">Find a recipe</h2>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-12 col-lg-6">
				<?php get_search_form(); ?>
			</div><!-- /.col-12 col-lg-8 -->
		</div><!-- /.row -->
	</div>
	<div class="cont-categories-filters">
		<div class="container">
			<div class="row">
				<div class="col-12 text-center">
					<a href="javascript:;" class="d-block d-md-none ssm-toggle-nav title-filters" title="Browse by category">
						<h2>Browse by category</h2>
					</a>
					<div class="d-none d-md-block">
						<h2>Browse by category</h2>
						<div class="col-12">
							<?php // Parent categories dropdowns ?>
							<?php  wp_list_categories( array(
								'orderby' => 'ID'
							)); ?> 
						</div><!-- /.col-12 -->
					</div><!-- /.d-none d-md-block -->
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</div><!-- /.cont-categories-filters -->
</div>


<section class="popular-recipes-container">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h1 class="sals-h1-title text-center">Popular Mexican Recipes</h1>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row">
			<div class="col-12 p-0">
				<div id="carousel-home" class="owl-carousel owl-theme popular-recipes-carousel">
					<?php 

					$posts = get_field('popular_recipes');

					if( $posts ): ?>
					    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
					        <?php setup_postdata($post); ?>
					        <?php
								$pageId = get_field('page_id');
								$postId = get_the_ID();
								$name = get_the_title();
								$imgUrl = get_the_post_thumbnail_url();
								$currentUrl = get_permalink();
								$allposttags = get_the_tags();
								$i=0;
								if ($allposttags) {
									foreach($allposttags as $tags) {
										$i++;
										if (1 == $i) {
											$firsttag = $tags->name;
										}
									}
								}

								$objPowerReviews[] = [
									'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
									'locale' => 'en_US',
									'merchant_group_id' => '78368',
									'merchant_id' => '278593',
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-slider-'.$pageId
									]
								];
							?>
					        <div class="item">
								<figure class="carousel-image rocket-lazyload" title="<?php the_field('sals-recipe-title'); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>')">
									<img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_field('sals-recipe-title'); ?>">
								</figure>
								<div class="information">
									<div class="left">
										<div class="title">
											<h3><?php the_field('sals-recipe-title'); ?></h3>
											<div class="brand">With <?php the_field('sals-recipe-author'); ?></div><!-- /.brand -->
										</div><!-- /.title -->
										<div id="pr-reviewsnippet-slider-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
										<p class="description d-block d-lg-none"><?php the_field('sals-recipe-description'); ?></p>
									</div><!-- /.left -->
									<div class="right">
										<a href="<?php the_permalink(); ?>" class="view-recipe" title="<?php the_field('sals-recipe-title'); ?>">View recipe</a>
									</div><!-- /.right -->
								</div><!-- /.information -->
								<p class="description d-none d-lg-block"><?php the_field('sals-recipe-description'); ?></p>
							</div>
					    <?php endforeach; ?>
					    <?php wp_reset_postdata(); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>

<?php
	$recipes_groups = new WP_Query(
		array(
			'post_type'=>'recipes_groups',
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	); 
?>
<section class="popular-group-recipes">
	<div class="container">
		<?php $count_group = 1; while ( $recipes_groups->have_posts() ) : $recipes_groups->the_post(); ?>
		<div class="row<?php if ($count_group > 1): ?> mar-top-3r<?php endif ?>">
			<div class="col-12">
				<h2><?php the_field('group_title'); ?></h2>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row justify-content-sm-center justify-content-lg-start">
			<?php

			$post_objects = get_field('group_recipes');

			if( $post_objects ): ?>
				<?php $count = 1; foreach( $post_objects as $post): ?>

				<?php
					$pageId = get_field('page_id');
					$postId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = get_permalink();
					$allposttags = get_the_tags();
					$i=0;
					if ($allposttags) {
						foreach($allposttags as $tags) {
							$i++;
							if (1 == $i) {
								$firsttag = $tags->name;
							}
						}
					}

					$objPowerReviews[] = [
						'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
						'locale' => 'en_US',
						'merchant_group_id' => '78368',
						'merchant_id' => '278593',
						'page_id' => strval($pageId),
						'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-group-'.$count_group.'-'.$pageId
						]
					];
				?>

				<?php setup_postdata($post); ?>
				<?php if ($count <= 5): ?>
				<div class="col-6 col-lg-3 col-md-5 col-sm-5 col-6">
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="d-block">
							<div class="recipe-image img-fluid" title="<?php the_field('sals-recipe-title'); ?>" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
							<div class="recipe-name"><?php the_field('sals-recipe-title'); ?></div><!-- .recipe-name -->
							<div class="brand">With <?php the_field('sals-recipe-author'); ?></div><!-- /.brand -->

							<div id="pr-reviewsnippet-group-<?php echo $count_group; ?>-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
						</a>
					</div><!-- /.item -->
				</div><!-- /.col-12 col-md-3 -->
				<?php endif ?>
				<?php $count++; endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

		</div><!-- /.row -->
		
		<?php $count_group++; endwhile; wp_reset_postdata(); ?>
	</div><!-- /.container -->
</section><!-- /.popular-group-recipes -->


<section class="cont-brands">
	<div class="container">
		<div class="row text-center">
			<div class="col-12">
				<h2>Our favorite brands for your family's meals</h2>

				<?php if( have_rows('brands_group') ): ?>

					<div class="row justify-content-center align-items-center">

					<?php while( have_rows('brands_group') ): the_row(); 

						$brand_name = get_sub_field('brand_name');
						$brand_image = get_sub_field('brand_image');
						$brand_link = get_sub_field('brand_link');

					?>
						
						<div class="col-12 col-md item">
							<a class="d-block" href="<?php echo $brand_link; ?>" target="_blank" title="<?php echo $brand_name; ?>">
								<img src="<?php echo $brand_image; ?>" class="img-fluid" alt="<?php echo $brand_name; ?>" />
							</a>
						</div>

					<?php endwhile; ?>

					</div>

				<?php endif; ?>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</section><!-- /.cont-brands -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<script type="text/javascript" charset="utf-8">
	var currentUrl = window.location.href;
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>

<?php get_footer(); ?>
