<?php 

// Template Name: Coupons

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

 <section class="coupons-page">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-9 py-5 d-flex align-items-center justify-content-center" style="height: 400px;">
                <h2 class="text-center" style="font-size: 40px; line-height: 45px;">There are no coupons available at this time. <br />
                    Please check back soon!
                </h2>
            </div>
        </div>
    </div>
</section> 

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<?php 
 get_footer();
