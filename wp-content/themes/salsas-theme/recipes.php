<?php 
// Template Name: Recipes
get_header(); 

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

?>

<?php
	$recipes_query = new WP_Query(
		array(
			'post_type'=>'post',
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC',
			'orderby'=>'menu_order'
		)
	); 
?>

<div class="container-fluid normal">
	<div class="row">
		<div class="col-12 col-lg-3 col-md-4 left-filters d-none d-md-block">
			<?php get_template_part( 'template-parts/category-filters' ); ?>
		</div><!-- /.col-12 col-lg-3 -->
		<div class="col-12 col-lg-9 col-md-8 col-xl-8 right-recipes">
			<div class="container">
				<div class="d-block d-md-none">
					<div class="row">
						<div class="col-12">
							<a href="javascript:;" class="btn-filter-recipes title-filters ssm-toggle-nav green-color" title="Filter All Recipes"><img src="<?php echo $upload_theme; ?>icons/icon-filter.svg" alt="Filter All recipes" class="img-fluid" /> Filter All Recipes</a>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
				</div><!-- /.d-block d-md-none -->
				<div class="row justify-content-sm-start justify-content-lg-start">
					<script type="text/javascript">
						var data = [];
					</script>
					<?php while ( $recipes_query->have_posts() ): $recipes_query->the_post(); ?>
					<?php
						$pageId = get_field('page_id');
						$postId = get_the_ID();
						$name = get_the_title();
						$currentUrl = get_permalink();
						$allposttags = get_the_tags();
						$postDescription = get_field('sals-recipe-description', $postId);
						$i=0;

						$apiKey = '59fb8bdf-cb43-490b-8b09-975d68551c2a';
						$merchantGroup = '78368';
						$merchantId = '278593';

						$imgUrl = get_field('recipe-salsas-image');

						if ($imgUrl == '') {
							$imgUrl = get_the_post_thumbnail_url();
						}
						
					?>
					<script type="text/javascript">
						data.push({
							locale: 'en_US',
							api_key: '<?php echo $apiKey; ?>',
							merchant_group_id: '<?php echo $merchantGroup; ?>',
							merchant_id: '<?php echo $merchantId; ?>',
							page_id: '<?php echo $pageId; ?>',
							style_sheet: '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
							review_wrapper_url: '/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>',
							components: {
								CategorySnippet: 'pr-reviewsnippet-<?php echo $pageId; ?>'
							}
						});
					</script>
					<div class="col-xl-4 col-lg-6 col-md-6 col-sm-5 col-6 px-md-3 px-2">

						<div id="post-<?php the_ID(); ?>" class="item type-post hentry" data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

							<a href="<?php echo esc_url( get_permalink()); ?>" title="<?php the_field('sals-recipe-title'); ?>" class="d-block">
								<div class="post-thumbnail category-post-image" style="background-image: url(<?php echo $imgUrl; ?>)">
									<?php the_post_thumbnail(); ?>
								</div>
								<div class="entry-header">
									<p class="entry-title"><?php the_field('sals-recipe-title'); ?></p>
									<div class="brand">With <?php the_field('sals-recipe-author'); ?></div>
								</div><!-- .entry-header -->

								<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
							</a>
							<?php if ($pageId != '') : ?>
							<a href="/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="btn btn-link btn-write-review-link">Write a Review</a>
							<?php endif; ?>
						</div><!-- #post-## -->
					</div>
					<?php endwhile; wp_reset_postdata(); ?>
					<script type="text/javascript" charset="utf-8">
						jQuery(document).ready(function(){
							POWERREVIEWS.display.render(data);
						});
					</script>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.col-12 col-lg-9 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<?php get_footer(); ?>