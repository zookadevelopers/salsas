<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); 
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

global $column_results;
?>


<?php 
	if ( have_posts() ) {
		$column_results = 'col-12 col-md-8 col-lg-9 col-xl-8';
	} else {
		$column_results = 'col-12 col-md-12 col-lg-12 col-xl-12';
	}
?>

<div class="container-fluid normal">
	<div class="row">
		<?php if ( have_posts() ) : ?>
			<div class="col-12 col-lg-3 col-md-4 left-filters d-none d-md-block">
				<?php get_template_part( 'template-parts/category-filters' ); ?>
			</div><!-- /.col-12 col-lg-3 -->
		<?php endif; ?>	
			
		<div class="<?php echo $column_results; ?> right-recipes">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cont-search-results">
							<?php if (get_search_query() != '') : ?>
							<h1 class="text-center"><?php printf( esc_html__( '"%s"', 'wp-bootstrap-starter' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
							<?php else : ?>
							<h1 class="text-center">Choose filter options above to view recipes</h1>
							<?php endif ?>
						</div><!-- /.cont-search-results -->
					</div><!-- /.col-12 -->
				</div>

				<?php
		if ( have_posts() ) : ?>
			<div class="d-block d-md-none">
				<div class="row">
					<div class="col-12">
						<a href="javascript:;" class="btn-filter-recipes title-filters ssm-toggle-nav" title="Filter All Recipes"><img src="<?php echo $upload_theme; ?>icons/icon-filter.svg" alt="Filter All recipes" class="img-fluid" /> Filter All Recipes</a>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.d-block d-md-none -->
			<div class="row justify-content-sm-center justify-content-lg-start">
				<?php
					/* Start the Loop */
					$count = 0;
					
					while ( have_posts() ) : the_post();
						$count++;
					endwhile;


					while ( have_posts() ) : the_post();
						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						if ($count < 1) {
							?>
							<div class="col-12">
								<p class="text-center"><?php esc_html_e( 'We can’t seem to find the page you’re looking for. Here are some helpful links instead.', 'wp-bootstrap-starter' ); ?></p><?php get_template_part( 'template-parts/popular-recipes-groups'); ?>
							</div><!-- /.col-12 -->
							<?php
							break;
						} else {
							//get_template_part( 'template-parts/content', get_post_format() );

							if ( 'post' === get_post_type() && !is_single() ) :
								
								$pageId = get_field('page_id');
								$postId = get_the_ID();
								$currentUrl = get_permalink();
								$allposttags = get_the_tags();
								$i=0;
								if ($allposttags) {
									foreach($allposttags as $tags) {
										$i++;
										if (1 == $i) {
											$firsttag = $tags->name;
										}
									}
								}

								$imgUrl = get_field('recipe-salsas-image');

								if ($imgUrl == '') {
									$imgUrl = get_the_post_thumbnail_url();
								}

								$objPowerReviews[] = [
									'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
									'locale' => 'en_US',
									'merchant_group_id' => '78368',
									'merchant_id' => '278593',
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/add-review?post_id='.$postId.'pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-cat-'.$pageId
									]
								];
						?>

								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-5 col-6 px-md-3 px-2">

									<div id="post-<?php the_ID(); ?>" class="item type-post hentry" data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

										<a href="<?php echo esc_url( get_permalink()); ?>" title="<?php the_field('sals-recipe-title'); ?>" class="d-block">
											<div class="post-thumbnail category-post-image" title="<?php the_field('sals-recipe-title'); ?>" style="background-image: url(<?php echo $imgUrl; ?>)">
												<?php the_post_thumbnail(); ?>
											</div>
											<div class="entry-header">
												<p class="entry-title"><?php the_field('sals-recipe-title'); ?></p>
												<div class="brand">With <?php the_field('sals-recipe-author'); ?></div>
											</div><!-- .entry-header -->

											<div id="pr-reviewsnippet-cat-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
										</a>
										<?php if ($pageId != '') : ?>
										<a href="/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="btn btn-link btn-write-review-link">Write a Review</a>
										<?php endif; ?>
									</div><!-- #post-## -->

								</div>

							<?php endif; ?>

							
						<?php }
					endwhile;

					the_posts_navigation();
		else :
			?>
			<p class="text-center"><?php esc_html_e( 'We can’t seem to find the page you’re looking for. Here are some helpful links instead.', 'wp-bootstrap-starter' ); ?></p>
			<?php get_template_part( 'template-parts/popular-recipes-groups'); ?>
			<?php
		endif; ?>
			
			</div><!-- /.row -->
			<script type="text/javascript" charset="utf-8">
				jQuery(document).ready(function(){
					POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
				});
			</script>


			</div>
		</div>
	</div>
</div>


<?php
get_footer();
