<?php 
$upload_dir = wp_upload_dir();
$salsas_domain = get_site_url();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php
	global $recipeBrand;
	global $brandLink;
	$pageId = get_field('page_id');
	$name = get_the_title();
	$imgUrl = get_the_post_thumbnail_url();
	$currentUrl = get_permalink();
	$allposttags = get_the_tags();
	$i=0;
	if ($allposttags) {
		foreach($allposttags as $tags) {
			$i++;
			if (1 == $i) {
				$firstTagSlug = $tags->slug;
				$firstTagName = $tags->name;
			}
		}
	}

	$recipeBrand = $firstTagSlug;

	switch ($recipeBrand) {
		case 'chi-chis':
			$recipeBrand = $upload_theme.'brands/logo-chichis.png';
			$brandLink = $salsas_domain.'/chi-chis';
			$categoryImage = get_field('chichis_logo', 'option');
			break;
		case 'herdez':
			$recipeBrand = $upload_theme.'brands/logo-herdez.png';
			$brandLink = $salsas_domain.'/herdez';
			$categoryImage = get_field('herdez_logo', 'option');
			break;
		case 'la-victoria':
			$recipeBrand = $upload_theme.'brands/logo-victoria.png';
			$brandLink = $salsas_domain.'/la-victoria';
			$categoryImage = get_field('victoria_logo', 'option');
			break;
		case 'dona-maria':
			$recipeBrand = $upload_theme.'brands/logo-maria.png';
			$brandLink = $salsas_domain.'/dona-maria';
			$categoryImage = get_field('maria_logo', 'option');
			break;
		case 'bufalo':
			$recipeBrand = $upload_theme.'brands/logo-bufalo.png';
			$brandLink = $salsas_domain.'/bufalo';
			$categoryImage = get_field('bufalo_logo', 'option');
			break;
		case 'embasa':
			$recipeBrand = $upload_theme.'brands/logo-embasa.png';
			$brandLink = $salsas_domain.'/embasa';
			$categoryImage = get_field('embasa_logo', 'option');
			break;
		case 'del-fuerte':
			$recipeBrand = $upload_theme.'brands/logo-fuerte.png';
			$brandLink = $salsas_domain.'/del-fuerte';
			$categoryImage = get_field('fuerte_logo', 'option');
			break;
		case 'don-miguel':
			$recipeBrand = $upload_theme.'brands/logo-miguel.png';
			$brandLink = $salsas_domain.'/don-miguel';
			$categoryImage = get_field('miguel_logo', 'option');
			break;
	}

	$objPowerReviews[] = [
		'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
		'locale' => 'en_US',
		'merchant_group_id' => '78368',
		'merchant_id' => '278593',
		'page_id' => strval($pageId),
		'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
		'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
		'components' => [
			'CategorySnippet' => 'pr-reviewsnippet-top-'.$pageId
		]
	];

	$imgUrl = get_field('recipe-salsas-image');

	if ($imgUrl == '') {
		$imgUrl = get_the_post_thumbnail_url();
	}

?>

<div id="page-sub-header" class="sub-header-filter header-single-post" style="background-image: url('<?php echo $imgUrl; ?>');">

	<a href="<?php echo home_url('/recipes'); ?>" class="back-recipes d-none"><i class="fa fa-angle-left"></i> Back to all Recipes</a>

	<?php if( get_field('recipe_servings_suggestion') ): ?>
		<span class="serving-suggestion">Serving Suggestion</span>
	<?php endif; ?>

	<?php if (get_field('sals-recipe-video')): ?>
		<div class="cont-recipe-video">
			<?php the_field('sals-recipe-video') ?>
			<div class="button-play" id="play-button" data-toggle="modal" data-target="#recipeVideo"></div><!-- /.button-play -->
		</div><!-- /.cont-recipe-video -->
	<?php endif ?>
</div>

<section id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post-id="<?php the_ID(); ?>" data-brand-name="<?php echo $firstTagSlug; ?>">

	<div class="container">
		<div class="row">
			<div class="col-12">
				<header class="entry-header">
					<h1 class="recipe-title"><?php the_field('sals-recipe-title'); ?></h1>

					<div class="product-reviews">
						<div id="pr-reviewsnippet-top-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
					</div>

					<div class="recipe-reviews d-none">
						<div class="star-rating"></div><!-- /.star-rating -->
						<div class="write-review">
							<a href="#" class="btn btn-link-dark btn-review btn-recipe-gray">Write a review</a>
						</div><!-- /.write-review -->
					</div><!-- /.recipe-reviews -->

					<div class="recipe-description">
						<div class="d-none d-md-block">
							<?php
								$description = get_field('sals-recipe-description');
							?>	
							<p<?php if (strlen($description) < 100) : ?> class="showing"<?php endif; ?>><?php the_field('sals-recipe-description'); ?></p>
							<?php if (strlen($description) > 100) : ?>
							<div class="cont-show-more">
								<a href="#" class="btn btn-link read-more">Read More</a>
							</div>
							<?php endif; ?>
						</div><!-- /.d-none -->

						<div class="d-block d-md-none">
							<p><?php the_field('sals-recipe-description'); ?></p>
							<div class="cont-show-more">
								<a href="#" class="btn btn-link read-more">Read More</a>
							</div>
						</div><!-- /.d-block -->
					</div><!-- /.recipe-description -->

					<div class="recipe-information">
						<?php if( get_field('sals-recipe-servings') ): ?>
						<div class="recipe-servings information-item">
							<label>Servings</label>
							<span><?php the_field('sals-recipe-servings'); ?></span>
						</div><!-- /.recipe-servings -->
						<?php endif; ?>

						<?php if( get_field('sals-recipe-calories-servings') ): ?>
						<div class="recipe-calories information-item">
							<label>Calories</label>
							<span><?php the_field('sals-recipe-calories-servings'); ?></span>
						</div><!-- /.recipe-calories -->
						<?php endif; ?>

						<?php if( get_field('sals-recipe-prep-time') ): ?>
						<div class="recipe-prep-time information-item">
							<label>Prep Time</label>
							<span><?php the_field('sals-recipe-prep-time'); ?></span>
						</div><!-- /.recipe-prep-time -->
						<?php endif; ?>
						
						<?php if( get_field('sals-recipe-cook-time') ): ?>
						<div class="recipe-cook-time information-item">
							<label>Cook Time</label>
							<span><?php the_field('sals-recipe-cook-time'); ?></span>
						</div><!-- /.recipe-cook-time -->
						<?php endif; ?>

						<?php if( get_field('sals-recipe-total-time') ): ?>
						<div class="recipe-total-time information-item">
							<label>Total Time</label>
							<span><?php the_field('sals-recipe-total-time'); ?></span>
						</div><!-- /.recipe-total-time -->
						<?php endif; ?>

					</div><!-- /.recipe-information -->

				</header><!-- .entry-header -->
			</div><!-- /.col-12 -->
		</div><!-- /.row -->

		<div class="cont-columns">
			<div class="row">
				<div class="col-lg-4 col-12">
					<div class="column col-left col-ingredients">
						<h4>Ingredients</h4>
						<?php the_content(); ?>
					</div><!-- /.col-left col-ingredients -->
				</div><!-- /.col-lg-4 col-12 -->
				<div class="col-lg-8 col-12">
					<div class="column col-right col-directions">
						<h4>Directions</h4>
						<?php the_field('sals-recipe-directions'); ?>
					</div><!-- /.col-right col-directions -->
				</div><!-- /.col-lg-8 col-12 -->
			</div><!-- /.row -->
		</div><!-- /.cont-columns -->

		<div class="recipe-share">
			<div class="row justify-content-between align-items-center">
				<div class="col-12 col-md-12 col-xl-3 mt-4 mb-5 mb-lg-3 my-lg-0 text-center text-xl-left">
					<?php echo do_shortcode('[ssba-buttons]'); ?>
				</div><!-- /.col-12 col-xl-4 -->
				<div class="col-12 col-md-12 col-lg-12 col-xl-8 d-lg-flex justify-content-lg-center d-xl-block">
					<div class="row justify-content-between align-items-center text-center text-xl-left row-ie">
						<div class="col-12 col-md-12 col-xl-5 text-xl-right mb-lg-3">
							<a href="<?php echo $salsas_domain; ?>/recipes" class="btn btn-main w-100">View Other <br />Mexican Recipes</a>
						</div><!-- /.col-7 col-xl-3 -->
						<div class="col-12 col-md-12 col-xl-3 p-lg-0 mb-lg-3">
							<p class="date">Recipe by <span class="text-uppercase"><?php echo $firstTagName; ?></span><sup>&reg;</sup> brand<br /><?php the_field('sals-recipe-date'); ?></p>
						</div><!-- /.col-12 -->
						<?php if (get_field('sals-recipe-brand-logo')): ?>
							<div class="col-12 col-md-12 col-xl-3 ">
								<a href="<?php echo $brandLink; ?>" class="d-block" title="<?php echo $firstTagName; ?>">
									<div class="brand-logo" style="background-image: url('<?php echo $categoryImage; ?>')"></div><!-- /.brand-logo -->
								</a>
							</div><!-- /.col-12 col-lg-4 -->
						<?php endif ?>
					</div><!-- /.row -->
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
		</div><!-- /.recipe-share -->
	</div><!-- /.container -->
		
	<?php if (get_field('sals-recipe-video')): ?>
	<!-- Modal -->
	<div class="modal fade recipe-video" id="recipeVideo" tabindex="-1" role="dialog" aria-labelledby="recipeVideoTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="cont-popup-recipe-video">
						<?php the_field('sals-recipe-video') ?>
					</div><!-- /.cont-recipe-video -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<?php endif ?>

</section><!-- #post-## -->

<div class="main-cont-reviews">
	<div class="container">
		<div class="cont-reviews">
			<div class="row">
				<div class="col-12">
					<h2>Reviews</h2>
				</div><!-- /.col-12 -->
			</div><!-- /.row -->
			
			<div class="row columns">
				<div class="col-md-5 col-12">
					<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet"></div>
					<div id="pr-reviewhistogram" class="p-w-r">
						<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
					</div>
					<div class="cont-write-review"></div><!-- /.cont-write-review -->

				</div><!-- /.col-5 -->
				<div class="col-md-7 col-12">
					<div id="pr-reviewdisplay-<?php the_field('page_id'); ?>" class="cont-review-display"></div>
				</div><!-- /.col-7 -->
			</div><!-- /.row -->

		</div><!-- /.cont-reviews -->
	</div><!-- /.container -->
</div><!-- /.container-fluid -->

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>

<script type="text/javascript" charset="utf-8">
	POWERREVIEWS.display.render(
		[
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: '/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
				components: {
					CategorySnippet: 'pr-reviewsnippet-top-<?php the_field('page_id'); ?>',
				}
			},
			{
				api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
				locale: 'en_US',
				merchant_group_id: '78368',
				merchant_id: '278593',
				page_id: '<?php the_field('page_id'); ?>',
				review_wrapper_url: '/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
				style_sheet: '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
				on_render: function(config, data) {
					jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
					jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn btn-write-review');
				},
				components: {
					ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
					ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>'
				}
			}
		]
	);
</script>
<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
<?php 
 get_footer();

