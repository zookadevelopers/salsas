<?php 
// Template Name: Products

// Products Home
$products_home = new WP_Query(
	array(
		'post_type'=>'products', 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC'
	)
); 

$posInfo = get_post();
// var_dump($posInfo);
$posName = get_field('post_type_slug');
$posName = str_replace(" ","_",$posName);

// Products Query
$products = new WP_Query(
	array(
		'post_type'=>$posName, 
		'post_status'=>'publish', 
		'posts_per_page'=> -1,
		'order' => 'ASC',
        'orderby'=>'menu_order'
	)
);

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?> 

<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php $postId =  get_the_ID(); ?>	
<section class="banner-product container-fluid">
</section>

<section class="text-under-banner text-product py-4">
	<div class="container">
		<div class="col-12">
			<h1><?php the_field('title_category'); ?></h1>
			<?php the_content() ?>
		</div>
	</div>
</section>

<section class="products-home product-category">
	<div class="container">
		<div class="row items-products">
			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				<?php
					$pageId = get_field('page_id');
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$objPowerReviews[] = [
						'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
						'locale' => 'en_US',
						'merchant_group_id' => '78368',
						'merchant_id' => '278593',
						'page_id' => strval($pageId),
						'style_sheet' => '/wp-content/themes/herdez-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/herdez/add-review?pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
						]
					];
				?>
				<div class="col-12 col-md-4 col-lg-3 item">
					<a class="link-product" href="<?php the_permalink(); ?>">
						<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="" />
						<p class=""><?php the_field('product-name'); ?></p>
					</a>
					<div class="product-reviews">
						<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	</div>
</section>

<section class="featured-recipes-home">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3>Explore our other products</h3>
			</div>
		</div>
		<div class="row items-products">
			<div class="all-products-carousel owl-carousel owl-theme">
				<?php while ( $products_home->have_posts() ) : $products_home->the_post(); ?>
					<?php if ($postId != get_the_ID() ): ?>
						<div class="text-center item">
							<a class="link-product" href="<?php the_permalink(); ?>">
								<img class="img-product" src="<?php the_post_thumbnail_url(); ?>" alt="" />
								<p class=""><?php the_title(); ?></p>
							</a>
						</div>
					<?php endif ?>	
				<?php endwhile; ?>
			</div>
		</div>
	</div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

<?php endwhile; else : ?>
    <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>

<script type="text/javascript" charset="utf-8">
	var currentUrl = window.location.href;
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>
<?php 
 get_footer();
