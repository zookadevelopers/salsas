$(document).ready(function () {
	$('.ssm-toggle-nav').on('click', function(){
		var $this = $(this);
		var buttonTitle = $this.attr('title');
		console.log(buttonTitle);
		if ($this.hasClass('title-filters')) {
			$('.cont-mobile-menu .visible-menu').hide();
			$('.cont-mobile-menu .visible-filters').show();

			if (buttonTitle != "" || buttonTitle != undefined) {
				$('.left-filters h5').text(buttonTitle);
			}

		} else {
			$('.cont-mobile-menu .visible-menu').show();
			$('.cont-mobile-menu .visible-filters').hide();
		}
	});
});