<?php 
	$upload_dir = wp_upload_dir();
	$upload_theme = get_template_directory_uri().'/inc/assets/img/';
?>
<div class="nav-fixed">
	<div class="title d-flex align-items-center">
		<?php if (is_page_template('recipes.php')): ?><img src="<?php echo $upload_theme; ?>icons/icon-filter.svg" class="img-fluid d-block d-md-none" />&nbsp;<?php endif; ?><h5>Filter All Recipes</h5>
	</div><!-- /.title -->
	<div class="cont-options">
		<?php $search_filter_id = get_field( 'search_and_filter_id', 'option' ); ?>
		<?php echo do_shortcode( '[searchandfilter id="'.$search_filter_id.'"]' ); ?>
	</div><!-- /.radio-list -->
</div><!-- /.nav-fixed -->