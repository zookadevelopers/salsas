<?php
	$recipes_groups = new WP_Query(
		array(
			'post_type'=>'recipes_groups',
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	); 
?>

<section class="popular-group-recipes">
	<div class="container">
		<?php $count_group = 1; while ( $recipes_groups->have_posts() ) : $recipes_groups->the_post(); ?>
		<?php if ($count_group <= 1): ?> 
		<div class="row">
			<div class="col-12">
				<h2><?php the_field('group_title'); ?></h2>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row">
			<?php

			$post_objects = get_field('group_recipes');

			if( $post_objects ): ?>
				<?php $count = 1; foreach( $post_objects as $post): ?>

				<?php
					$pageId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = get_permalink();
					$allposttags = get_the_tags();
					$i=0;
					if ($allposttags) {
						foreach($allposttags as $tags) {
							$i++;
							if (1 == $i) {
								$firsttag = $tags->name;
							}
						}
					}
				?>

				<?php setup_postdata($post); ?>
				<?php if ($count <= 5): ?>
				<?php
					$pageId = get_field('page_id');
					$postId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$objPowerReviews[] = [
						'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
						'locale' => 'en_US',
						'merchant_group_id' => '78368',
						'merchant_id' => '278593',
						'page_id' => strval($pageId),
						'style_sheet' => '/wp-content/themes/salsas-theme/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?post_id='.$postId.'&pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-group-'.$count_group.'-'.$pageId
						]
					];
				?>
				<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 col-6 mb-sm-4 mb-lg-4">
					<div class="item">
						<a href="<?php the_permalink(); ?>" title="<?php the_field('sals-recipe-title'); ?>" class="d-block">
							<div class="recipe-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
							<div class="recipe-name"><?php the_field('sals-recipe-title'); ?></div><!-- .recipe-name -->
							<div class="brand">With <?php the_field('sals-recipe-author'); ?></div><!-- /.brand -->

							<div id="pr-reviewsnippet-group-<?php echo $count_group; ?>-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
						</a>
						<?php if ($pageId != '') : ?>
						<a href="/add-review?post_id=<?php echo $postId; ?>&pr_page_id=<?php echo $pageId; ?>&pr_merchant_id=<?php echo $merchantId; ?>&pr_api_key=<?php echo $apiKey; ?>&pr_merchant_group_id=<?php echo $merchantGroup; ?>" title="Write a Review" class="btn btn-link btn-write-review-link">Write a Review</a>
						<?php endif; ?>
					</div><!-- /.item -->
				</div><!-- /.col-12 col-md-3 -->
				<?php endif ?>
				<?php $count++; endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

		</div><!-- /.row -->
		<?php endif ?>
		<?php $count_group++; endwhile; wp_reset_postdata(); ?>
	</div><!-- /.container -->
</section><!-- /.popular-group-recipes -->

<script type="text/javascript" charset="utf-8">
	jQuery(document).ready(function(){
		POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
	});
</script>
