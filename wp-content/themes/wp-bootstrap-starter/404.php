<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-12 col-sm-12 col-lg-12 page-404-main-section">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header text-center">
					<h1 class="page-title"><?php esc_html_e( 'Oops!', 'wp-bootstrap-starter' ); ?><small>Error Code: 404</small></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p class="text-center"><?php esc_html_e( 'We can’t seem to find the page you’re looking for. Here are some helpful links instead.', 'wp-bootstrap-starter' ); ?></p>

					<div class="page-menu">
						<?php echo do_shortcode( '[do_widget id=nav_menu-5]' ); ?>
					</div><!-- /.page-menu -->

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
