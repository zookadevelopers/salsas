<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<style>	
.sub-header-filter.overlay::before {
	background-color: rgba(0, 0, 0, .<?php the_field('category_page_opacity', 'option') ?>) !important;	
}
</style>

 
	<section id="primary" class="content-area col-12 col-sm-12 col-lg-12 p-0">
		
			
			<div class="container-fluid normal">
				<div class="row">
					<div class="col-12 col-lg-3 left-filters">
						<?php get_template_part( 'template-parts/category-filters' ); ?>
					</div><!-- /.col-12 col-lg-3 -->
				

					<div class="col-12 col-lg-8 right-recipes">
						<?php // the search/filter plugin uses this section "main" to load items async ?>
						<main id="main" class="site-main" role="main">
						<div class="container">

							<?php

							if ( have_posts() ) : ?>

								<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
								<div class="row">
									<?php
										while ( have_posts() ) : the_post();
										
										if ( 'post' === get_post_type() && !is_single() ) :
											
										$pageId = get_the_ID();
										$name = get_the_title();
										$imgUrl = get_the_post_thumbnail_url();
										$currentUrl = get_permalink();
										$allposttags = get_the_tags();
										$i=0;
										if ($allposttags) {
											foreach($allposttags as $tags) {
												$i++;
												if (1 == $i) {
													$firsttag = $tags->name;
												}
											}
										}

										$objPowerReviews[] = [
											'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
											'locale' => 'en_US',
											'merchant_group_id' => '78368',
											'merchant_id' => '278593',
											'page_id' => strval($pageId),
											'style_sheet' => '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
											'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
											'components' => [
												'CategorySnippet' => 'pr-reviewsnippet-cat-'.$pageId
											]
										];
									?>

									<div class="col-lg-4 col-md-6 col-sm-12 col-12">
										<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

											<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
												<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
													<?php the_post_thumbnail(); ?>
												</div>
												<div class="entry-header">
													<?php
													if ( is_single() ) :
														the_title( '<h1 class="entry-title">', '</h1>' );
													else :
														the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
													endif;

													?>
												</div><!-- .entry-header -->
												<div id="pr-reviewsnippet-cat-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
											</a>
										</article><!-- #post-## -->

									</div>

									<?php endif; ?>

									<?php if (is_single()) : ?>
									<?php 
										$allposttags = get_the_tags();
										$i=0;
										if ($allposttags) {
											foreach($allposttags as $tags) {
												$i++;
												if (1 == $i) {
													$firsttag = $tags->name;
												}
											}
										}
									?>
									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post-id="<?php the_ID(); ?>" data-brand-name="<?php echo $firsttag; ?>">
										<div class="container">
											<div class="row">
												<div class="col-12">
													<header class="entry-header">
														<h1 class="recipe-title"><?php the_field('sals-recipe-title'); ?></h1>

														<div class="product-reviews">
															<div id="pr-reviewsnippet-top-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
														</div>
														
														<div class="recipe-reviews d-none">
															<div class="star-rating"></div><!-- /.star-rating -->
															<div class="write-review">
																<a href="#" class="btn btn-link-dark btn-review btn-recipe-gray">Write a Review</a>
															</div><!-- /.write-review -->
														</div><!-- /.recipe-reviews -->

														<div class="recipe-description">
															<div class="d-none d-md-block">
																<?php
																	$description = get_field('sals-recipe-description');
																?>	
																<p<?php if (strlen($description) < 100) : ?> class="showing"<?php endif; ?>><?php the_field('sals-recipe-description'); ?></p>
																<?php if (strlen($description) > 100) : ?>
																<div class="cont-show-more">
																	<a href="#" class="btn btn-link read-more">Read More</a>
																</div>
																<?php endif; ?>
															</div><!-- /.d-none -->

															<div class="d-block d-md-none">
																<p><?php the_field('sals-recipe-description'); ?></p>
																<div class="cont-show-more">
																	<a href="#" class="btn btn-link read-more">Read More</a>
																</div>
															</div><!-- /.d-block -->
														</div><!-- /.recipe-description -->

														<div class="recipe-information">
															<?php if( get_field('sals-recipe-servings') ): ?>
															<div class="recipe-servings information-item">
																<label>Servings</label>
																<span><?php the_field('sals-recipe-servings'); ?></span>
															</div><!-- /.recipe-servings -->
															<?php endif; ?>

															<?php if( get_field('sals-recipe-calories-servings') ): ?>
															<div class="recipe-calories information-item">
																<label>Calories</label>
																<span><?php the_field('sals-recipe-calories-servings'); ?></span>
															</div><!-- /.recipe-calories -->
															<?php endif; ?>

															<?php if( get_field('sals-recipe-prep-time') ): ?>
															<div class="recipe-prep-time information-item">
																<label>Prep Time</label>
																<span><?php the_field('sals-recipe-prep-time'); ?></span>
															</div><!-- /.recipe-prep-time -->
															<?php endif; ?>
															
															<?php if( get_field('sals-recipe-cook-time') ): ?>
															<div class="recipe-cook-time information-item">
																<label>Cook Time</label>
																<span><?php the_field('sals-recipe-cook-time'); ?></span>
															</div><!-- /.recipe-cook-time -->
															<?php endif; ?>

															<?php if( get_field('sals-recipe-total-time') ): ?>
															<div class="recipe-total-time information-item">
																<label>Total Time</label>
																<span><?php the_field('sals-recipe-total-time'); ?></span>
															</div><!-- /.recipe-total-time -->
															<?php endif; ?>

														</div><!-- /.recipe-information -->

													</header><!-- .entry-header -->
												</div><!-- /.col-12 -->
											</div><!-- /.row -->

											<div class="cont-columns">
												<div class="row">
													<div class="col-lg-4 col-12">
														<div class="column col-left col-ingredients">
															<h4>Ingredients</h4>
															<?php the_content(); ?>
														</div><!-- /.col-left col-ingredients -->
													</div><!-- /.col-lg-4 col-12 -->
													<div class="col-lg-8 col-12">
														<div class="column col-right col-directions">
															<h4>Directions</h4>
															<?php the_field('sals-recipe-directions'); ?>
														</div><!-- /.col-right col-directions -->
													</div><!-- /.col-lg-8 col-12 -->
												</div><!-- /.row -->
											</div><!-- /.cont-columns -->

											<div class="recipe-share">
												<div class="row">
													<div class="col-12">
														<?php echo do_shortcode('[ssba-buttons]'); ?>
													</div><!-- /.col-12 -->
												</div><!-- /.row -->
											</div><!-- /.recipe-share -->
										</div><!-- /.container -->
										
										<?php if (get_field('sals-recipe-video')): ?>
											<!-- Modal -->
											<div class="modal fade recipe-video" id="recipeVideo" tabindex="-1" role="dialog" aria-labelledby="recipeVideoTitle" aria-hidden="true">
												<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="cont-popup-recipe-video">
																<?php the_field('sals-recipe-video') ?>
															</div><!-- /.cont-recipe-video -->
													
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-modal-close" data-dismiss="modal">Close</button>
														</div>
													</div>
												</div>
											</div>
										<?php endif ?>

									</article><!-- #post-## -->
											
									<?php endif; ?>

									<?php if ( is_page_template('search.php')): ?>

									<?php
										$pageId = get_the_ID();
										$name = get_the_title();
										$imgUrl = get_the_post_thumbnail_url();
										$currentUrl = get_permalink();
										$allposttags = get_the_tags();
										$i=0;
										if ($allposttags) {
											foreach($allposttags as $tags) {
												$i++;
												if (1 == $i) {
													$firsttag = $tags->name;
												}
											}
										}

										$objPowerReviews[] = [
											'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
											'locale' => 'en_US',
											'merchant_group_id' => '78368',
											'merchant_id' => '278593',
											'page_id' => strval($pageId),
											'style_sheet' => '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
											'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
											'components' => [
												'CategorySnippet' => 'pr-reviewsnippet-search-'.$pageId
											]
										];
									?>

									<div class="col-lg-3 col-12">

										<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

											<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
												<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
													<?php the_post_thumbnail(); ?>
												</div>
												<header class="entry-header">
													<?php
													if ( is_single() ) :
														the_title( '<h1 class="entry-title">', '</h1>' );
													else :
														the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
													endif;

													?>
												</header><!-- .entry-header -->

												<footer class="entry-footer">
													<?php wp_bootstrap_starter_entry_footer(); ?>
												</footer><!-- .entry-footer -->

												<div id="pr-reviewsnippet-search-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
											</a>
										</article><!-- #post-## -->

									</div>
									<script type="text/javascript" charset="utf-8">
										jQuery(window).load(function(){
											var currentUrl = window.location.href;
											jQuery(document).ready(function(){
												POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
											});
										});
									</script>
									<?php endif; ?>
							
									<?php endwhile;

									the_posts_navigation();

									else :

										get_template_part( 'template-parts/content', 'none' );

									endif; ?>
								</div><!-- /.row -->
								
								<script type="text/javascript" charset="utf-8">
									jQuery(window).load(function(){
										var currentUrl = window.location.href;
										jQuery(document).ready(function(){
											POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
										});
									});
								</script>
						</div>


						</main><!-- #main -->
					</div>





				</div>
			</div>
		
	</section><!-- #primary -->

<?php
get_footer();
