<?php 
// Template Name: Brands
get_header(); ?>

<section class="cont-all-brands">
	<div class="container">
		<div class="row text-center">
			<div class="col-12">
				<h1><?php the_field('brands_title'); ?></h1>
				<p><?php the_field('brands_description'); ?></p>
			</div>
		</div>
		<?php if( have_rows('brands_group') ): ?>

		<div class="row justify-content-center align-items-center">

			<?php while( have_rows('brands_group') ): the_row(); 

				$brand_name = get_sub_field('brand_name');
				$brand_image = get_sub_field('brand_image');
				$brand_link = get_sub_field('brand_link');

			?>
				
			<div class="col-6 col-md-3 item">
				<a class="d-block" href="<?php echo $brand_link; ?>" target="_blank">
					<img src="<?php echo $brand_image; ?>" class="img-fluid" alt="<?php echo $brand_name; ?>" />
				</a>
			</div>

			<?php endwhile; ?>

		</div>

		<?php endif; ?>
	</div><!-- /.container -->
</section><!-- /.cont-brands -->

<?php
	$recipes_groups = new WP_Query(
		array(
			'post_type'=>'recipes_groups',
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	); 
?>

<section class="popular-group-recipes">
	<div class="container">
		<?php $count_group = 1; while ( $recipes_groups->have_posts() ) : $recipes_groups->the_post(); ?>
		<?php if ($count_group <= 1): ?> 
		<div class="row">
			<div class="col-12">
				<h2><?php the_field('group_title'); ?></h2>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row">
			<?php

			$post_objects = get_field('group_recipes');

			if( $post_objects ): ?>
				<?php $count = 1; foreach( $post_objects as $post): ?>

				<?php
					$pageId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = get_permalink();
					$allposttags = get_the_tags();
					$i=0;
					if ($allposttags) {
						foreach($allposttags as $tags) {
							$i++;
							if (1 == $i) {
								$firsttag = $tags->name;
							}
						}
					}
				?>

				<?php setup_postdata($post); ?>
				<?php if ($count <= 5): ?>
				<div class="col-lg-3 col-md-6 col-sm-12 col-12 mb-sm-4 mb-lg-0">
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="d-block">
							<div class="recipe-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
							<div class="recipe-name"><?php the_field('sals-recipe-title'); ?></div><!-- .recipe-name -->
							<div class="brand">With <?php the_field('sals-recipe-author'); ?></div><!-- /.brand -->

							<div id="pr-reviewsnippet-group-<?php echo $count_group; ?>-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
						</a>
					</div><!-- /.item -->
				</div><!-- /.col-12 col-md-3 -->
				<?php endif ?>
				<?php $count++; endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

		</div><!-- /.row -->
		<?php endif ?>
		<?php $count_group++; endwhile; wp_reset_postdata(); ?>
	</div><!-- /.container -->
</section><!-- /.popular-group-recipes -->


<?php get_footer(); ?>
