<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
				</div><!-- /.col-12 -->
			</div><!-- .row -->
		</div><!-- .container -->
	</div><!-- #content -->
	
	<?php if( is_404()) : ?>
		<?php
			$recipes_groups = new WP_Query(
				array(
					'post_type'=>'recipes_groups',
					'post_status'=>'publish', 
					'posts_per_page'=> -1,
					'order' => 'ASC'
				)
			); 
		?>

		<section class="popular-group-recipes">
			<div class="container">
				<?php $count_group = 1; while ( $recipes_groups->have_posts() ) : $recipes_groups->the_post(); ?>
				<?php if ($count_group <= 1): ?> 
				<div class="row">
					<div class="col-12">
						<h2><?php the_field('group_title'); ?></h2>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
				<div class="row">
					<?php

					$post_objects = get_field('group_recipes');

					if( $post_objects ): ?>
						<?php $count = 1; foreach( $post_objects as $post): ?>

						<?php
							$pageId = get_field('page_id');
							$name = get_the_title();
							$imgUrl = get_the_post_thumbnail_url();
							$currentUrl = get_permalink();
							$allposttags = get_the_tags();
							$postDescription = get_field('sals-recipe-description', $postId);
							$i=0;
							if ($allposttags) {
								foreach($allposttags as $tags) {
									$i++;
									if (1 == $i) {
										$firsttag = $tags->name;
									}
								}
							}
						?>

						<?php setup_postdata($post); ?>
						<?php if ($count <= 5): ?>
						<div class="col-lg-3 col-md-6 col-sm-12 col-12">
							<div class="item">
								<a href="<?php the_permalink(); ?>" class="d-block">
									<div class="recipe-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
									<div class="recipe-name"><?php the_field('sals-recipe-title'); ?></div><!-- .recipe-name -->
									<div class="brand">With <?php the_field('sals-recipe-author'); ?></div><!-- /.brand -->

									<div id="pr-reviewsnippet-group-<?php echo $count_group; ?>-<?php the_field('page_id'); ?>" class="stars-snippet category-page"></div>
								</a>
							</div><!-- /.item -->
						</div><!-- /.col-12 col-md-3 -->
						<?php endif ?>
						<?php $count++; endforeach; ?>
						<?php wp_reset_postdata(); ?>
					<?php endif; ?>

				</div><!-- /.row -->
				<?php endif ?>
				<?php $count_group++; endwhile; wp_reset_postdata(); ?>
			</div><!-- /.container -->
		</section><!-- /.popular-group-recipes -->
	<?php endif; ?>

	<?php if( is_single()) : ?>
		<?php $postUrl = get_post_permalink( get_the_ID() ); ?>
	<script type="text/javascript" charset="utf-8">
		var currentUrl = window.location.href;

		console.log('<?php echo $postUrl; ?>');
 
		POWERREVIEWS.display.render({
			api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
			locale: 'en_US',
			merchant_group_id: '78368',
			merchant_id: '278593',
			page_id: '<?php the_field('page_id'); ?>',
			review_wrapper_url: '/add-review?post_id=<?php the_ID(); ?>&pr_page_id=<?php the_field('page_id'); ?>',
			style_sheet: '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
			components: {
				ReviewSnippet: 'pr-reviewsnippet-<?php the_field('page_id'); ?>',
				ReviewDisplay: 'pr-reviewdisplay-<?php the_field('page_id'); ?>',
			},
			on_render: function () {
				jQuery('.pr-review-snapshot-block-histogram').appendTo('#pr-reviewhistogram .pr-review-snapshot');
				jQuery('.pr-snippet-write-review-link').appendTo('.cont-write-review').addClass('btn-write-review');
			}
		});
	</script>

		<div class="main-cont-reviews">
			<div class="container">
				<div class="cont-reviews">
					<div class="row">
						<div class="col-12">
							<h2>Reviews</h2>
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
					
					<div class="row columns">
						<div class="col-md-5 col-12">
							<div id="pr-reviewsnippet-<?php the_field('page_id'); ?>" class="stars-snippet"></div>
							<div id="pr-reviewhistogram" class="p-w-r">
								<div class="pr-review-snapshot"></div><!-- /.pr-review-snapshot -->
							</div>
							<div class="cont-write-review"></div><!-- /.cont-write-review -->

						</div><!-- /.col-5 -->
						<div class="col-md-7 col-12">
							<div id="pr-reviewdisplay-<?php the_field('page_id'); ?>" class="cont-review-display"></div>
						</div><!-- /.col-7 -->
					</div><!-- /.row -->

				</div><!-- /.cont-reviews -->
			</div><!-- /.container -->
		</div><!-- /.container-fluid -->
	<?php endif; ?>

    <?php get_template_part( 'footer-widget' ); ?>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container pt-3 pb-3">
            <div class="site-info d-flex align-items-center">
            	<?php dynamic_sidebar( 'footer-copyright' ); ?>
            	<span class="sep"> | </span>
            	Copyright © <?php echo date('Y'); ?> &copy;&nbsp; <?php echo '<a href="'. site_url() .'" target="_blank">salsas.com</a>'; ?>
            	<!-- change this code for current year date -> <?php echo date('Y'); ?> -->
            </div><!-- close .site-info -->
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div>
</div><!-- #page -->
<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
<script src="http://labs.rampinteractive.co.uk/touchSwipe/jquery.touchSwipe.min.js"></script>
<?php wp_footer(); ?>

</body>
</html>