<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

$upload_dir = wp_upload_dir();
$upload_theme = get_template_directory_uri().'/inc/assets/img/';

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php if (is_single()) : ?>
		
	<?php 
		$post = get_post( $post_id );
		$slug = $post->post_name;
		$allposttags = get_the_tags();
        $categories = get_the_category();
        $authorName = get_the_author_meta( 'display_name', $post->post_author);
        $directions = get_field('sals-recipe-directions');

        $prepTime = strtolower(str_replace('', ',', get_field('sals-recipe-prep-time')));
        preg_match_all('!\d+!', $prepTime, $matchesPrepTime);

        if (isset($matchesPrepTime[0][1])) {
            $prepTime = 'PT'.$matchesPrepTime[0][0].'H'.$matchesPrepTime[0][1].'M';
        } else {
            $time = (strpos($prepTime, 'Hours')) ? 'H' : 'M';
            $prepTime = 'PT'.$matchesPrepTime[0][0].$time;
        }

        $cookTime = strtolower(str_replace('', ',', get_field('sals-recipe-cook-time')['sals-cook-time-label']));
        preg_match_all('!\d+!', $cookTime, $matchesCookTime);

        if (isset($matchesCookTime[0][0])) {
            $cookTime = 'PT'.get_field('sals-recipe-cook-time')['sals-cook-time-time'].'H'.$matchesCookTime[0][0].'M';
        } else {
            $time = (strpos($cookTime, 'hours')) ? 'H' : 'M';
            $cookTime = 'PT'.get_field('sals-recipe-cook-time')['sals-cook-time-time'].$time;
        }

        $totalTime = strtolower(str_replace('', ',', get_field('sals-recipe-total-time')));
        preg_match_all('!\d+!', $totalTime, $matchesTotalTime);

        if (isset($matchesTotalTime[0][1])) {
            $totalTime = 'PT'.$matchesTotalTime[0][0].'H'.$matchesTotalTime[0][1].'M';
        } else {
            $time = (strpos($totalTime, 'hours')) ? 'H' : 'M';
            $totalTime = 'PT'.$matchesTotalTime[0][0].$time;
        }

        $videoUrl = get_field("sals-recipe-video");

        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $videoUrl, $match);

        if(!empty($match[0])) {
            $videoUrl = $match[0][0];
        }

		$i=0;
		if ($allposttags) {
			foreach($allposttags as $tags) {
				$i++;
				if (1 == $i) {
					$firsttag = $tags->name;
				}
			}
		}
	?>
	<meta name="description" content="<?php the_field('sals-recipe-description'); ?>"/>
	<link rel="canonical" href="<?php echo get_permalink(get_the_ID(), false); ?>" />
	<meta property="og:description" content="<?php the_field('sals-recipe-description'); ?>" />
	<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
	<meta property="og:locale" content="en_US" />
	<meta property="og:site_name" content="Salsas" />
	<meta property="og:title" content="<?php the_field('sals-recipe-title'); ?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php echo get_permalink(get_the_ID(), false); ?>" />
	<meta property="article:published_time" content="<?php the_time('c'); ?>" />
	<meta property="article:section" content="<?php echo $firsttag; ?>" />
	<meta property="article:tag" content="<?php echo $firsttag; ?>" />	
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:description" content="<?php the_field('sals-recipe-description'); ?>" />
	<meta name="twitter:image" content="<?php the_post_thumbnail_url(); ?>">
	<meta name="twitter:title" content="<?php the_field('sals-recipe-title'); ?>" />

    <!-- Object to Google Assistant -->
    <script type="application/ld+json">
        {
            "@context": "http://schema.org/",
            "@type": "Recipe",
            "name": "<?php the_field('sals-recipe-title'); ?>",
            "image": [
                "<?php the_post_thumbnail_url(); ?>"
            ],
            "author": {
                "@type": "Person",
                "name": "<?php echo $authorName; ?>"
            },
            "datePublished": "<?php echo $post->post_modified; ?>",
            "description": "<?php the_field('sals-recipe-description'); ?>",
            "prepTime": "<?php echo $prepTime; ?>",
            "cookTime": "<?php echo $cookTime; ?>",
            "totalTime": "<?php echo $totalTime; ?>",
            "keywords": "<?php the_field('sals-recipe-title'); ?>",
            "recipeYield": "<?php the_field('sals-recipe-servings'); ?>",
            "recipeCategory": "<?php echo esc_html( $categories[0]->name ); ?>",
            "recipeCuisine": "American",
            "nutrition": {
                "@type": "NutritionInformation",
                "calories": "270 calories"
            },
            "recipeIngredient": [
                "<?php echo strip_tags($post->post_content); ?>"
            ],
            "recipeInstructions": [
                {
                    "@type": "HowToStep",
                    "text": "<?php echo strip_tags($directions); ?>"
                }
            ],
            "review": {
                "@type": "Review",
                "reviewRating": {
                    "@type": "Rating",
                    "ratingValue": "4",
                    "bestRating": "5"
                },
                "author": {
                    "@type": "Person",
                    "name": "Julia Benson"
                },
                "datePublished": "2018-05-01",
                "reviewBody": "This cake is delicious!",
                "publisher": "The cake makery"
            },
            "aggregateRating": {
                "@type": "AggregateRating",
                "ratingValue": "5",
                "ratingCount": "18"
            },
            "video": {
                "name": "<?php the_field('sals-recipe-title'); ?>",
                "description": "<?php the_field('sals-recipe-description'); ?>",
                "thumbnailUrl": [
                    "<?php the_post_thumbnail_url(); ?>"
                ],
                "contentUrl": "<?php echo $videoUrl; ?>",
                "embedUrl": "<?php echo $videoUrl; ?>",
                "uploadDate": "<?php echo $post->post_modified; ?>"
            }
        }
    </script>

	<?php endif;?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php get_template_part( 'template-parts/mobile-menu' );?>
<div class="loader d-none"><img src="<?php echo $upload_theme ?>logo-salsas.png" alt="Salsas.com" /></div>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
	<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
		<header id="masthead" class="site-header navbar-static-top <?php echo wp_bootstrap_starter_bg_class(); ?> main-header" role="banner">
			<div class="container">
				<nav class="navbar navbar-expand-md row">
					<div class="col-2 d-block d-md-none">
						<button class="ssm-toggle-nav hamburger-menu"><i class="fas fa-bars fa-3x"></i></button>
					</div><!-- /.col-12 col-2 -->
					<div class="col-10 col-md-4 col-lg-4">
						<div class="navbar-brand w-100">
							<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
								<a href="<?php echo esc_url( home_url( '/' )); ?>">
									<img src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
								</a>
							<?php else : ?>
								<a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
							<?php endif; ?>
						</div>
					</div><!-- /.col-12 col-lg-4 -->
					<div class="col-12 col-md-8 d-flex align-items-center">
						<?php
							if (is_front_page()) { 
								$notHome = ' d-flex justify-content-end';
							}

							wp_nav_menu(array(
								'theme_location'    => 'primary',
								'container'       => 'div',
								'container_id'    => 'main-nav',
								'container_class' => 'collapse navbar-collapse justify-content-center header-nav',
								'menu_id'         => false,
								'menu_class'      => 'navbar-nav'.$notHome,
								'depth'           => 3,
								'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
								'walker'          => new wp_bootstrap_navwalker()
							));
						?>
						<div class="d-none d-md-block">
						<?php if (!is_front_page()) : ?> <?php get_search_form(); ?> <?php endif; ?>
						<?php if (is_front_page()) : ?> <a href="#" class="d-none d-md-block btn-search-focus"><i class="fa fa-search"></i></a> <?php endif; ?>
						</div><!-- /.d-none d-md-block -->
					</div><!-- /.col-12 col-lg-8 -->
				</nav>
			</div>
		</header><!-- #masthead -->

		
		<?php if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
			<div id="page-sub-header" class="sub-header-filter header-home" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 jus">
							<h1>
								<?php
								if(get_theme_mod( 'header_banner_title_setting' )){
									echo get_theme_mod( 'header_banner_title_setting' );
								}else{ echo 'Find a recipe'; }
								?>
							</h1>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-12 col-lg-6">
							<?php get_search_form(); ?>
						</div><!-- /.col-12 col-lg-8 -->
					</div><!-- /.row -->
				</div>
				<div class="cont-categories-filters">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<h2>
									<?php
									if(get_theme_mod( 'header_banner_tagline_setting' )){
										echo get_theme_mod( 'header_banner_tagline_setting' );
									}else{ echo esc_html__('Browse by category'); }
									?>
								</h2>
							</div><!-- /.col-12 -->
						</div><!-- /.row -->
						<div class="row">
							<div class="col-12 d-none d-lg-block">
								<?php // Parent categories dropdowns ?>
								<?php  wp_list_categories( array(
									'orderby' => 'ID'
								)); ?> 
							</div><!-- /.col-12 -->
							<div class="col-12 d-block d-lg-none">
								<select id="all-categories-dropdown" class="form-control all-categories-dropdown" onchange="location = this.value;">
								<?php 
									$categories = get_categories();
									foreach ($categories as $cat) {
										$category_link = get_category_link($cat->cat_ID);
										if ($cat->name != "All categories") {
											echo '<option value="'.esc_url( $category_link ).'">'.$cat->name.'</option>';
										}
									}
								?>
								</select>
							</div><!-- /.col-12 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</div><!-- /.cont-categories-filters -->
			</div>
		<?php endif; ?>

		<?php if(is_blog() && !is_category() && !is_single()): ?>

			<?php 
				$page_for_posts = get_option( 'page_for_posts' );
				$pagePostImage = wp_get_attachment_image_src( get_post_thumbnail_id( $page_for_posts ), 'full')[0];
			?>

			<div id="page-sub-header" class="sub-header-filter header-home header-category" <?php if(!is_single()): ?> style="background-image: url('<?php echo $pagePostImage; ?>');" <?php endif; ?>>

			</div>
		<?php endif; ?>

		<?php if(is_category()): ?>

			<?php 

				$cat = $wp_query->get_queried_object();
				if ( isset( $cat->term_id ) ) {
					$cat_class = sanitize_html_class( $cat->slug, $cat->term_id );
					if ( is_numeric( $cat_class ) || ! trim( $cat_class, '-' ) ) {
						$cat_class = $cat->term_id;
					}

					$classes = $cat->term_id;
				}

				$postImages = new WP_Query(
					array(
						'post_type'=>'post',
						'post_status'=>'publish',
						'posts_per_page'=> -1,
						'cat' => $classes, 
						'order' => 'ASC'
					)
				);
			?>

			<style>	
			.sub-header-filter.overlay::before {
				background-color: rgba(0, 0, 0, .<?php the_field('category_page_opacity', 'option') ?>) !important;	
			}
			</style>

			<div id="page-sub-header" class="sub-header-filter header-category overlay" style="background-image: url('<?php if (function_exists('z_taxonomy_image_url')) { echo z_taxonomy_image_url(); } else if(has_header_image()){ header_image(); } ?>'); background-repeat: no-repeat !important; ">


				<?php //the_archive_title( '<h1 class="page-title">', '</h1>' );?>

				<h1 class="page-title"><?php wp_title();?></h1>
				
			</div>
		<?php endif; ?>

		<?php if(is_single()): ?>
			<div id="page-sub-header" class="sub-header-filter header-single-post" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">

				<a href="<?php echo home_url('/recipes'); ?>" class="back-recipes"><i class="fa fa-angle-left"></i> Back to all Recipes</a>

				<?php if (get_field('sals-recipe-video')): ?>
					<div class="cont-recipe-video">
						<?php the_field('sals-recipe-video') ?>
						<div class="button-play" id="play-button" data-toggle="modal" data-target="#recipeVideo"></div><!-- /.button-play -->
					</div><!-- /.cont-recipe-video -->
				<?php endif ?>

				<?php if (function_exists('z_taxonomy_image')) z_taxonomy_image(); ?>

			</div>
			<?php if (get_field('sals-recipe-brand-logo')): ?>
				<div class="container">
					<div class="row">
						<div class="col-12 justify-content-center">
							<div class="brand-logo">
								<figure>
									<img class="img-fluid" src="<?php the_field('sals-recipe-brand-logo'); ?>" width="auto" heigth="auto" />
								</figure>
							</div><!-- /.brand-logo -->
						</div><!-- /.col-12 -->
					</div><!-- /.row -->
				</div><!-- /.container -->
			<?php endif ?>
		<?php endif; ?>

		<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.min.js"></script>

	<div id="content" class="site-content">
		<?php
			if ( is_front_page() || is_page( '86' ) || get_page_template_slug($post->ID) === 'brand.php' || get_page_template_slug($post->ID) === 'recipes.php' || is_category() || is_search() || is_blog() ) {} else {
				?>
				<div class="<?php if ( is_blog() ): ?>container<?php else : ?>container-fluid<?php endif; ?>">
					<div class="row">
						<div class="col-12 <?php if (is_page('11')) : ?>p-0<?php endif; ?>">
				<?php
			}
			

		?>
			
	<?php endif; ?>