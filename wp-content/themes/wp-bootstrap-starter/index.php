<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-sm-12 col-md-12 col-lg-12">
		<main id="main" class="site-main" role="main">
		<?php
			$args = array(
				'post_type'=> 'post',
				'orderby'    => 'ID',
				'post_status' => 'publish',
				'order'    => 'DESC',
				'posts_per_page' => -1 
			);
			$result = new WP_Query( $args );

			if ( $result-> have_posts() ) {
				while ( $result->have_posts() ) : $result->the_post();
					$pageId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = home_url( $wp->request );

					$objPowerReviews[] = [
						'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
						'locale' => 'en_US',
						'merchant_group_id' => '78368',
						'merchant_id' => '278593',
						'page_id' => strval($pageId),
						'style_sheet' => '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
						'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
						'components' => [
							'CategorySnippet' => 'pr-reviewsnippet-'.$pageId
						]
					]; 
				endwhile;
			}
			wp_reset_postdata();
		?>

		<?php
		if ( $result->have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;
			?>
			<div class="row">
				<?php
				/* Start the Loop */
				while ( $result->have_posts() ) : $result->the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;
				?>
			</div>
			<?php

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

	<script type="text/javascript" charset="utf-8">
		jQuery(document).ready(function(){
			POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
		});
	</script>

<?php
get_footer();
