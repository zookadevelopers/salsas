<?php 
// Template Name: Recipes
get_header(); ?>

<?php
	$recipes_query = new WP_Query(
		array(
			'post_type'=>'post',
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	); 
?>

<div class="container-fluid normal">
	<div class="row">
		<div class="col-12 col-lg-3 col-md-3 left-filters d-none d-md-block">
			<?php get_template_part( 'template-parts/category-filters' ); ?>
		</div><!-- /.col-12 col-lg-3 -->
		<div class="col-12 col-md-9 col-lg-9 col-xl-8 right-recipes">
			<div class="container">
				<div class="row">
					<script type="text/javascript">
						var data = [];
					</script>
					<?php $count = 1; while ( $recipes_query->have_posts() ) : $recipes_query->the_post(); ?>
					<?php
						$pageId = get_the_ID();
						$name = get_the_title();
						$imgUrl = get_the_post_thumbnail_url();
						$currentUrl = get_permalink();
						$allposttags = get_the_tags();
						$postDescription = get_field('sals-recipe-description', $postId);
						$i=0;
						if ($allposttags) {
							foreach($allposttags as $tags) {
								$i++;
								if (1 == $i) {
									$firsttag = $tags->name;
								}
							}
						}
					?>
					<script type="text/javascript">
						data.push({
							locale: 'en_US',
							api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
							merchant_group_id: '78368',
							merchant_id: '278593',
							page_id: '<?php echo $pageId ?>',
							style_sheet: '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
							review_wrapper_url: '/add-review?pr_page_id=' + <?php echo $pageId ?>,
							components:{
								CategorySnippet: 'pr-reviewsnippet-' + <?php echo $pageId ?>
							}
						});
					</script>
					<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">

						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

							<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
								<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
									<?php the_post_thumbnail(); ?>
								</div>
								<div class="entry-header">
									<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
								</div><!-- .entry-header -->

								<div id="pr-reviewsnippet-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
							</a>
						</article><!-- #post-## -->
					</div>
					<?php $count++; endwhile; wp_reset_postdata(); ?>
					<script type="text/javascript" charset="utf-8">
						var currentUrl = window.location.href;
						jQuery(document).ready(function(){
							POWERREVIEWS.display.render(data);
						});
					</script>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</div><!-- /.col-12 col-lg-9 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<?php get_footer(); ?>