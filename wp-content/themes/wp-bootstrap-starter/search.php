<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>
<div class="container-fluid normal">
	<div class="row">
		<div class="col-12 col-lg-3 col-md-3 left-filters d-none d-md-block">
			<?php get_template_part( 'template-parts/category-filters' ); ?>
		</div><!-- /.col-12 col-lg-3 -->
		<div class="col-12 col-xl-8 col-lg-9 col-md-9 right-recipes">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="cont-search-results">
							<?php if (get_search_query() != '') : ?>
							<h1 class="text-center"><?php printf( esc_html__( '"%s"', 'wp-bootstrap-starter' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
							<?php else : ?>
							<h1 class="text-center">Choose filter options above to view recipes</h1>
							<?php endif ?>
						</div><!-- /.cont-search-results -->
					</div><!-- /.col-12 -->
				</div>

				<?php
		if ( have_posts() ) : ?>
			<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
			<div class="row">
				<?php
					/* Start the Loop */
					$count = 0;
					
					while ( have_posts() ) : the_post();
						$count++;
					endwhile;

					while ( have_posts() ) : the_post();
						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						if ($count == 1) {
							?>
							<div class="col-12">
								<p class="text-center"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wp-bootstrap-starter' ); ?></p>
							</div><!-- /.col-12 -->
							<div class="col-12 mb-5">
								<?php get_search_form(); ?>
							</div><!-- /.col-12 -->
							<?php
							break;
						} else {
							//get_template_part( 'template-parts/content', get_post_format() );

							if ( 'post' === get_post_type() && !is_single() ) :
								
								$pageId = get_the_ID();
								$name = get_the_title();
								$imgUrl = get_the_post_thumbnail_url();
								$currentUrl = get_permalink();
								$allposttags = get_the_tags();
								$i=0;
								if ($allposttags) {
									foreach($allposttags as $tags) {
										$i++;
										if (1 == $i) {
											$firsttag = $tags->name;
										}
									}
								}

								$objPowerReviews[] = [
									'api_key' => '59fb8bdf-cb43-490b-8b09-975d68551c2a',
									'locale' => 'en_US',
									'merchant_group_id' => '78368',
									'merchant_id' => '278593',
									'page_id' => strval($pageId),
									'style_sheet' => '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
									'review_wrapper_url' => '/add-review?pr_page_id='.$pageId,
									'components' => [
										'CategorySnippet' => 'pr-reviewsnippet-cat-'.$pageId
									]
								];

						?>

								<div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">

									<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post="category-recipe" <?php if (get_field('sals-recipe-video')) : ?>data-video="true"<?php endif; ?>>

										<a href="<?php echo esc_url( get_permalink()); ?>" class="d-block">
											<div class="post-thumbnail category-post-image" style="background-image: url(<?php the_post_thumbnail_url(); ?>)">
												<?php the_post_thumbnail(); ?>
											</div>
											<div class="entry-header">
												<?php
												if ( is_single() ) :
													the_title( '<h1 class="entry-title">', '</h1>' );
												else :
													the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
												endif;

												?>
											</div><!-- .entry-header -->

											<footer class="entry-footer">
												<?php //wp_bootstrap_starter_entry_footer(); ?>
											</footer><!-- .entry-footer -->
											<div id="pr-reviewsnippet-cat-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
										</a>
									</article><!-- #post-## -->

								</div>

							<?php endif; ?>

							
						<?php }
					endwhile;

					the_posts_navigation();
		else :
			get_template_part( 'template-parts/content', 'none-search' );

		endif; ?>
			
			</div><!-- /.row -->
			<script type="text/javascript" charset="utf-8">
				jQuery(window).load(function(){
					var currentUrl = window.location.href;
					jQuery(document).ready(function(){
						POWERREVIEWS.display.render(<?php print_r(json_encode($objPowerReviews)); ?>);
					});
				});
			</script>


			</div>
		</div>
	</div>
</div>


<?php
get_footer();
