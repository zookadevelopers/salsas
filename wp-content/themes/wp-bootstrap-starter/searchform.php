<form role="search" method="get" class="search-form main-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div class="input-group">
        <input type="search" oninput="this.value=this.value.replace(/[^A-Za-z0-9 ]/g,'');" required class="search-field form-control" placeholder="<?php echo esc_attr_x( 'I want a recipe for...', 'placeholder', 'wp-bootstrap-starter' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php _ex( 'Search for:', 'label', 'wp-bootstrap-starter' ); ?>">
        <div class="input-group-append">
            <button type="submit" class="search-submit btn btn-dark" value="<?php echo esc_attr_x( 'Search', 'submit button', 'wp-bootstrap-starter' ); ?>"><i class="fas fa-search"></i></button>
        </div>
    </div>
</form>

