var $ = jQuery;

if (localStorage.getItem("coupons") === null) {
	window.count = 0;
} else {
	window.count = JSON.parse(localStorage.getItem('coupons')).length;
}

if(count == 0) {
	window.couponsClipped = [];
} else {
	window.couponsClipped = JSON.parse(localStorage.getItem('coupons'));
}

/* Detect Navigator User Agent for overflow hidden */
var ua = navigator.userAgent;
ua = ua.toString();
$('html').attr('id', ua);


$(window).load(function() {
	var carousel = $('#carousel-home');

	$('.owl-carousel.owl-header').owlCarousel({
		animateIn: 'fadeIn',
		animateOut: 'fadeOut',
		autoplay: true,
		autoplayTimeout: 6000,
		dots: false,
		loop: false,
		items: 1,
		transitionStyle : "fade"
	});

	carousel.owlCarousel({
		loop: true,
		margin: 20,
		dots: false,
		nav: true,
		center: true,
		items: 1,
		responsive : {
			1000 : {
				stagePadding: 350
			}
		}
	});

	$('#carousel-home .owl-next').click();

	if ($('body').hasClass('category')) {
		console.log('category');
		var currentFilterChildren = $('.main-parent-filter .cat-item.current-cat-parent .children');
		var currentFilterCategory = currentFilterChildren.find('li.current-cat input[type="checkbox"]');
		currentFilterCategory.attr("checked", true);
	}

	if ($('body').hasClass('single-post')) {

		var checkRecipeVideo = setInterval(function(){
			if($(".recipe-video iframe").length != 0){
				$(".recipe-video iframe")[0].src += "?version=3&enablejsapi=1";	
				clearInterval(checkRecipeVideo);
			}
		}, 1000);
	}

	var windowWidth = $(window).width();
	if (windowWidth <= 768) {
		$('.cont-categories-filters .categories > ul > .cat-item > a').on('click', function(e){
			console.log('click');
			e.preventDefault();
		});

		$('[data-toggle="dropdown"]').on('click touchstart', function(){
			$('.dropdown-toggle').dropdown();
		});

		$('.cont-mobile-menu').slideAndSwipe();
	}

	$('.button-play').on('click', function(e) {
		$('.recipe-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
	});

	$('.recipe-video [data-dismiss="modal"]').on('click', function(e) {
		$('.recipe-video iframe')[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
	});

	if ($('body').hasClass('page-id-95')) {
		couponCount();
		appendPrint();
	}

	$('.cont-categories-filters').on('click', function(){
		console.log('show');
		$('#all-categories-dropdown').focus().click();
	});

});

$(document).ready(function(){

	var btn_read = $('.recipe-description .read-more');
	btn_read.on('click', function(e){
		e.preventDefault();
		var $this = $(this);
		$('.recipe-description p').toggleClass('showing');
		$this.text($this.text() == 'Read More' ? 'Read Less' : 'Read More');
	});

	$('.btn-search-focus').on('click', function(e){
		e.preventDefault();
		$('html, body').stop().animate({
			scrollTop: $('#page-sub-header h1').offset().top - 50
		}, 500);
		$('.main-search input').focus();
	});

	$('#is-product-content').hide();
	$('[class*="acceptance"]').on('click', function(){
		var contactSpecificProduct = $('[type="checkbox"][name="acceptance-840"]').attr('checked') ? true:false;
		
		if (contactSpecificProduct == true) {
			$('#is-product-content').show();
		} else {
			$('#is-product-content').hide();
		}
	});

	$(window).load(function() {
		//loader
		$(".loader").fadeOut("slow");
	});

	var filterLabels = $('.main-parent-filter .cat-item ul.children > li > label');

	filterLabels.on('change', function(){
		disableFilterInputs($(this));
	});

	$('.searchandfilter.main-filter-form').on('submit', function(e) {
		if(!$("[name='ofcategory[]']").is(":checked")) {
			e.preventDefault();
			$(this).append('<div class="my-4 text-center">please select at least one option</div>');
			return false;
		}
		return true;
	});
});

function disableFilterInputs(element) {
	var $this = element;
	var thisInput = $this.children('input[type="checkbox"]');
	var cat_inputs = $this.closest('ul.children').find('li input');

	cat_inputs.attr("checked", false);
	thisInput.attr("checked", true);
}

// Coupons Page

function couponCount() {

	var clearPrint = document.querySelector("#clear-print-coupons");

	if (localStorage.getItem("coupons") === null) {
		window.clippedCount = 0;

		clearPrint.classList.add("d-none");
	} else {
		window.clippedCount = JSON.parse(localStorage.getItem('coupons')).length;

		if (clippedCount != 0) {

			clearPrint.classList.remove("d-none");
		}
	}

	$('.print-coupons-minicart .count-coupon').text(clippedCount);
}

function clipCoupon(coupon) {

	// Get data properties for coupon clipped
	var couponParent = $(coupon).parent();
	var couponName = couponParent.data('coupon-id');
	var couponImage = couponParent.data('coupon-image');

	// check if object exist in array
	for (var k in couponsClipped){
		if (couponsClipped.hasOwnProperty(k)) {
			if (couponsClipped[k].name == couponName) {
				return false;
			}
		}
	}
	// validate if this element !contain class
	if (coupon.classList.contains('coupon-clipped') != true) {

		// push to object with all coupons clipped
		couponsClipped.push({name : couponName, image : couponImage});

		// append div to print coupons
		appendPrint();

		// add class for this coupon clipped
		coupon.classList.add('coupon-clipped');

		localStorage.setItem('coupons', JSON.stringify(couponsClipped));

		couponCount();

	} else {
		console.log('Exist');
	}
}

function appendPrint() {
	// append div to print coupons
	var printList = document.querySelector("#cont-print-coupons");

	for (var k in couponsClipped){
		if (couponsClipped.hasOwnProperty(k)) {
			printList.insertAdjacentHTML("beforeend", '<div class="coupon-printed"><h1>'+ couponsClipped[k].name +'</h1><img src="'+ couponsClipped[k].image +'" class="img-fluid" width="auto" height="auto" /></div>');
		}
	}
}

function clearLocalstorage() {
	localStorage.clear();
	couponCount();
}



// function hasTouch() {
// 	return 'ontouchstart' in document.documentElement
// 		   || navigator.maxTouchPoints > 0
// 		   || navigator.msMaxTouchPoints > 0;
// }

// if (hasTouch()) { // remove all :hover stylesheets
// 	try { // prevent exception on browsers not supporting DOM styleSheets properly
// 		for (var si in document.styleSheets) {
// 			var styleSheet = document.styleSheets[si];
// 			if (!styleSheet.rules) continue;

// 			for (var ri = styleSheet.rules.length - 1; ri >= 0; ri--) {
// 				if (!styleSheet.rules[ri].selectorText) continue;

// 				if (styleSheet.rules[ri].selectorText.match(':hover')) {
// 					styleSheet.deleteRule(ri);
// 				}
// 			}
// 		}
// 	} catch (ex) {}
// }

$('.reset-filters').on('click', function(e){
	e.preventDefault();
	$('.nav-filters input:checkbox').removeAttr('checked');
	window.location.href = '/recipe';
});

/* M E N U   M O B I L E  */
$('.site-content').on('click', '.overlay-custom', function(){
	if ($('#simple-menu i').hasClass('fa-close')) {
		$('#simple-menu').click();
	}
});


// Remove the "for" attribute from the category filter checkboxes that are hidden
$('.cont-options .sf-field-category > ul > li > label').attr('for','');
