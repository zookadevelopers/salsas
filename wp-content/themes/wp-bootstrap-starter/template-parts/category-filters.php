<div class="nav-fixed">
	<h5>Filter All Recipes</h5>
	<div class="cont-options">
		<?php $search_filter_id = get_field( 'search_and_filter_id', 'option' ); ?>
		<?php echo do_shortcode( '[searchandfilter id="'.$search_filter_id.'"]' ); ?>
	</div><!-- /.radio-list -->
</div><!-- /.nav-fixed -->