<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<section class="no-results error-404 page-404-main-section not-found mb-5">

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'wp-bootstrap-starter' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p class="text-center"><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'wp-bootstrap-starter' ); ?></p>
			<?php
				get_search_form();

		else : ?>

			<p class="text-center"><?php esc_html_e( 'We can’t seem to find the page you’re looking for. Here are some helpful links instead.', 'wp-bootstrap-starter' ); ?></p>

			<div class="page-menu">
				<?php echo do_shortcode( '[do_widget id=nav_menu-5]' ); ?>
			</div><!-- /.page-menu -->
			
		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .error-404 -->
