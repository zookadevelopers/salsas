<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<section class="no-results error-404 page-404-main-section not-found mb-5">

	<header class="page-header text-center">
		<h1 class="page-title"><?php esc_html_e( 'Oops!', 'wp-bootstrap-starter' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different selection.', 'wp-bootstrap-starter' ); ?></p>

		<?php else : ?>

			<p class="text-center"><?php esc_html_e( 'We can’t seem to find the page you’re looking for. Here are some helpful links instead.', 'wp-bootstrap-starter' ); ?></p>

			<div class="page-menu">
				<?php echo do_shortcode( '[do_widget id=nav_menu-5]' ); ?>
			</div><!-- /.page-menu -->
			
		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .error-404 -->

<?php
	$recipes_groups = new WP_Query(
		array(
			'post_type'=>'recipes_groups',
			'post_status'=>'publish', 
			'posts_per_page'=> -1,
			'order' => 'ASC'
		)
	); 
?>

<section class="popular-group-recipes">
	<div class="container">
		<?php $count_group = 1; while ( $recipes_groups->have_posts() ) : $recipes_groups->the_post(); ?>
		<?php if ($count_group <= 1): ?> 
		<div class="row">
			<div class="col-12">
				<h2><?php the_field('group_title'); ?></h2>
			</div><!-- /.col-12 -->
		</div><!-- /.row -->
		<div class="row">
			<?php

			$post_objects = get_field('group_recipes');

			if( $post_objects ): ?>
				<?php $count = 1; foreach( $post_objects as $post): ?>

				<?php
					$pageId = get_the_ID();
					$name = get_the_title();
					$imgUrl = get_the_post_thumbnail_url();
					$currentUrl = get_permalink();
					$allposttags = get_the_tags();
					$i=0;
					if ($allposttags) {
						foreach($allposttags as $tags) {
							$i++;
							if (1 == $i) {
								$firsttag = $tags->name;
							}
						}
					}
				?>

				<?php setup_postdata($post); ?>
				<?php if ($count <= 5): ?>
				<div class="col-12 col-md-3">
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="d-block">
							<div class="recipe-image" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
							<div class="recipe-name"><?php the_field('sals-recipe-title'); ?></div><!-- .recipe-name -->
							<div class="brand">With <?php the_field('sals-recipe-author'); ?></div><!-- /.brand -->

							<div id="pr-reviewsnippet-group-<?php echo $count_group; ?>-<?php the_ID(); ?>" class="stars-snippet category-page"></div>
						</a>
					</div><!-- /.item -->
				</div><!-- /.col-12 col-md-3 -->
				<?php endif ?>
				<?php $count++; endforeach; ?>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>

		</div><!-- /.row -->
		<?php endif ?>
		<?php $count_group++; endwhile; wp_reset_postdata(); ?>
	</div><!-- /.container -->
</section><!-- /.popular-group-recipes -->
