<div class="d-block d-md-none">
	<?php 
		$upload_dir = wp_upload_dir();
		$upload_theme = get_template_directory_uri().'/inc/assets/img/';
	?>

	<div class="ssm-overlay ssm-toggle-nav"></div>
	<div class="cont-mobile-menu left-filters">
		<a href="javascript:;" class="btn-close-menu-mobile ssm-toggle-nav"><figure><img src="<?php echo $upload_theme ?>close.png" class="img-fluid" alt="close" width="auto" height="auto" /></figure></a>
		<section class="mobile-logo">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php if ( get_theme_mod( 'wp_bootstrap_starter_logo' ) ): ?>
							<a class="d-block" href="<?php echo esc_url( home_url( '/' )); ?>">
								<figure>
									<img class="img-fluid" src="<?php echo esc_attr(get_theme_mod( 'wp_bootstrap_starter_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="auto" height="auto" />
								</figure>
							</a>
						<?php else : ?>
							<a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
						<?php endif; ?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.mobile-logo -->
		<section class="menu-scroll">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<?php
							wp_nav_menu(array(
								'theme_location'	=> 'primary',
								'container'			=> 'div',
								'container_id'		=> 'main-nav',
								'container_class'	=> 'justify-content-center header-nav',
								'menu_id'			=> false,
								'menu_class'		=> 'navbar-nav',
								'depth'				=> 3,
								'fallback_cb'		=> 'wp_bootstrap_navwalker::fallback',
								'walker'			=> new wp_bootstrap_navwalker()
							));
						?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
				<div class="row">
					<div class="col-12">
						<?php get_search_form(); ?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
				<hr/>
				<div class="row">
					<div class="col-12">
						<?php get_template_part( 'template-parts/category-filters' );?>
					</div><!-- /.col-12 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section><!-- /.menu-scroll -->
	</div><!-- /.cont-mobile-menu -->
</div><!-- /.d-block d-sm-none -->
