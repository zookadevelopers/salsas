<?php 
// Template Name: Write Review
	get_header(); 
?>
<?php
	$postId = $_GET['post_id'];
	$postInfo = get_post($postId);
	$pageId = get_field('page_id', $postId);

	$postTitle = get_field('sals-recipe-title' , $postId);
	$postDescription = get_field('sals-recipe-description', $postId);
	$postImage = get_the_post_thumbnail_url($postId);
	$postUrl = get_post_permalink( $postId );
?>

<?php 
	$allposttags = get_the_tags($postId);
	$i=0;
	if ($allposttags) {
		foreach($allposttags as $tags) {
			$i++;
			if (1 == $i) {
				$firsttag = $tags->name;
			}
		}
	}
?>

<script src="//ui.powerreviews.com/stable/4.0/ui.js" type="text/javascript"></script>
<div class="container">
	<div class="row justify-content-center">
		<div class="col-12 col-md-8">
			<div id="pr-write" class="cont-write-review"></div>
		</div><!-- /.col-12 -->
	</div><!-- /.row -->
</div><!-- /.container -->

<script>// <![CDATA[
	var currentUrl = window.location.host +'/?page_id='+<?php echo $postId; ?>;

	POWERREVIEWS.display.render({
		api_key: '59fb8bdf-cb43-490b-8b09-975d68551c2a',
		locale: 'en_US',
		merchant_group_id: '78368',
		merchant_id: '278593',
		page_id: '<?php echo $pageId; ?>',
		style_sheet: '/wp-content/themes/wp-bootstrap-starter/inc/assets/css/custom/reviews.css',
		product:{
			name: "<?php echo $postTitle; ?>",
			url: "<?php echo $postUrl; ?>",
			description: "<?php echo $postDescription; ?>",
			image_url: "<?php echo $postImage; ?>",
			category_name: "Salsas Site",
			brand_name: "<?php echo $firsttag; ?>",
		},
		components: {
			Write: 'pr-write'
		}
	});
// ]]></script>


<?php get_footer(); ?>